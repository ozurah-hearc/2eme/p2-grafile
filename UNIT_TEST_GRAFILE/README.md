# Utility of this folder
This folder is used by the unit tests (or for testing the app).

# What should I do with it
Be sure to copy this folder with it's content at the root of your OS (like `C:/UNIT_TEST_GRAFILE` on window; `/UNIT_TEST_GRAFILE` on linux)
Else the tests that need existing files will failed.