#pragma once

/**
 * @file includes_grafileConverters.h
 * @brief This file is used to simplify the include of each converters.
 */

#include "converter/DecimalToIntegerConverter.h"
#include "converter/IntegerToDecimalConverter.h"
