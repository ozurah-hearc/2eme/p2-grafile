#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/data/GrafileDataDecimal.h"
 * @brief       Header file, for the class "app/src/data/GrafileDataDecimal.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include "GrafileData.h"

/**
 * @brief GrafileData specialisation for double values.
 */
class GrafileDataDecimal : public GrafileData<double>
{
public:
    GrafileDataDecimal(QVector<double> values);
    GrafileDataDecimal(double value);
    GrafileDataDecimal();

};
