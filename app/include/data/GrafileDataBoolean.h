#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/data/GrafileDataBoolean.h"
 * @brief       Header file, for the class "app/src/data/GrafileDataBoolean.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include "GrafileData.h"

/**
 * @brief GrafileData specialisation for boolean values.
 */
class GrafileDataBoolean : public GrafileData<bool>
{
public:
    GrafileDataBoolean(QVector<bool> values);
    GrafileDataBoolean(bool value);
    GrafileDataBoolean();

};
