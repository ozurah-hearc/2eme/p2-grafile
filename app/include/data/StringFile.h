#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/data/StringFile.h"
 * @brief       Header file, for the class "app/src/data/StringFile.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <QString>
#include <memory>

#include <QMetaType>
#include <QDebug>

/**
 * @brief The main data structure for this project. Each files that will be modified through treatments nodes are stored in an object of this class.
 * A file is represented by it's original path+filename ("originalPath"), it's current location (path+filename) ("currentPath") and it's destination (path+filename) ("finalPath").
 * It also contains some flags to know how the file has been treated and what to do when apply modification.
 */
class StringFile
{
private:
    /**
     * @brief The current path of the file (where is it actualy located).
     */
    QString _currentPath = "";
    /**
     * @brief The original path of the file (where is it located before any changes, like copy to temp folder).
     */
    QString _originalPath = "";
    /**
     * @brief The final path of the file (where it will be located after all changes).
     */
    QString _finalPath = "";

    /**
     * @brief Flag to indicate this file will be deleted.
     */
    bool needDelete = false;
    /**
     * @brief Flag to indicate this file will be moved.
     * (indicate that the source will be deleted after the move, no need to set "needDelete" flag).
     */
    bool needMove = false;

public:
    StringFile() = default;
    StringFile(QString path);
    StringFile(QString path, QString originalPath);
    StringFile(QString path, QString originalPath, QString finalPath);
    StringFile(StringFile const &) = default;

    ~StringFile() = default;

    /**
     * @brief Set the current path of the file (where is it actualy located).
     *
     * @param path
     */
    void setCurrentPath(QString path);
    /**
     * @brief Get the current path of the file (where is it actualy located).
     *
     * @return QString
     */
    QString getCurrentPath();
    /**
     * @brief Set the original path of the file (where is it located before any changes, like copy to temp folder).
     *
     * @param path
     */
    void setOriginalPath(QString path);
    /**
     * @brief Get the original path of the file (where is it located before any changes, like copy to temp folder).
     *
     * @return QString
     */
    QString getOriginalPath();
    /**
     * @brief Set the final path of the file (where it will be located after all changes).
     *
     * @param path
     */
    void setFinalPath(QString path);
    /**
     * @brief Get the final path of the file (where it will be located after all changes).
     *
     * @return QString
     */
    QString getFinalPath();

    /**
     * @brief Set the flag to indicate this file will be deleted.
     *
     * @param state
     */
    void setNeedDelete(bool state);
    /**
     * @brief Get the flag to indicate this file will be deleted.
     *
     * @return bool
     */
    bool getNeedDelete();
    /**
     * @brief Set the flag to indicate this file will be moved.
     * (indicate that the source will be deleted after the move, no need to set "needDelete" flag).
     *
     * @param state
     */
    void setNeedMove(bool state);
    /**
     * @brief Get the flag to indicate this file will be moved.
     * (indicate that the source will be deleted after the move, no need to set "needDelete" flag).
     *
     * @return bool
     */
    bool getNeedMove();

    /* *************************** *
     * ***** DE/SERIALISATION **** *
     * *************************** *
     * Source : https://stackoverflow.com/questions/44567345/serialization-qlistmyobject-to-json
     * *************************** */

    /**
     * @brief Serialize this object to JSON.
     *
     * @return QJsonObject
     */
    QJsonObject toJson() const;
    /**
     * @brief Serialize a collection of StringFile to JSON.
     *
     * @param list collection to serialize.
     * @return QJsonArray
     */
    static QJsonArray toJson(const QList<StringFile> & list);
    /**
     * @brief Deserialize a JSON object to this object.
     */
    void fromJson(QJsonObject const &);
    /**
     * @brief Deserialize a JSON array to a collection of StringFile.
     *
     * @param list JSON array to deserialize.
     * @return QList<StringFile>
     */
    static QList<StringFile> fromJson(QJsonArray const& list);

    /* *************************** *
     * **** OPERATOR OVERLOAD **** *
     * *************************** */

    friend bool operator==(const StringFile& l, const StringFile& r);

    friend QDebug operator<<(QDebug dbg, const StringFile &stringFile);
    friend QDataStream &operator<<(QDataStream &, const StringFile &);
    friend QDataStream &operator>>(QDataStream &, StringFile &);
};

bool operator==(const StringFile& l, const StringFile& r);

Q_DECLARE_METATYPE(StringFile); // to use in QVariant (require for qDebug)
QDebug operator<<(QDebug dbg, const StringFile &stringFile); // to use in qDebug
QDataStream &operator<<(QDataStream &, const StringFile &);
QDataStream &operator>>(QDataStream &, StringFile &);
