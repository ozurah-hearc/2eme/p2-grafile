#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/data/GrafileDataStringFile.h"
 * @brief       Header file, for the class "app/src/data/GrafileDataStringFile.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include "GrafileData.h"
#include "StringFile.h"

/**
 * @brief GrafileData specialisation for StringFile values.
 * This class provides a additional method to deals with StringFiles values as pointer.
 */
class GrafileDataStringFile : public GrafileData<StringFile>
{
public:
    GrafileDataStringFile(QVector<StringFile> values);
    GrafileDataStringFile(StringFile value);
    GrafileDataStringFile();

    /**
     * @brief Get the pointer on the value of the collection at the specified index
     * 
     * @param index index of the data to get
     * @return StringFile* specified data pointer
     * @remark There is no check if the specified index is out of bound of the collection
     * @remark This method wasn't setted in the GrafileData class because it cause error if the T are primitive types like int.
     */
    StringFile * getPtrValueAt(int index);
};
