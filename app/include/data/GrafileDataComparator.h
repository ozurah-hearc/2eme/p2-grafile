#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/data/GrafileDataComparator.h"
 * @brief       Header file, for the class "app/src/data/GrafileDataComparator.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include "GrafileData.h"
#include "Enums.h"

/**
 * @brief GrafileData specialisation for Comparator type values.
 */
class GrafileDataComparator : public GrafileData<CompareOperatorType>
{
public:
    GrafileDataComparator(QVector<CompareOperatorType> values);
    GrafileDataComparator(CompareOperatorType value);
    GrafileDataComparator();

};
