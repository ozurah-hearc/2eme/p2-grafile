#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/data/GrafileDataText.h"
 * @brief       Header file, for the class "app/src/data/GrafileDataText.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include "GrafileData.h"

/**
 * @brief GrafileData specialisation for string values.
 */
class GrafileDataText : public GrafileData<QString>
{
public:
    GrafileDataText(QVector<QString> texts);
    GrafileDataText(QString text);
    GrafileDataText();

};
