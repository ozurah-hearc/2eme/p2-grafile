#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/data/GrafileDataDateTime.h"
 * @brief       Header file, for the class "app/src/data/GrafileDataDateTime.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include "GrafileData.h"
#include <QDateTime>

/**
 * @brief GrafileData specialisation for QDateTime values.
 */
class GrafileDataDateTime : public GrafileData<QDateTime>
{
public:
    GrafileDataDateTime(QVector<QDateTime> dates);
    GrafileDataDateTime(QDateTime date);
    GrafileDataDateTime();

};
