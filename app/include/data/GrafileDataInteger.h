#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/data/GrafileDataInteger.h"
 * @brief       Header file, for the class "app/src/data/GrafileDataInteger.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include "GrafileData.h"

/**
 * @brief GrafileData specialisation for int values.
 */
class GrafileDataInteger : public GrafileData<int>
{
public:
    GrafileDataInteger(QVector<int> values);
    GrafileDataInteger(int value);
    GrafileDataInteger();

};
