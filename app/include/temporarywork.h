#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/temporarywork.h"
 * @brief       Header file, for the class "app/src/ui/MainWindow.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <QString>
#include <QWidget>

/**
 * @brief Class with only statics methods to deal with temporary file (in the temp folder).
 */
class TemporaryWork
{
private:
    /**
     * @brief The name of the app folder in the temporary path.
     */
    static const QString FOLDER_APP_NAME;
    /**
     * @brief The template for random file name.
     * @see Value according the template specified in https://doc.qt.io/qt-6/qtemporaryfile.html.
     */
    static const QString TEMP_FILENAME_TEMPLATE;
public:
    /**
     * @brief The path to the temporary folder (including the FOLDER_APP_NAME).
     */
    static const QString TEMP_FOLDER;

    /**
     * @brief Get a filename with the path in the temporary folder.
     * If the temporary folder doesn't exist, it will be created.
     * @return QString path and filename.
     */
    static QString getTempFile();
    /**
     * @brief Delete the temporary folder with all it's content.
     */
    static void cleanTempAppFolder();
    /**
     * @brief Check if the file path is the path to the temporary folder.
     * 
     * @param file file (with path) to check.
     * @return true if the file is in the temporary folder, false otherwise.
     */
    static bool isFileInTemp(QString file);
    /**
     * @brief Display a message box that indicate the copy to the temp. folder didn't work
     * To avoid lost files, prompt what to do with the source file.
     * 
     * @param file source file that couldn't be copied to the temp. folder.
     * @param parent owner of the message box.
     * @return true the user select "keep"
     * @return false the user select "skip"
     */
    static bool displayMessageNoTempFile(QString file, QWidget* parent = nullptr);
private:
    TemporaryWork() = delete; // Only statics methods, no need to create an instance.
    /**
     * @brief Create the temporary folder if required.
     */
    static void createTempAppFolder();

};
