#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/converter/DecimalToIntegerConverter.h"
 * @brief       Header file, for the class "app/src/converter/DecimalToIntegerConverter.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/TypeConverter>

#include "data/GrafileDataDecimal.h"
#include "data/GrafileDataInteger.h"

using QtNodes::NodeData;
using QtNodes::NodeDataType;
using QtNodes::SharedNodeData;

/**
 * @brief Class to allow the conversion of a GrafileDataDecimal to a GrafileDataInteger between node port.
 */
class DecimalToIntegerConverter
{

public:

  SharedNodeData operator()(SharedNodeData data);

private:

  SharedNodeData _integer;
};
