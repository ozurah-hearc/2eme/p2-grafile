#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/converter/IntegerToDecimalConverter.h"
 * @brief       Header file, for the class "app/src/converter/IntegerToDecimalConverter.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/TypeConverter>

#include "data/GrafileDataInteger.h"
#include "data/GrafileDataDecimal.h"

using QtNodes::NodeData;
using QtNodes::NodeDataType;
using QtNodes::SharedNodeData;

/**
 * @brief Class to allow the conversion of a GrafileDataInteger to a GrafileDataDecimal between node port.
 */
class IntegerToDecimalConverter
{

public:

  SharedNodeData  operator()(SharedNodeData data);

private:

  SharedNodeData _decimal;
};
