#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/filter/OrBoolFilterNode.h"
 * @brief       Header file, for the class "app/src/filter/OrBoolFilterNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "includes_grafileData.h"
#include "filter/FilterNodeModel.h"

/**
 * @brief This node will perform a OR between 2 boolean.
 * It contains 2 boolean input ports and 1 boolean output port.
 */
class OrBoolFilterNode : public FilterNodeModel
{
public:
    OrBoolFilterNode();

    /**
     * @brief Perform a OR between the input A and B.
     * If one on the inputs aren't connected, the output will be the connected one.
     */
    virtual void compute() override;
    std::shared_ptr<NodeData> outData(PortIndex outPortIndex) override;

protected:
    virtual void convertInputsToVars() override;

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    GrafileDataBoolean inA;
    GrafileDataBoolean inB;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    GrafileDataBoolean output;
};
