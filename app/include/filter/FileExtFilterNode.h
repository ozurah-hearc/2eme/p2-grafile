#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/filter/FileExtFilterNode.h"
 * @brief       Header file, for the class "app/src/filter/FileExtFilterNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "includes_grafileData.h"
#include "filter/FilterNodeModel.h"

/**
 * @brief This node will compare the extension of the file with the given text input.
 * It contains as input : the StringFiles and a Text. As output : the files that match and the files that doesn't match.
 * @remark For now, the compare is in the exactitude of the text value, case sensitve. An upgrade to use Regex should be nice.
 */
class FileExtFilterNode : public FilterNodeModel
{
public:
    FileExtFilterNode();

    /**
     * @brief Perform the comparaison between each files extension and the extensions.
     */
    virtual void compute() override;
    std::shared_ptr<NodeData> outData(PortIndex outPortIndex) override;

protected:
    virtual void convertInputsToVars() override;

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    GrafileDataStringFile inFiles;
    GrafileDataText inExt;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    GrafileDataStringFile outFiltered;
    GrafileDataStringFile outExcluded;

};
