#pragma once

/**
 * @attention   /!\ Unimplemented node /!\
 * 
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/filter/FileContainsFilterNode.h"
 * @brief       Header file, for the class "app/src/filter/FileContainsFilterNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 * 
 */

#include <nodes/NodeDataModel>
#include "filter/FilterNodeModel.h"

class FileContainsFilterNode : public FilterNodeModel
{
public:
    FileContainsFilterNode();

    // TODO: methods

protected:

    // TODO: attributes
};
