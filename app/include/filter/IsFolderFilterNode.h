#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/filter/IsFolderFilterNode.h"
 * @brief       Header file, for the class "app/src/filter/IsFolderFilterNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "includes_grafileData.h"
#include "filter/FilterNodeModel.h"

/**
 * @brief This node will separate the given StringFile input to files and folders.
 * It contains as input : the StringFiles. As output : the files; the folders.
 * @remark A file is detected has folder if at least 1 condition match :
 *          - The path exists and it's a folder
 *          - The path ends with a '/', '\' or '[/\](.).'
 */
class IsFolderFilterNode : public FilterNodeModel
{
public:
    IsFolderFilterNode();

    /**
     * @brief Perform the separation between each files and folders.
     */
    virtual void compute() override;
    std::shared_ptr<NodeData> outData(PortIndex outPortIndex) override;

protected:
    virtual void convertInputsToVars() override;

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    GrafileDataStringFile inFiles;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    GrafileDataStringFile outFiles;
    GrafileDataStringFile outFolders;
};
