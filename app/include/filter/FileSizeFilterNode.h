#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/filter/FileSizeFilterNode.h"
 * @brief       Header file, for the class "app/src/filter/FileSizeFilterNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "includes_grafileData.h"
#include "filter/FilterNodeModel.h"

/**
 * @brief This node will compare the size of the file.
 * It contains as input : the StringFiles; a comparator and decimal value. As output : the files that match and the files that doesn't match.
 * @remark The comparaison will use "DEFAULT_UNIT" has unit base for the decimal value.
 */
class FileSizeFilterNode : public FilterNodeModel
{
public:
    /**
     * @brief Unit to use for the decimal value.
     * The base is in "megabyte" (b -> *1024 = kB -> *1024 = MO").
     */
    const double DEFAULT_UNIT = 1024 * 1024; 

    FileSizeFilterNode();

    /**
     * @brief Perform the comparaison between each file size and the decimal value, according the comparator type.
     */
    virtual void compute() override;
    std::shared_ptr<NodeData> outData(PortIndex outPortIndex) override;

protected:
    virtual void convertInputsToVars() override;

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    GrafileDataStringFile inFiles;
    GrafileDataComparator inComparators;
    GrafileDataDecimal inSizes;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    GrafileDataStringFile outFiltered;
    GrafileDataStringFile outExcluded;
};
