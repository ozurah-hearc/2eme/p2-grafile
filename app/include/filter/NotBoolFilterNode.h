#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/filter/NotBoolFilterNode.h"
 * @brief       Header file, for the class "app/src/filter/NotBoolFilterNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "includes_grafileData.h"
#include "filter/FilterNodeModel.h"

/**
 * @brief This node will perform a NOT on a boolean.
 * It contains 1 boolean input port and 1 boolean output port.
 */
class NotBoolFilterNode : public FilterNodeModel
{
public:
    NotBoolFilterNode();

    /**
     * @brief Perform a NOT (bool inversion logic) on the boolean.
     */
    virtual void compute() override;
    std::shared_ptr<NodeData> outData(PortIndex outPortIndex) override;

protected:
    virtual void convertInputsToVars() override;

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    GrafileDataBoolean input;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    GrafileDataBoolean output;
};
