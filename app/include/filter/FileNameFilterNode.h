#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/filter/FileNameFilterNode.h"
 * @brief       Header file, for the class "app/src/filter/FileNameFilterNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "includes_grafileData.h"
#include "filter/FilterNodeModel.h"

/**
 * @brief This node will compare the name of the file with the given text input.
 * It contains as input : the StringFiles; a Text and is the compare is case sensitive. As output : the files that match and the files that doesn't match.
 * @remark For now, the compare is in the exactitude of the text value. An upgrade to use Regex should be nice.
 */
class FileNameFilterNode : public FilterNodeModel
{
public:
    FileNameFilterNode();

    /**
     * @brief Perform the comparaison between each files name and the names.
     */
    virtual void compute() override;
    std::shared_ptr<NodeData> outData(PortIndex outPortIndex) override;

protected:
    virtual void convertInputsToVars() override;

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    GrafileDataStringFile inFiles;
    GrafileDataText inNames;
    bool inCaseSensitive;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    GrafileDataStringFile outFiltered;
    GrafileDataStringFile outExcluded;
};
