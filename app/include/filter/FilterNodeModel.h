#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/filter/FilterNodeModel.h"
 * @brief       Header file, for the class "app/src/filter/FilterNodeModel.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "GrafileNodeModel.h"
#include "Enums.h"

/**
 * @brief Base class for "filter" node.
 * It will perform a filter between values passed as input.
 * Some filter nodes can be enabled. Some filters force the compute (like boolean filters) others will need the graph executed ("_isComputeExec = true") to perform their compute method.
 * The input and output ports are defined in the derived classes.
 */
class FilterNodeModel : public GrafileNodeModel
{
public:
    FilterNodeModel();
    /**
     * @brief
     * 
     * @param name The name of the node (that the user can see and understand the role of the node).
     * @param canEnable Is the node had a "enable" input port.
     */
    FilterNodeModel(QString name, bool canEnable = true);

protected:
    /**
     * @brief Compare two value with the specified comparison operator.
     * 
     * @param a Value A
     * @param comparator Comparison operator 
     * @param b Value B
     * @return Comparaison result
     */
    bool compareValues(int a, CompareOperatorType comparator, int b);
};
