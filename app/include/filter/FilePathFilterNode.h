#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/filter/FilePathFilterNode.h"
 * @brief       Header file, for the class "app/src/filter/FilePathFilterNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "includes_grafileData.h"
#include "filter/FilterNodeModel.h"

/**
 * @brief This node will compare the path of the file with the given text input.
 * It contains as input : the StringFiles and a Text. As output : the files that match and the files that doesn't match.
 * @remark The compared path can have relative information inside (like "C:/folder/../folder2") and the compare is case insensitive. An upgrade to use Regex should be nice.
 */
class FilePathFilterNode : public FilterNodeModel
{
public:
    FilePathFilterNode();

    /**
     * @brief Perform the comparaison between each files path and the paths.
     */
    virtual void compute() override;
    std::shared_ptr<NodeData> outData(PortIndex outPortIndex) override;

protected:
    virtual void convertInputsToVars() override;

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    GrafileDataStringFile inFiles;
    GrafileDataText inPaths;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    GrafileDataStringFile outFiltered;
    GrafileDataStringFile outExcluded;
};
