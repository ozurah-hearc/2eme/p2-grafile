#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/GrafileNodeModel.h"
 * @brief       Header file, for the class "app/src/ui/MainWindow.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "GrafileData.h"

using QtNodes::PortType;
using QtNodes::PortIndex;
using QtNodes::NodeData;
using QtNodes::NodeDataType;
using QtNodes::NodeDataModel;
using QtNodes::NodeValidationState;

/**
 * @brief The main class for the nodes used in the Grafile project.
 * It specialise the NodeDataModel of NodeEditor to simplify and uniformise some methods.
 */
class GrafileNodeModel : public NodeDataModel
{
    Q_OBJECT

signals:
    /**
     * @brief onEnabledComputeChange Event to notify the compute enabled for nodes have changed
     * @param isComputed The new value for execute compute logic
     */
    void eventEnabledComputeChange(bool isComputed);

public:

    /**
     * @brief Construct a new Grafile Node.
     * It will call GrafileNodeModel("Unnamed node")
     */
    GrafileNodeModel();
    /**
     * @brief Construct a new Grafile Node.
     * @param name The name of the node.
     */
    GrafileNodeModel(QString name);

    /**
     * @brief Get the widget parent of the node without need to specify it.
     * 
     * @return QWidget* 
     */
    QWidget * getParent();
    
    /**
     * @brief Get the caption of the node, actually, it is the same as the name.
     * @return QString Caption of the node.
     */
    QString caption() const override;

    /**
     * @brief Get the name of the node.
     * @return QString Name of the node.
     */
    QString name() const override;

    /**
     * @brief The logic of the node should be placed in this method.
     * Every time a input is changed, this method will be called if "_isComputeExec" or "forceCompute" = true.
     * @remark used by "setInData"
     */
    virtual void  compute() {};

    /**
     * @brief Set if the compute of each nodes can be executed when inputs change.
     * @param state new state of execution availibility.
     */
    void setComputeExec(bool state)
    {
        _isComputeExec = state;
        Q_EMIT eventEnabledComputeChange(state);
    }

    /**
     * @brief Get if the compute of each nodes can be executed when inputs change.
     * @return
     */
    bool isComputeExec() { return this->_isComputeExec; }

    /**
     * @brief Get if this node is enabled or not.
     * If it isn't enabled, the "compute" will rise, but it will just retrive the inputs to the outputs without modification.
     * 
     * To set it, use "setEnabled" (public) or "setEnableInput" (protected) according the situation.
     * @return bool 
     */
    bool isEnabled() { return this->_isEnabled; }
    /**
     * @brief Set if this node is enabled or not.
     * If it isn't enabled, the "compute" will rise, but it will just retrive the inputs to the outputs without modification.
     * 
     * @param isEnabled
     */
    void setEnabled(bool state) { this->_isEnabled = state; }

    /**
     * @brief Get the number of port (inputs or ouputs) of the node.
     * 
     * @param portType Which type of port we want to get the number of ports.
     * @return unsigned int Number of port.
     */
    unsigned int nPorts(PortType portType) const override;

    /**
     * @brief Get the type of the specified port
     * 
     * @param portType The type of port (in or out).
     * @param portIndex The index of the port.
     * @return NodeDataType The type of the port.
     */
    NodeDataType dataType(PortType portType, PortIndex portIndex) const override;

    /**
     * @brief Is the caption of the specified port visible or not.
     * 
     * @return bool 
     */
    bool portCaptionVisible(PortType portType, PortIndex portIndex) const override { return true; }

    /**
     * @brief Get the caption of the specified port.
     * 
     * @param portType The type of port (in or out).
     * @param portIndex The index of the port.
     * @return QString The caption of the port.
     */
    QString portCaption(PortType portType, PortIndex portIndex) const override;

    /**
     * @brief Retrive the data of a out port of the node.
     * 
     * @param outPortIndex the index of the port.
     * @return std::shared_ptr<NodeData> The data of the out port.
     */
    std::shared_ptr<NodeData> outData(PortIndex outPortIndex) override { return nullptr; }

    /**
     * @brief Set the data of a in port.
     * This method will be executed when "Q_EMIT dataUpdated(...)" is called by the previous connected node.
     * 
     * @param data data to set on the port.
     * @param portIndex port index to set the data.
     */
    void setInData(std::shared_ptr<NodeData> data, PortIndex portIndex) override;

    /**
     * @brief Set the data of a in port.
     * This method will be executed when "Q_EMIT dataUpdated(...)" is called by the previous connected node.
     * 
     * @param data data to set on the port.
     * @param portIndex port index to set the data.
     * 
     * @remark /!\ Actually, even if this method is designed to handle multiple connexion in a single in port, we don't handle it in our application.
     *         Only the first data (connexion) will be used /!\
     */
    void setInData(std::vector<std::shared_ptr<NodeData>> data, PortIndex inPortIndex) override;


    /**
     * @brief Get the embedded widget of this node.
     * If the node contains a widget, this method should be override.
     * 
     * @return QWidget* the widget embedded in the node (nullptr in this case).
     */
    QWidget *embeddedWidget() override { return nullptr; }

    /**
     * @brief Get the state of validation of the node.
     * 
     * @return NodeValidationState 
     */
    NodeValidationState  validationState() const override;

    /**
     * @brief Get the validation message of the node.
     * 
     * @return QString 
     */
    QString  validationMessage() const override;
    /**
     * @brief Change the current validation state of the node.
     * 
     * @param hasError True if the node contains an erreur, else if the node has no error.
     */
    void changeValidationState(bool hasError);

protected:

    /**
     * @brief Call this method to register a in port.
     * 
     * @param typeName Type name of the port (it can only accept connexion with this type (or with a converter)).
     * @param name Name of the port (which one will be displayed near the connexion point in the node).
     */
    void addInputPort(QString typeName, QString name);
    /**
     * @brief Call this method to register a out port.
     * 
     * @param typeName Type name of the port (it can only accept connexion with this type (or with a converter)).
     * @param name Name of the port (which one will be displayed near the connexion point in the node).
     */
    void addOutputPort(QString typeName, QString name);

    /**
     * @brief Is the node is enabled.
     * @see isEnabled, setEnabled, setEnableInput.
     * 
     */
    bool _isEnabled = true;

    /**
     * @brief is the compute function will be called when a input port is updated ?
     * It will be setted to true by the start node, and false by the end node.
     *
     * It is static because the enable state is shared beteween each nodes.
     * @remark Considere using the setter "setComputeExec" to change this value, else the event "eventEnabledComputeChange" will not been rise.
     * @remark /!\ It's not the best way to deal it, but for now it's an "acceptable" solution, it should by changed when the "run graph" system is ameliored.
     */
    static bool _isComputeExec;

    /**
     * @brief Is the compute function need to be executed even if _isComputeExec is false.
     * 
     * The execution logic use the nodes diretly have a path with the start node.
     * set this variable to true for "value nodes" or nodes that doesn't deals with StringFile like "boolean filter". 
     */
    bool forceCompute = false; // Enable this if the node input can't be connected to the start node.
    
    /**
     * @brief Name of the node (which one the user will seen).
     */
    QString _name = "Unnamed node";

    /**
     * @brief The types of in ports (index of the list based on the port number).
     */
    QVector<NodeDataType> _inputTypes  = {};
    /**
     * @brief The types of out ports (index of the list based on the port number).
     */
    QVector<NodeDataType> _outputTypes = {};

    /**
     * @brief The names of in ports (index of the list based on the port number).
     */
    QVector<QString> _inputNames  = {};
    /**
     * @brief The names of out ports (index of the list based on the port number).
     */
    QVector<QString> _outputNames = {};

    /**
     * @brief The data of in ports (index of the list based on the port number).
     * 
     * @remark Used "NodeData" instead of "GrafileData" to allow futher nodes that node use the Grafile logic (like addon).
     */
    QVector<std::shared_ptr<NodeData>> _input;
    /**
     * @brief Convert the _input values to variables of the derived classes.
     * * Every time a input is changed, this method will be called.
     * @remarks used by "setInData".
     * - If not overrided, _inputs is not translated.
     */
    virtual void convertInputsToVars() {return;};
    /**
     * @brief perform the translation of the _input at the specified port to the _isEnabled variable.
     * 
     * @param inPortIndex index of the enable input port.
     * 
     * @remark Call this method when overriding "convertInputsToVars" if the node can be enabled.
     */
    void setEnableInput(QtNodes::PortIndex inPortIndex);

    /**
     * @brief The state of validation of the node.
     * If the state isn't valid, display "modelValidationError" on the node.
     */
    NodeValidationState modelValidationState = NodeValidationState::Valid;
    /**
     * @brief Displayed message if the NodeValidation isn't valid.
     * 
     */
    QString modelValidationError = QStringLiteral("Missing or incorrect inputs");
};
