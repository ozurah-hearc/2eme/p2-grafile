#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/treatment/MoveTreatmentNode.h"
 * @brief       Header file, for the class "app/src/treatment/MoveTreatmentNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "includes_grafileData.h"
#include "treatment/TreatmentNodeModel.h"

class GrafileDataStringFile;

/**
 * @brief This node will prepare files to be moved into a specific folder.
 * @remark /!\ For now, folders aren't handled for move. 
 *             If a folder is detected,it will be ignored
 *             (to avoid unwanted issue with next nodes,
 *              the output will not contains the folder). /!\
 */
class MoveTreatmentNode : public TreatmentNodeModel
{
public:
    MoveTreatmentNode();

    /**
     * @brief Perform the move node logic (for each dest., it will call "moveLogic" method).
     * When called it will copy files to the temporary folder and set their current and final path.
     * The files will be setted to be moved to each path (only folder) contains in the "dest" variable.
     * The flag "move" will be setted for each output data (except deleted one).
     * @remark If a file is moved to more than one destination ("dest" variable), the "apply move" (of the end node) will create a copy to each destination and delete only the source.
     */
    virtual void  compute() override;
    std::shared_ptr<NodeData> outData(PortIndex outPortIndex) override;

    /**
     * @brief Perform the move logic for each files :
     *  1) copy to the file to the temporary folder;
     *  2) Set the current and the final path of the file.
     *  3) Set the move flag
     * 
     * @param src all files to move.
     * @param dest where the file will be moved.
     * @return GrafileDataStringFile The data modified according a move operation (except the flag).
     */
    GrafileDataStringFile moveLogic(GrafileDataStringFile src, QString dest);
protected:
    virtual void convertInputsToVars() override;

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    GrafileDataStringFile src;
    GrafileDataStringFile dest;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    GrafileDataStringFile output;
};
