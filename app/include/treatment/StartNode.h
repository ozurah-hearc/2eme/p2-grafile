#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/treatment/StartNode.h"
 * @brief       Header file, for the class "app/src/treatment/StartNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "treatment/TreatmentNodeModel.h"

#include "data/GrafileDataStringFile.h"

/**
 * @brief This node is the starting point of the graph execution.
 * It allows other nodes to execute their compute method after this one had execute his compute method.
 */
class StartNode : public TreatmentNodeModel
{
    Q_OBJECT

public:
    StartNode();
    /**
     * @brief Perform the start graph logic
     */
    void compute() override;
    std::shared_ptr<NodeData> outData(PortIndex outPortIndex) override;

    /**
     * @brief Set the files to use as source for the entry point of the graph.
     * @param files files to use (path + filename)
     */
    void setData(QStringList files);
private:
    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    GrafileDataStringFile output;
};
