#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/treatment/EndNode.h"
 * @brief       Header file, for the class "app/src/treatment/EndNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "includes_grafileData.h"
#include "treatment/TreatmentNodeModel.h"

/**
 * @brief This node is the ending point of the graph execution.
 * It disallow other nodes to execute their compute method when this one had execute his compute method.
 * During his execution, it will apply the modifications made by the treatment nodes.
 */
class EndNode : public TreatmentNodeModel
{
    Q_OBJECT

public:
    EndNode();
    /**
     * @brief Perform the end graph logic
     * When called it will pass the "_isComputeExec" flag to false.
     * It will apply the modifications made by the treatment nodes :
     *  1) Create a copy of the temporary files to the final path that needed (if the file is copied or moved);
     *  2) If all files have been copied to their final path without error, it will delete each files marked as "moved" or "deleted";
     *  3) If no error after the deletion, it will clean the temporary folder (by deleting it).
     */
    void compute() override;

protected:
    virtual void convertInputsToVars() override;

    /**
     * @brief Perform the point 1 of the "compute" method for a single file :
     * 1) Create a copy of the temporary files to the final path that needed (if the file is copied or moved).
     * 
     * @param file file that will be copied.
     * @return is the copy worked ?
     */
    bool applyCopyLogic(StringFile *file);
    /**
     * @brief Perform the point 2 of the "compute" method for a single file :
     * 2) If all files have been copied to their final path without error, it will delete each files marked as "moved" or "deleted".
     * 
     * @param file file that will be deleted.
     * @return is the deletion worked ?
     */
    bool applyDeleteLogic(StringFile *file);

    /**
     * @brief Prompt a message box to the user if an error occured during an "applyXXXLogic" method.
     * The message will be visible in the console too.
     * 
     * @param file The file that encountered an error.
     * @param when At which moment the error occured (when copy; when move; when delete).
     */
    void displayErrorMessageApplyLogic(StringFile file, QString when);

    /**
     * @brief Perform a copy of the file to the final path (only files not moved).
     * 
     * @param file file that will be copied.
     * @return is the copy worked ?
     */
    bool applyCopy(StringFile file);
    /**
     * @brief Perform a "move" (copy) of the file to the final path (only files moved).
     * 
     * @param file file that will be "moved".
     * @return is the move (copy) worked ? 
     */
    bool applyMove(StringFile file);
    /**
     * @brief Perfom the deletion of the source file (for files mared as moved or deleted).
     * 
     * @param file file that will be deleted.
     * @return is the deletion worked ?
     */
    bool applyDelete(StringFile file);

    /**
     * @brief Prompt a message to the user to ask what to do with the file because the destination already contains a file with the same name.
     * 
     * @param file file who had his destination with a file with the same name.
     * @return true if the user choose to overwrite the file, false otherwise.
     */
    bool actionIfFileAlreadyExists(StringFile file);

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    GrafileDataStringFile src;
};
