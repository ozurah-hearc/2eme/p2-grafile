#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/treatment/TreatmentNodeModel.h"
 * @brief       Header file, for the class "app/src/treatment/TreatmentNodeModel.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "GrafileNodeModel.h"

/**
 * @brief Base class for "treatment" node.
 * It will perform a treatment according the StringFile source.
 * This nodes can be enabled and the compute method will only be called if the graph is executed ("_isComputeExec = true").
 * The input and output ports are defined in the derived classes.
 */
class TreatmentNodeModel : public GrafileNodeModel
{
    Q_OBJECT

public:
    TreatmentNodeModel();
    /**
     * @brief
     * 
     * @param name The name of the node (that the user can see and understand the role of the node).
     * @param canEnable Is the node had a "enable" input port.
     */
    TreatmentNodeModel(QString name, bool canEnable = true);
};
