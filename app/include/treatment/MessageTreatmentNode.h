#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/treatment/MessageTreatmentNode.h"
 * @brief       Header file, for the class "app/src/treatment/MessageTreatmentNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "includes_grafileData.h"
#include "treatment/TreatmentNodeModel.h"

/**
 * @brief This node will prompt a message to the user.
 * It can be used for exemple to enable/disabled nodes by the user at graphe runtime.
 */
class MessageTreatmentNode : public TreatmentNodeModel
{
public:
    MessageTreatmentNode();

    /**
     * @brief Perform the message logic :
     * It will prompt a message to the user according his configuration.
     * The out port will return true according which button the user clicked.
     * If the node is disabled, the out port will return both true.
     */
    virtual void  compute() override;
    std::shared_ptr<NodeData> outData(PortIndex outPortIndex) override;

protected:
    virtual void convertInputsToVars() override;

    /* *************************** *
     * **** DEFAULT VALUE FOR **** *
     * **** UNCONNECTED IN    **** *
     * *************************** */

    QString DEFAULT_MESSAGE = "default message";
    QString DEFAULT_TXT_BTN1 = "button 1";
    QString DEFAULT_TXT_BTN2 = "button 2";

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    GrafileDataStringFile inForConnectionOrder; // Just used to trigg this node during the graph execution at the desired moment.
    QString message = DEFAULT_MESSAGE;
    QString txtBtn1 = DEFAULT_TXT_BTN1;
    QString txtBtn2 = DEFAULT_TXT_BTN2;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    GrafileDataStringFile outForConnectionOrder; // Just used to trigg this node during the graph execution at the desired moment.
    bool onBtn1 = false;
    bool onBtn2 = false;
};
