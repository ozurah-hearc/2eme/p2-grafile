#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/treatment/CopyTreatmentNode.h"
 * @brief       Header file, for the class "app/src/treatment/CopyTreatmentNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "includes_grafileData.h"
#include "treatment/TreatmentNodeModel.h"

class GrafileDataStringFile;

/**
 * @brief This node will prepare files to be copied into a specific folder.
 * @remark /!\ For now, folders aren't handled for copy. 
 *             If a folder is detected,it will be ignored
 *             (to avoid unwanted issue with next nodes,
 *              the output will not contains the folder). /!\
 */
class CopyTreatmentNode : public TreatmentNodeModel
{
public:
    CopyTreatmentNode();

    /**
     * @brief Perform the copy node logic (for each dest., it will call "copyLogic" method).
     * When called it will copy files to the temporary folder and set their current and final path.
     * The files will be setted to be copied to each path (only folder) contains in the "dest" variable.
     */
    virtual void  compute() override;
    std::shared_ptr<NodeData> outData(PortIndex outPortIndex) override;

    /**
     * @brief Perform the copy logic for each files :
     *  1) copy to the file to the temporary folder;
     *  2) Set the current and the final path of the file.
     * 
     * @param src all files to copy.
     * @param dest where the file will be copied.
     * @return GrafileDataStringFile The data modified according a copy operation.
     */
    GrafileDataStringFile copyLogic(GrafileDataStringFile src, QString dest);

protected:
    virtual void convertInputsToVars() override;

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    GrafileDataStringFile src;
    GrafileDataStringFile dest;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    GrafileDataStringFile output;
};
