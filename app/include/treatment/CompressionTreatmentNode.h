#pragma once

/**
 * @attention   /!\ Unimplemented node /!\ 
 * 
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/treatment/CompressionTreatmentNode.h"
 * @brief       Header file, for the class "app/src/treatment/CompressionTreatmentNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "treatment/TreatmentNodeModel.h"

class CompressionTreatmentNode : public TreatmentNodeModel
{
public:
    CompressionTreatmentNode();

    // TODO: methods

protected:

    // TODO: attributes
};
