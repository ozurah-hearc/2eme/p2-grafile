#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/treatment/DeleteTreatmentNode.h"
 * @brief       Header file, for the class "app/src/treatment/DeleteTreatmentNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "includes_grafileData.h"
#include "treatment/TreatmentNodeModel.h"

/**
 * @brief This node will prepare files to be deleted.
 * The deleted file will be the origine value.
 * If a file was already manipulated (that means it has a final path, ie after moved or copied) the deleted file will be the final path.
 * In this case, the output will have the current and origin path modified to the final path.
 * @remark a StringFile marked has deleted has no final path.
 * @remark a deleted file by extension can't be moved, the "move" flag will be setted to false.
 */
class DeleteTreatmentNode : public TreatmentNodeModel
{
public:
    DeleteTreatmentNode();

    /**
     * @brief Perform the delete node logic.
     * When called, it will :
     * 1) For manipulated files (who had a final path), it will set the current and origin with the final path.
     * 2) Set the final path to "" (empty).
     * 3) set the deleted flag to true, and set the move flag to false.
     */
    virtual void  compute() override;
    std::shared_ptr<NodeData> outData(PortIndex outPortIndex) override;

protected:
    virtual void convertInputsToVars() override;

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    GrafileDataStringFile src;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    GrafileDataStringFile output;
};
