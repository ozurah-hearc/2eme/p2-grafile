#ifndef INPUTCOLUMNWIDGET_H
#define INPUTCOLUMNWIDGET_H

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/ui/InputColumnWidget.h"
 * @brief       Header file, for the class "app/src/ui/InputColumnWidget.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <QWidget>
#include <QList>
#include <QString>
#include <QTreeView>
#include <QFileDialog>
#include <QStack>

#include "CustomFileModel.h"

/**
 * @brief Input column widget.
 *
 */
class InputColumnWidget : public QWidget
{
    Q_OBJECT

private:
    QTreeView* inputTree;
    CustomFileModel* fileSystemModel;
    QFileDialog* selectInputDialog;
    QFutureWatcher<void>* listDirectoriesWatcher;

public:
    InputColumnWidget(QWidget *parent = nullptr);
    QList<QFileInfo> *getOpenedFiles();
    void setExecuteButtonEnabled(bool isEnabled);

private:
    void setupUI();

private slots:
    void selectedDirectory(QString directory);
    void updateReadyness();

signals:
    void directoryChanged(QString directory);
    void selectedFolder(QString folder);
    void dataLoaded(bool state);
    void execute();
};

#endif // INPUTCOLUMNWIDGET_H
