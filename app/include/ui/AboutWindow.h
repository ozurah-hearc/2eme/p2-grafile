#ifndef ABOUTWINDOW_H
#define ABOUTWINDOW_H

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 *
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/ui/AboutWindow.h"
 * @brief       Header file, for the class "app/src/ui/AboutWindow.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <QWidget>

/**
 * @brief The widget that contains about informations.
 */
class AboutWindow : public QWidget
{
    Q_OBJECT
public:
    AboutWindow(QWidget *parent = nullptr, Qt::WindowType type = Qt::Dialog);

private:
    /**
    * @brief Create the about window UI and connect components.
    */
    void setupUI();
signals:

};

#endif // ABOUTWINDOW_H
