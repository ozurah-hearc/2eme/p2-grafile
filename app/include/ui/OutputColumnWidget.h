#ifndef OUTPUTCOLUMNWIDGET_H
#define OUTPUTCOLUMNWIDGET_H

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/ui/OutputColumnWidget.h"
 * @brief       Header file, for the class "app/src/ui/OutputColumnWidget.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <QWidget>
#include <QWidget>
#include <QList>
#include <QString>
#include <QTreeView>
#include <QFileSystemModel>
#include <QPushButton>

class OutputColumnWidget : public QWidget
{
    Q_OBJECT

private:
    QTreeView* outputTree;
    QFileSystemModel* fileSystemModel;
    QPushButton* executeButton;

public:
    OutputColumnWidget(QWidget *parent = nullptr);

private:
    void setupUI();

private slots:
    void selectedFolder(QString folder);

public slots:
    void readyState(bool enabled);

signals:
    void folderChanged(QString folder);
};

#endif // OUTPUTCOLUMNWIDGET_H
