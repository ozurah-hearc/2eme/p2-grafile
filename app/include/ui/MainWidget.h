#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/ui/MainWidget.h"
 * @brief       Header file, for the class "app/src/ui/MainWidget.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <QWidget>

#include "ui/MainWindow.h"
#include "ui/GrafileWidget.h"
#include "ui/InputColumnWidget.h"

/**
 * @brief This is the widget that will be used inside the mainwindow.
 */
class MainWidget : public QWidget
{
    Q_OBJECT

private:
    /**
    * @brief Reference to the Grafile widget
    */  
    GrafileWidget* _grafileWidget;
    /**
    * @brief Reference to the InputColumn widget
    * It contains the loaded files and the execute button
    */  
    InputColumnWidget* inputColumnWidget;

    QPushButton* executeButton;
    QPushButton* cancelExecuteButton;

    bool isInputColumnWidgetDataLoaded;

public:
    explicit MainWidget(MainWindow *parent = nullptr);

    /**
    * @brief Get the Grafile widget
    * @return the Grafile widget.
    */
    GrafileWidget* grafileWidget();

private:
    /**
    * @brief Create the Grafile widget UI part and connect components.
    */
    void setupUI();

    /**
     * @brief It will setup the source of the graph and execute it.
     * It will also toggle the "execute/cancel graph" buttons.
     */
    void executeGraph();
    /**
     * @brief It will cancel the execution of graph by disabled the "compute" of each nodes.
     * It will also toggle the "execute/cancel graph" buttons.
     * @remark Nodes that have already started their compute will finished it.
     */
    void cancelGraph();

    /**
     * @brief toggle the "execute / cancel" graph button according the state of the graph (is already executed) and
     * enabled them if data are loaded in the input column widget.
     */
    void toggleExecutionButton();
    /**
     * @brief action when getting a state from the InputColumnWidget, about the loading of his data.
     * @param state The actual state of the data loading.
     */
    void onInputColumnWidgetDataLoaded(bool state);
    /**
     * @brief onGraphStarted action when getting the graph execution state changed.
     * @param state The actual state of the graph execution.
     */
    void onGraphExecutionChanged(bool state);
};
