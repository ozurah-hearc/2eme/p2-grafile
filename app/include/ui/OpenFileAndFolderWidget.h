#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/ui/OpenFileAndFolderWidget.h"
 * @brief       Header file, for the class "app/src/ui/OpenFileAndFolderWidget.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QFileDialog>

/**
 * @brief Widget to open files or folder separatly.
 * You can choose multiple source, or clean the selected elements.
 * 
 * When the opened elements change, an event is rised with the path of opened elements.
 */
class OpenFileAndFolderWidget : public QWidget
{
    Q_OBJECT

public:

    OpenFileAndFolderWidget(QWidget *parent = nullptr);

    /**
     * @brief Get all (files and folders) opened elements in this widget.
     * 
     * @return QStringList 
     */
    QStringList getOpenedElements();
    /**
     * @brief Set the Opened Elements of this widget.
     * The files and the folders will be separated in this method.
     * @remark To avoid issue with folder, the path should end with '/' or '\' or '[/\](.).'.
     *         because if the folder doesn't exist, it will be interpreted as a file.
     * @param elements files and folders.
     */
    void setOpenedElements(QStringList elements);

signals:
    /**
     * @brief emitted when the opened elements change.
     * 
     * @param openedElements All elements opened in this widget (the result is the same as getOpenedElements()).
     */
    void openedElementsChange(QStringList openedElements);


protected:
    /**
     * @brief Default message when no element are opened.
     */
    const QString NOTHING_OPENED = "No element openend";

    /**
     * @brief Button to open files.
     */
    QPushButton* btnOpenFiles = nullptr;
    /**
     * @brief Button to open folders.
     */
    QPushButton* btnOpenFolders = nullptr;
    /**
     * @brief Button to remove each opened files.
     */
    QPushButton* btnCleanFiles = nullptr;
    /**
     * @brief Button to remove each opened folders.
     */
    QPushButton* btnCleanFolders = nullptr;

    /**
     * @brief Label to show the opened elements.
     */
    QLabel* lblOpenedElements = nullptr;

    /**
     * @brief Opened files.
     */
    QStringList *openedFiles;
    /**
     * @brief Opened folders.
     */
    QStringList *openedFolders;

    /**
     * @brief Open a file dialog to choose files.
     */
    QFileDialog *dialogFiles = nullptr;
    /**
     * @brief Open a file dialog to choose folders.
     */
    QFileDialog *dialogFolders = nullptr;

    /**
     * @brief update the label with the opened elements.
     */
    void updateLabel();

    /**
     * @brief Memory of the previous path of the file dialog.
     */
    QString openLocation;
    /**
     * @brief Update the location of the file dialog (file and folder) at the specified path.
     * @param openedSrc The path to set (only the first value will be used).
     * @remark openedSrc is a string list to allows to give the result of an open dialog without verification.
     */
    void updateOpenLocation(QStringList openedSrc);
};




