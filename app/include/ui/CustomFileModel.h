#ifndef CUSTOMFILEMODEL_H
#define CUSTOMFILEMODEL_H

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/ui/CustomFileModel.h"
 * @brief       Header file, for the class "app/src/ui/CustomFileModel.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <QStandardItemModel>
#include <QFileInfo>
#include <QPromise>

typedef struct CustomFileEntry {
    QFileInfo fileInfo;
    QStandardItem* parent;
} CustomFileEntry ;

/**
 * @brief Custom file model supporting asynchronous fetching of child elements.
 *
 */
class CustomFileModel : public QStandardItemModel
{
private:
    /**
    * @brief Root item of the model.
    */
    QStandardItem *rootItem;

    /**
    * @brief Icon for directories.
    */
    QIcon dirIcon;

    /**
    * @brief Icon for files.
    */
    QIcon fileIcon;

    /**
     * @brief Files loaded insinde this model.
     * It contains the filename and the full path to the file
     */
    QList<QFileInfo> *populateFiles;

public:
    CustomFileModel(QObject* parent = 0);

    /**
    * @brief Add a directory to the model and fetch it's children.
    */
    QFutureWatcher<void>* addDirectory(QString path);

    /**
    * @brief Populate a directory. 
    * @note Should be called asynchronously.
    */
    void populateDirectory(QPromise<void> &promise, CustomFileEntry root);

    /**
     * @brief Get the populated files (each loaded files with their full path)
     * 
     * @return QList<QFileInfo>* 
     */
    QList<QFileInfo> *getPopulateFiles();
};

#endif // CUSTOMFILEMODEL_H
