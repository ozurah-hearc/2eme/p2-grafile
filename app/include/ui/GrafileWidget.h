#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/ui/GrafileWidget.h"
 * @brief       Header file, for the class "app/src/ui/GrafileWidget.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <QWidget>
#include <nodes/DataModelRegistry>
#include <nodes/FlowScene>

using QtNodes::DataModelRegistry;

class StartNode;
class EndNode;
/**
 * @brief The widget that contains the NodeEditor UI.
 */
class GrafileWidget : public QWidget
{
    Q_OBJECT

signals:
    /**
     * @brief onEnabledComputeChange Event to notify the compute enabled for nodes have changed
     * @param isComputed The new value for execute compute logic
     * @remark It's a retransmission of the same event from the "start node"
     */
    void eventEnabledComputeChange(bool isComputed);

private:
    QtNodes::FlowScene *scene;

    /**
     * @brief m_connection Signal reference of the start eventEnabledComputeChange
     */
    QMetaObject::Connection connectionEventStart;
    /**
     * @brief m_connection Signal reference of the end eventEnabledComputeChange
     */
    QMetaObject::Connection connectionEventEnd;

    /**
     * @brief reference on the start node, to easly cancel the graph execution.
     */
    StartNode *start;
    /**
     * @brief reference on the start node, to detect end of execution.
     */
    EndNode *end;

public:
    GrafileWidget(QWidget *parent);

    /**
     * @brief Register all availibles nodes to let the user use them.
     *
     * @return std::shared_ptr<DataModelRegistry>
     */
    std::shared_ptr<DataModelRegistry> registerDataModels();

    /**
     * @brief is the graph is actually executed
     * @return
     */
    bool isGraphStarted();
    /**
     * @brief If the graphe is valid, perform the exeuction of the graphe with the specified source as entry point.
     *
     * @param sourceFiles files (path + filename) to use inside of the graph.
     * @remark We want a copy of the source because if they changed, it will not interfere with the current execution of the graph
     */
    void executeGraph(QStringList sourceFiles);
    /**
     * @brief It will cancel the execution of graph by disabled the "compute" of each nodes.
     * @remark Nodes that have already started their compute will finished it.
     */
    void cancelExecuteGraph();
    /**
     * @brief Verify if the graph is valid and can be executed.
     * Validty conditions :
     *  - 1 unique start node;
     *  - 1 unique end node;
     *  - All nodes had their validationState to valid.
     *
     * @return bool is the graph can be executed.
     */
    bool verifyGraphValidity();

    /**
     * @brief Load a graph from a JSON ".flow" file (NodeEditor structure format).
     *
     */
    void loadGraph();

    /**
     * @brief Save a graph to a JSON ".flow" file (NodeEditor structure format).
     *
     */
    void saveGraph();
};
