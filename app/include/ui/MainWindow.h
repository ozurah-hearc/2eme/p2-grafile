#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/ui/MainWindow.h"
 * @brief       Header file, for the class "app/src/ui/MainWindow.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <QMainWindow>

#include "ui/AboutWindow.h"

/**
 * @brief The main window of the Grafile Project.
 * 
 */
class MainWindow : public QMainWindow
{
private:
    QMenu* editMenu;
    QMenu* helpMenu;
    QAction* saveGraphAction;
    QAction* loadGraphAction;
    QAction* aboutAction;
    AboutWindow* aboutWindow;

public:
    explicit MainWindow(QWidget *parent = nullptr);

private:
    /**
     * @brief Create the UI of the main window.
     */
    void setupUI();
    /**
     * @brief Create the menu bar of the main window.
     */
    void setupMenubar();

    /**
     * @brief Open about window
     */
    void showAbout();
};
