#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/TestInNode.h"
 * @brief       Header file, for the class "app/src/TestInNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "includes_grafileData.h"
#include "filter/FilterNodeModel.h"

/**
 * @brief A node to test the values passed as input.
 * The passed value with be print in the console.
 */
class TestInNode : public GrafileNodeModel
{
public:

    TestInNode();

    /**
     * @brief Print the inputs values to the console with qDebug().
     * Only connected input will be displayed.
     * @remark this compute is forced (will be called even if the graph isn't running).
     */
    virtual void compute() override;

protected:
    /**
     * @brief Convert the inputs to the "IN VARS" (and verify if input is connected).
     * * Every time a input is changed, this method will be called.
     */
    virtual void convertInputsToVars() override;

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    GrafileDataBoolean inBool;
    GrafileDataComparator inComp;
    GrafileDataDateTime inDateTime;
    GrafileDataDecimal inDecimal;
    GrafileDataInteger inInt;
    GrafileDataStringFile inStringFile;
    GrafileDataText inText;

    /* ***************************** *
     * ***** Is IN connected ******* *
     * ***************************** */

    bool inBoolConnected = false;
    bool inCompConnected = false;
    bool inDateTimeConnected = false;
    bool inDecimalConnected = false;
    bool inIntConnected = false;
    bool inStringFileConnected = false;
    bool inTextConnected = false;
};
