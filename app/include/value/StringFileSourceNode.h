#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/value/StringFileSourceNode.h"
 * @brief       Header file, for the class "app/src/value/StringFileSourceNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "value/ValueSourceNodeModel.h"
#include "data/GrafileDataStringFile.h"
#include "ui/OpenFileAndFolderWidget.h"

#include <QStringList>

/**
 * @brief Class for StringFile source node.
 * It will contain a widget to choose the StringFile value.
 * This node contains no input port, and a single GrafileDataStringFile output port.
 */
class StringFileSourceNode : public ValueSourceNodeModel
{
public:
    StringFileSourceNode();

    QWidget *embeddedWidget() override;

    std::shared_ptr<NodeData> outData(PortIndex) override;

    /**
     * @brief Save this node with it's value to a JSON (according the NodeEditor ".flow" format).
     * @return QJsonObject 
     */
    QJsonObject save() const override;
    /**
     * @brief Restore this node with it's value from a JSON (according the NodeEditor ".flow" format).
     * @param p 
     */
    void restore(QJsonObject const &p) override;

protected:
    /**
     * @brief Get all files of the _values and convert them to a QStringFile.
     * This method is called to transmite the opened files to the _inputWidget during the restore method.
     * 
     * @return QStringList 
     */
    QStringList valuesToListString();

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    OpenFileAndFolderWidget* _inputWidget = nullptr;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    QList<StringFile> _values;
};
