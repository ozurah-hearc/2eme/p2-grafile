#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/value/DecimalSourceNode.h"
 * @brief       Header file, for the class "app/src/value/DecimalSourceNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "value/ValueSourceNodeModel.h"
#include "data/GrafileDataDecimal.h"

#include <QLineEdit>

/**
 * @brief Class for double source node.
 * It will contain a widget to choose the double value.
 * This node contains no input port, and a single GrafileDataDecimal output port.
 */
class DecimalSourceNode : public ValueSourceNodeModel
{
public:
    DecimalSourceNode();

    QWidget *embeddedWidget() override;

    std::shared_ptr<NodeData> outData(PortIndex) override;

    /**
     * @brief Save this node with it's value to a JSON (according the NodeEditor ".flow" format).
     * @return QJsonObject 
     */
    QJsonObject save() const override;
    /**
     * @brief Restore this node with it's value from a JSON (according the NodeEditor ".flow" format).
     * @param p 
     */
    void restore(QJsonObject const &p) override;

protected:

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    QLineEdit* _inputWidget = nullptr;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    double _value = 0.0;
};
