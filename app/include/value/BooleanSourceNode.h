#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/value/BooleanSourceNode.h"
 * @brief       Header file, for the class "app/src/value/BooleanSourceNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "value/ValueSourceNodeModel.h"
#include "data/GrafileDataBoolean.h"

#include <QPushButton>

/**
 * @brief Class for boolean source node.
 * It will contain a widget to choose the boolean value.
 * This node contains no input port, and a single GrafileDataBoolean output port.
 */
class BooleanSourceNode : public ValueSourceNodeModel
{
public:
    BooleanSourceNode();

    QWidget *embeddedWidget() override;

    std::shared_ptr<NodeData> outData(PortIndex) override;

    /**
     * @brief Save this node with it's value to a JSON (according the NodeEditor ".flow" format).
     * @return QJsonObject 
     */
    QJsonObject save() const override;
    /**
     * @brief Restore this node with it's value from a JSON (according the NodeEditor ".flow" format).
     * @param p 
     */
    void restore(QJsonObject const &p) override;

protected:
    /**
     * @brief Convert the _value as a string for the displayed text in the widget.
     * It is also called by the save and restore methods.
     * @return QString 
     */
    QString valToString();

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    QPushButton* _inputWidget = nullptr;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    bool _value = false;
};
