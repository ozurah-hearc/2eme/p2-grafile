#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/value/TextSourceNode.h"
 * @brief       Header file, for the class "app/src/value/TextSourceNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "value/ValueSourceNodeModel.h"
#include "data/GrafileDataText.h"

#include <QTextEdit>

/**
 * @brief Class for string source node.
 * It will contain a widget to choose the string value.
 * This node contains no input port, and a single GrafileDataText output port.
 */
class TextSourceNode : public ValueSourceNodeModel
{
public:
    TextSourceNode();

    QWidget *embeddedWidget() override;

    std::shared_ptr<NodeData> outData(PortIndex) override;

    /**
     * @brief Save this node with it's value to a JSON (according the NodeEditor ".flow" format).
     * @return QJsonObject 
     */
    QJsonObject save() const override;
    /**
     * @brief Restore this node with it's value from a JSON (according the NodeEditor ".flow" format).
     * @param p 
     */
    void restore(QJsonObject const &p) override;
    
protected:

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    QTextEdit* _inputWidget = nullptr;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    QString _value = "sample text";
};
