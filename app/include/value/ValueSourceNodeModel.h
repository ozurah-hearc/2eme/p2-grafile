#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/value/ValueSourceNodeModel.h"
 * @brief       Header file, for the class "app/src/value/ValueSourceNodeModel.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "GrafileNodeModel.h"

/**
 * @brief Base class for "value" source node.
 * It will contain a widget to choose the value.
 * This type of nodes will contains no input port, and a single GrafileData output port.
 */
class ValueSourceNodeModel : public GrafileNodeModel
{
public:
    ValueSourceNodeModel();
    ValueSourceNodeModel(QString name);
};
