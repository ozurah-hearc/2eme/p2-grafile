#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/value/DateTimeSourceNode.h"
 * @brief       Header file, for the class "app/src/value/DateTimeSourceNode.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeDataModel>
#include "value/ValueSourceNodeModel.h"
#include "data/GrafileDataDateTime.h"

#include <QDateTimeEdit>

/**
 * @brief Class for QDateTime source node.
 * It will contain a widget to choose the date and the time value.
 * This node contains no input port, and a single GrafileDataDateTime output port.
 */
class DateTimeSourceNode : public ValueSourceNodeModel
{
public:
    DateTimeSourceNode();

    QWidget *embeddedWidget() override;

    std::shared_ptr<NodeData> outData(PortIndex) override;

    /**
     * @brief Save this node with it's value to a JSON (according the NodeEditor ".flow" format).
     * @return QJsonObject 
     */
    QJsonObject save() const override;
    /**
     * @brief Restore this node with it's value from a JSON (according the NodeEditor ".flow" format).
     * @param p 
     */
    void restore(QJsonObject const &p) override;

protected:
    /**
     * @brief format for the save-restore of the date and time.
     */
    const QString DATETIME_FORMAT_SAVED = "yyyy-MM-dd hh:mm:ss";

    /* *************************** *
     * ********* IN VARS ********* *
     * *************************** */

    QDateTimeEdit* _inputWidget = nullptr;

    /* *************************** *
     * ********* OUT VARS ******** *
     * *************************** */

    QDateTime _value = QDateTime::currentDateTime();
};
