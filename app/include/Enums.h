#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/Enums.h"
 * @brief       A class to store and manage enums, there is no attached .cpp file
 *
 * @copyright   Copyright (c) 2022
 * 
 * @code {.cpp}
 *  // Example of use
 *  #include "Enums.h"
 *
 *  CompareOperatorType op = CompareOperatorType::Equals;
 *  QString opString = CompareOperatorToString[op]; // will be "="
 *  CompareOperatorType op2 = StringToCompareOperator[">="];
 * @endcode
 */

#include <QHash>

#include <QDebug>

/**
 * @brief This enum allows to choose the type of the comparison operator.
 */
enum class CompareOperatorType
{
    EQUALS,
    GREATHER_THAN,
    LOWER_THAN,
    LOWER_EQUALS_THAN,
    GREATHER_EQUALS_THAN,
    NOT_EQUALS
};

/**
 * @brief Get the Enum CompareOperatorType as string.
 *  The string will be the operator like "=" or ">=".
 */
static QHash<CompareOperatorType, QString> CompareOperatorToString = {
    {CompareOperatorType::EQUALS, "="},
    {CompareOperatorType::GREATHER_THAN, ">"},
    {CompareOperatorType::LOWER_THAN, "<"},
    {CompareOperatorType::LOWER_EQUALS_THAN, "<="},
    {CompareOperatorType::GREATHER_EQUALS_THAN, ">="},
    {CompareOperatorType::NOT_EQUALS, "!="}};

/**
 * @brief Get the Enum CompareOperatorType from string.
 * The string must be the operator like "=" or ">=".
 */
static QHash<QString, CompareOperatorType> StringToCompareOperator = {
    {"=", CompareOperatorType::EQUALS},
    {">", CompareOperatorType::GREATHER_THAN},
    {"<", CompareOperatorType::LOWER_THAN},
    {"<=", CompareOperatorType::LOWER_EQUALS_THAN},
    {">=", CompareOperatorType::GREATHER_EQUALS_THAN},
    {"!=", CompareOperatorType::NOT_EQUALS}};

/**
 * @brief Allors to display the enum in the qDebug().
 */
inline QDebug operator<<(QDebug dbg, CompareOperatorType enumVal)
{
  return dbg << CompareOperatorToString[enumVal];
}
