#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/GrafileData.h"
 * @brief       Template class for the "collection data source" of the project
 * @remark      /!\ no .cpp because it's a template class
 * 
 * @copyright   Copyright (c) 2022
 */

#include <nodes/NodeData>
#include <QVector>

using QtNodes::NodeDataType;
using QtNodes::NodeData;

/**
 * @brief Class for the data of the app.
 * The app will deal with data stored as collection.
 *
 * @tparam T Type of the data
 */
template <typename T>
class GrafileData : public NodeData
{
protected:
    /**
     * @brief Collection of data
     */
    QVector<T> _list;
    /**
     * @brief Type of the data
     */
    NodeDataType _type;

public:

    GrafileData(QVector<T> list, NodeDataType type) : _list(list), _type(type)
    {
    }

    GrafileData(QVector<T> list) : GrafileData(list, NodeDataType{"default", "Default"})
    {
    }

    GrafileData(T element) : GrafileData(QVector<T>(1, element))
    {
    }

    GrafileData()
    {
    }

    /**
     * @brief Get the data collection
     * @return 
     */
    QVector<T>& getList()
    {
        return this->_list;
    }

    /**
     * @brief Get a copy of the data collection
     * @return 
     */
    QVector<T> getCopyList()
    {
        return this->_list;
    }

    /**
     * @brief Clear the data collection
     */
    void clear()
    {
        this->_list.clear();
    }

    /**
     * @brief Set the collection of data
     * 
     * @param list collection to set
     */
    void setList(const QVector<T>& list) const
    {
        return this->_list = list;
    }

    /**
     * @brief Get the value of the collection at the specified index
     * 
     * @param index index of the data to get
     * @return T specified data
     * @remark There is no check if the specified index is out of bound of the collection
     */
    T getValueAt(int index)
    {
        return this->_list[index];
    }

    /**
     * @brief Set the value of the collection at the specified index
     * 
     * @param index index of the data to set
     * @param value value to set
     * @remark There is no check if the specified index is out of bound of the collection
     */
    void setValueAt(int index, T value)
    {
        this->_list[index] = value;
    }

    /**
     * @brief Get first value of the collection
     * @return T first data
     * This is a shortcut for getValueAt(0), it is used when the GrafileData store a single value
     * @remark There is no check if the collection contains a value at the index 0, consider using hasValues() before
     */
    T getValue()
    {
        return this->_list[0];
    }

    /**
     * @brief Set first value of the collection
     * @param value value to set
     * This is a shortcut for setValueAt(0, value), it is used when the GrafileData store a single value
     */
    void setValue(T value)
    {
        if (!this->hasValues())
        {
            this->_list.append(value);
        }
        else
        {
            this->_list[0] = value;
        }
    }

    /**
     * @brief Get the type of the data stored in this collection
     * 
     * @return NodeDataType 
     */
    NodeDataType type() const override
    {
        return this->_type;
    }

    /**
     * @brief Is at least one value present in the object ?
     * 
     * @return true if collection is not empty
     */
    bool hasValues() const
    {
        return this->_list.size() > 0;
    }
};
