#pragma once

/**
 * @file includes_grafileData.h
 * @brief This file is used to simplify the include of each Grafile Data.
 */

#include "data/GrafileDataBoolean.h"
#include "data/GrafileDataComparator.h"
#include "data/GrafileDataDateTime.h"
#include "data/GrafileDataDecimal.h"
#include "data/GrafileDataInteger.h"
#include "data/GrafileDataStringFile.h"
#include "data/GrafileDataText.h"
