#pragma once

/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/include/Utility.h"
 * @brief       Header file, for the class "app/src/ui/MainWindow.cpp"
 *
 * @copyright   Copyright (c) 2022
 */

#include <QString>

/**
 * @brief Class with utility methods. This class contains only statics methods.
 */
class Utility
{
public:
    Utility() = delete; // Only statics methods, no need to create an instance.

    /**
     * @brief The path point to a directory ?
     * 
     * @param path Path to check.
     * @return true if : the path exists and it's a directory; if the path end with '/' or '\' or '[/\](.).'.
     * @return false otherwise.
     */
    static bool isDir(QString path);
    /**
     * @brief Copy a file to another location.
     * If folder location doesn't exists, it will create.
     * The file isn't overwrite if already exists (return will be false)
     * 
     * @param src File to copy.
     * @param to Path where copy (with the filename and ext).
     * @return is the copy was successful.
     */
    static bool copyFile(QString src, QString to);
    /**
     * @brief Delete a file (move it to the thrash).
     * Only the file will be deleted, the path will be kept.
     * 
     * @param file file (with path) to delete.
     * @return is the deletion was successful (or file doesn't exist).
     */
    static bool deleteFile(QString file);
    /**
     * @brief Create a folder if required.
     * @return is the folder was created (or already exists).
     */
    static bool createDir(QString path);
};
