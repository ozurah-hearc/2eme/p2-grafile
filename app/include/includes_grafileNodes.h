#pragma once

/**
 * @file includes_grafileNodes.h
 * @brief This file is used to simplify the include of each Grafile nodes.
 */

#include "TestInNode.h"

#include "filter/AndBoolFilterNode.h"
#include "filter/FileContainsFilterNode.h"
#include "filter/FileExtFilterNode.h"
#include "filter/FileNameFilterNode.h"
#include "filter/FilePathFilterNode.h"
#include "filter/FileSizeFilterNode.h"
#include "filter/FilterNodeModel.h"
#include "filter/IsFolderFilterNode.h"
#include "filter/NotBoolFilterNode.h"
#include "filter/OrBoolFilterNode.h"

#include "treatment/ApplyTreatmentNode.h"
#include "treatment/CompressionTreatmentNode.h"
#include "treatment/CopyTreatmentNode.h"
#include "treatment/DateTimeAnalysisTreatmentNode.h"
#include "treatment/DeleteTreatmentNode.h"
#include "treatment/EndNode.h"
#include "treatment/MessageTreatmentNode.h"
#include "treatment/MoveTreatmentNode.h"
#include "treatment/NameAnalysisTreatmentNode.h"
#include "treatment/PreviewTreatmentNode.h"
#include "treatment/StartNode.h"
#include "treatment/TreatmentNodeModel.h"

#include "value/BooleanSourceNode.h"
#include "value/CompareOperationSourceNode.h"
#include "value/DateTimeSourceNode.h"
#include "value/DecimalSourceNode.h"
#include "value/IntegerSourceNode.h"
#include "value/StringFileSourceNode.h"
#include "value/TextSourceNode.h"
#include "value/ValueSourceNodeModel.h"
