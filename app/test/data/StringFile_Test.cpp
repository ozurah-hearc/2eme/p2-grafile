#include "gtest/gtest.h"
#include "data/StringFile.h"
#include "temporarywork.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>

TEST(StringFile, constructor_origineCurrent)
{
    // If the constructor is called with 1 argument, the origine and the current path will had the same values
    QString val = "location";
    StringFile sf(val);

    EXPECT_EQ(sf.getOriginalPath(), val);
    EXPECT_EQ(sf.getCurrentPath(), val);

    // If changing the origine, the current path should not be changed (non dependency between them)
    sf.setCurrentPath("another");
    EXPECT_NE(sf.getOriginalPath(), sf.getCurrentPath());
}

TEST(StringFile, toJson_fromJson_singleDefaultObject)
{
    StringFile sf;

    auto json = sf.toJson();

    StringFile sf2;
    sf2.fromJson(json);

    EXPECT_EQ(sf, sf2);
}

TEST(StringFile, toJson_fromJson_singleObject)
{
    // All possible attributes are changed to verify the restoration of the "to json" to "from json"
    StringFile sf("current", "origine", "final");
    sf.setNeedDelete(true);
    sf.setNeedMove(true);

    auto json = sf.toJson();

    StringFile sf2;
    sf2.fromJson(json);

    EXPECT_EQ(sf, sf2);
}

TEST(StringFile, toJson_fromJson_collection)
{
    StringFile sf("actual", "origine");
    StringFile sf2("actual and origin");

    QList<StringFile> collection1 = {sf, sf2};

    auto json = StringFile::toJson(collection1);

    auto collection2 = StringFile::fromJson(json);

    EXPECT_EQ(collection1, collection2);
}

TEST(StringFile, operatorEquality_equals)
{
    StringFile sf;
    StringFile sf2;

    EXPECT_TRUE(sf == sf2);

    sf.setOriginalPath("origine");
    sf2.setOriginalPath("origine");
    EXPECT_TRUE(sf == sf2);

    sf.setCurrentPath("actual");
    sf2.setCurrentPath("actual");
    EXPECT_TRUE(sf == sf2);

    sf.setFinalPath("final");
    sf2.setFinalPath("final");
    EXPECT_TRUE(sf == sf2);

    sf.setNeedDelete(true);
    sf2.setNeedDelete(true);
    EXPECT_TRUE(sf == sf2);

    sf.setNeedMove(true);
    sf2.setNeedMove(true);
    EXPECT_TRUE(sf == sf2);
}

TEST(StringFile, operatorEquality_notEquals)
{
    {
        StringFile sf;
        StringFile sf2;

        sf.setOriginalPath("origine");
        sf2.setOriginalPath("another");
        EXPECT_FALSE(sf == sf2);
    }

    {
        StringFile sf;
        StringFile sf2;

        sf.setCurrentPath("actual");
        sf2.setCurrentPath("another");
        EXPECT_FALSE(sf == sf2);
    }

    {
        StringFile sf;
        StringFile sf2;

        sf.setFinalPath("final");
        sf2.setFinalPath("another");
        EXPECT_FALSE(sf == sf2);
    }

    {
        StringFile sf;
        StringFile sf2;

        sf.setNeedDelete(true);
        sf2.setNeedDelete(false);
        EXPECT_FALSE(sf == sf2);
    }

    {
        StringFile sf;
        StringFile sf2;

        sf.setNeedMove(true);
        sf2.setNeedMove(false);
        EXPECT_FALSE(sf == sf2);
    }
}

TEST(StringFile, operatorStream)
{
    StringFile sf;

    sf.setOriginalPath("origine");
    sf.setCurrentPath("actual");
    sf.setFinalPath("final");
    sf.setNeedDelete(true);
    sf.setNeedMove(true);

    QString filename = TemporaryWork::getTempFile();
    QFile file(filename);

    // Save (out stream)
    if(!file.open(QIODevice::WriteOnly))
    {
        FAIL();
    }

    QDataStream out(&file);
    out << sf;

    file.close();

    // Load (in stream)
    StringFile sf2;

    if(!file.open(QIODevice::ReadOnly))
    {
        FAIL();
    }

    QDataStream in(&file);
    in >> sf2; // Stream

    EXPECT_EQ(sf, sf2);
}
