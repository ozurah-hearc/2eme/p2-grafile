#include "gtest/gtest.h"
#include "Utility.h"

#include <QFileInfo>

// /!\ To perform this test, the "UNIT_TEST_GRAFILE" folder available in the git must be copied to the root of the OS
//    (like C:/UNIT_TEST_GRAFILE on window; /UNIT_TEST_GRAFILE on linux)
// Else the test will failed

TEST(Utility, isDir_directories)
{
    EXPECT_TRUE(Utility::isDir("C:/aFolder/"));
    EXPECT_TRUE(Utility::isDir("C:/aFolder/."));
    EXPECT_TRUE(Utility::isDir("C:/aFolder/.."));
    EXPECT_TRUE(Utility::isDir("C:/aFolder\\"));
    EXPECT_TRUE(Utility::isDir("C:/aFolder\\."));
    EXPECT_TRUE(Utility::isDir("C:/aFolder\\.."));
    EXPECT_TRUE(Utility::isDir("/aFolder/"));
}

TEST(Utility, isDir_files)
{
    EXPECT_FALSE(Utility::isDir("C:/aFile"));
    EXPECT_FALSE(Utility::isDir("C:/aFile.txt"));
    EXPECT_FALSE(Utility::isDir("C:/aFile."));
}

TEST(Utility, createDir)
{
    QString TestFolderLocation = QFileInfo("/UNIT_TEST_GRAFILE/").absoluteFilePath(); // Where the existing files are placed (absolutepath will set the correct root)
    qDebug() << TestFolderLocation;

    EXPECT_TRUE(Utility::createDir(TestFolderLocation + "createdDir"));
}

/* DISABLED Because : CI doesn't had a trash, the test can only failed
TEST(Utility, deleteFile)
{
    QString TestFolderLocation = QFileInfo("/UNIT_TEST_GRAFILE/").absoluteFilePath(); // Where the existing files are placed (absolutepath will set the correct root)
    qDebug() << TestFolderLocation;

    EXPECT_TRUE(Utility::deleteFile(TestFolderLocation + "createdDir/aFile.txt"));
}
*/

TEST(Utility, copyFile)
{
    QString TestFolderLocation = QFileInfo("/UNIT_TEST_GRAFILE/").absoluteFilePath(); // Where the existing files are placed (absolutepath will set the correct root)
    qDebug() << TestFolderLocation;

    EXPECT_TRUE(Utility::copyFile(TestFolderLocation + "aFile.txt", TestFolderLocation + "createdDir/aFile.txt"));

    // Clean the file, for next tests
    QFile::remove(TestFolderLocation + "createdDir/aFile.txt");
}

/* DISABLED Because : CI doesn't had a trash, the test can only failed
TEST(Utility, fileDeleteCopySenquence)
{
    // We test the "delete, copy, copy, delete, delete" sequence to be sur we can create file if doesn't exists, same for the deletion

    QString TestFolderLocation = QFileInfo("/UNIT_TEST_GRAFILE/").absoluteFilePath(); // Where the existing files are placed (absolutepath will set the correct root)
    qDebug() << TestFolderLocation;

    QString fileToCopy = TestFolderLocation + "aFile.txt";
    QString where = TestFolderLocation + "createdDir/aFile.txt";

    EXPECT_TRUE(Utility::deleteFile(where));
    EXPECT_TRUE(Utility::copyFile(fileToCopy, where));
    EXPECT_FALSE(Utility::copyFile(fileToCopy, where)); // The file exists, the copy failed
    EXPECT_TRUE(Utility::deleteFile(where));
    EXPECT_TRUE(Utility::deleteFile(where));
}
*/
