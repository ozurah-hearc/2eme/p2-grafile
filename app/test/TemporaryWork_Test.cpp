#include "gtest/gtest.h"
#include "temporarywork.h"
#include <QFileInfo>

TEST(TemporaryWork_getTempFile, fileGet)
{
    auto file = TemporaryWork::getTempFile();
    EXPECT_FALSE(file.isEmpty());

    // We only got a path, the file should not exists
    EXPECT_FALSE(QFileInfo::exists(file));

    EXPECT_TRUE(TemporaryWork::isFileInTemp(file));
}

TEST(TemporaryWork_getTempFile, isFileInTemp)
{
    auto file = TemporaryWork::getTempFile();
    EXPECT_FALSE(file.isEmpty());

    EXPECT_TRUE(TemporaryWork::isFileInTemp(file));
}

TEST(TemporaryWork_getTempFile, temporaryFolderExistance)
{
    // This test will cover :
    //  private createTempAppFolder (by getTempFile)

    // When getting temp file, it will create the temporary folder if required
    auto file = TemporaryWork::getTempFile();

    EXPECT_TRUE(QFileInfo::exists(TemporaryWork::TEMP_FOLDER));
}

TEST(TemporaryWork_getTempFile, multipleFileGet)
{
    auto file1 = TemporaryWork::getTempFile();
    auto file2 = TemporaryWork::getTempFile();

    // The temp files names should be different
    EXPECT_NE(file1, file2);
}

TEST(TemporaryWork, cleanTempAppFolder)
{
    // Will create to be sure it exists
    auto file1 = TemporaryWork::getTempFile();
    EXPECT_TRUE(QFileInfo::exists(TemporaryWork::TEMP_FOLDER));

    // We delete it
    TemporaryWork::cleanTempAppFolder();
    EXPECT_FALSE(QFileInfo::exists(TemporaryWork::TEMP_FOLDER));
}

// displayMessageNoTempFile can't be tested : UI actions
