#include "gtest/gtest.h"
#include "treatment/CopyTreatmentNode.h"
#include "data/GrafileDataStringFile.h"
#include "temporarywork.h"

#include <QFileInfo>

using namespace std;

// /!\ To perform this test, the "UNIT_TEST_GRAFILE" folder available in the git must be copied to the root of the OS
//    (like C:/UNIT_TEST_GRAFILE on window; /UNIT_TEST_GRAFILE on linux)
// Else the test will failed

// /!\ There is no test for folders for the "CopyTreatmentNode" because they are unhandled for now
// If handled them in the the future, the test must be adapted ! /!\

TEST(CopyNode, to1Folder_fullTest)
{
    // Testing in, out, compute logic when node is enabled
    // This test will check the copy of a existing file, an unexisting file, a "moved" flagged file (from moveTreatment), a "deleted" flagged file (from deleteTreatment)

    // -- Test init --
    QString TestFolderLocation = QFileInfo("/UNIT_TEST_GRAFILE/").absoluteFilePath(); // Where the existing files are placed (absolutepath will set the correct root)
    qDebug() << TestFolderLocation;

    bool isEnable = true;

    StringFile srcSf1(TestFolderLocation+"aFile.txt"); // An existing file (should be created before the test)
    StringFile srcSf2(TestFolderLocation+"unexistant.txt");
    StringFile srcSf3(TestFolderLocation+"aFile.txt"); // Like if we got this one from the move treatment node
    srcSf3.setNeedMove(true);
    StringFile srcSf4(TestFolderLocation+"aFile.txt"); // Like if we got this one from the delete treatment node
    srcSf4.setNeedDelete(true);

    StringFile destSfFolder1(TestFolderLocation+"aFolder/");

    // We only copie the data; the test logic will control path (current and final) itself (no need to set them)
    StringFile resultSf1 = srcSf1;
    //srcSf2 is a file that doesn't exists, it will not been present in the output
    StringFile resultSf3 = srcSf3; // This one will be the same, and also copied (resultSf3B)
    StringFile resultSf3b = srcSf3; // The copy of the moved file
    resultSf3b.setNeedMove(false); // because it was copied, it have no move flag
    StringFile resultSf4 = srcSf4; // This one will be the same, because it is deleted (no need to make a copy)

    QList<StringFile> valFiles = {srcSf1, srcSf2, srcSf3, srcSf4};
    QList<StringFile> valDest = {destSfFolder1};
    // = (copy result in temp)
    QList<StringFile> valOutRes = {resultSf1, resultSf3, resultSf3b, resultSf4};

    // -- Perform test --
    CopyTreatmentNode node;

    shared_ptr<GrafileDataStringFile> inSrc
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));
    shared_ptr<GrafileDataStringFile> inDest
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valDest
                    ));

    // 0 is for enabled port
    node.setInData(inSrc, 1);
    node.setInData(inDest, 2);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outData = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));

    EXPECT_EQ(outData->getList().size(), valOutRes.size());

    // -- Test for paths (current, original, final) --
    StringFile testedFile = outData->getValueAt(0);
    // Is the file have the reference on the source ?
    EXPECT_EQ(testedFile.getOriginalPath(), srcSf1.getCurrentPath());

    // Is the file copied to temp.
    EXPECT_EQ(TemporaryWork::isFileInTemp(testedFile.getCurrentPath()), true);

    // Is the had correct destination.
    EXPECT_EQ(QFileInfo(testedFile.getFinalPath()).absolutePath(), QFileInfo(destSfFolder1.getCurrentPath()).absolutePath());

    // -- Test temp. copy "no data lost" --
    EXPECT_EQ(QFileInfo(testedFile.getCurrentPath()).size(), QFileInfo(srcSf1.getCurrentPath()).size());

    // -- Test for flags (move/delete) --
    // Is the moved file doesn't change
    testedFile = outData->getValueAt(1);

    EXPECT_EQ(testedFile, valOutRes[1]);

    // Is the copy of the moved file doesn't contains move flag
    testedFile = outData->getValueAt(2);
    EXPECT_FALSE(testedFile.getNeedMove());

    // Is the deleted file doesn't change
    testedFile = outData->getValueAt(3);
    EXPECT_EQ(testedFile, valOutRes[3]);
}

TEST(CopyNode, toMultipleFolder)
{
    // Testing in, out, compute logic when node is enabled
    // This test will check the copy to multiple location (not checking flagged files, because the previous test did it)
    // note : Only by verifying the final path

    // -- Test init --
    QString TestFolderLocation = QFileInfo("/UNIT_TEST_GRAFILE/").absoluteFilePath(); // Where the existing files are placed (absolutepath will set the correct root)
    qDebug() << TestFolderLocation;
    bool isEnable = true;

    StringFile srcSf1(TestFolderLocation+"aFile.txt"); // An existing file (should be created before the test)
    StringFile srcSf2(TestFolderLocation+"unexistant.txt");

    StringFile destSfFolder1(TestFolderLocation+"aFolder/");
    StringFile destSfFolder2(TestFolderLocation+"somePlace/");

    QString resultFinalPath1 = destSfFolder1.getCurrentPath() + "aFile.txt";
    QString resultFinalPath2 = destSfFolder2.getCurrentPath() + "aFile.txt";

    QList<StringFile> valFiles = {srcSf1, srcSf2};
    QList<StringFile> valDest = {destSfFolder1, destSfFolder2};
    // = (copy result in temp)
    QList<QString> valOutResFinalPath = {resultFinalPath1, resultFinalPath2};

    // -- Perform test --
    CopyTreatmentNode node;

    shared_ptr<GrafileDataStringFile> inSrc
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));
    shared_ptr<GrafileDataStringFile> inDest
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valDest
                    ));

    // 0 is for enabled port
    node.setInData(inSrc, 1);
    node.setInData(inDest, 2);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outData = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));

    EXPECT_EQ(outData->getList().size(), valOutResFinalPath.size());

    for (int i = 0; i < valOutResFinalPath.size(); i++)
    {
        EXPECT_EQ(outData->getValueAt(i).getFinalPath(), valOutResFinalPath[i]);
    }
}

TEST(CopyNode, disabled)
{
    // Testing in, out, compute logic when node is disabled
    // If the node is disabled, in files will be the same as out files

    // -- Test init --
    QString TestFolderLocation = QFileInfo("/UNIT_TEST_GRAFILE/").absoluteFilePath(); // Where the existing files are placed (absolutepath will set the correct root)
    qDebug() << TestFolderLocation;

    bool isEnable = false;

    StringFile srcSf1(TestFolderLocation+"aFile.txt"); // An existing file (should be created before the test)
    StringFile srcSf2(TestFolderLocation+"unexistant.txt");
    StringFile srcSf3(TestFolderLocation+"aFile.txt"); // Like if we got this one from the move treatment node (that means it is in the temp folder)
    srcSf3.setNeedMove(true);
    StringFile srcSf4(TestFolderLocation+"aFile.txt"); // Like if we got this one from the delete treatment node
    srcSf4.setNeedDelete(true);

    StringFile destSfFolder1(TestFolderLocation+"aFolder/");

    QList<StringFile> valFiles = {srcSf1, srcSf2, srcSf3, srcSf4};
    QList<StringFile> valDest = {destSfFolder1};
    // = (copy result in temp)
    QList<StringFile> valOutRes = {srcSf1, srcSf2, srcSf3, srcSf4};

    // -- Perform test --
    CopyTreatmentNode node;

    shared_ptr<GrafileDataStringFile> inSrc
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));
    shared_ptr<GrafileDataStringFile> inDest
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valDest
                    ));

    // 0 is for enabled port
    node.setInData(inSrc, 1);
    node.setInData(inDest, 2);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outData = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));

    EXPECT_EQ(outData->getList(), valOutRes);
}
