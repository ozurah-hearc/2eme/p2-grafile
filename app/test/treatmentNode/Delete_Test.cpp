#include "gtest/gtest.h"
#include "treatment/DeleteTreatmentNode.h"
#include "data/GrafileDataStringFile.h"

#include <QFileInfo>

using namespace std;


TEST(DeleteNode, enabled)
{
    // Testing in, out, compute logic when node is enabled
    // This test will check if "delete" flag is correctly setted, it will also check if files with final paths are correctly handled.
    // A file with final path means it was manipulated by another treatment node (copy/move),
    //   so the element we want to delete is the result of the manipulation.

    // note : a deleted flagged file will not having final path

    // -- Test init --
    bool isEnable = true;

    StringFile srcSf1("aFile.txt");
    StringFile srcSf2("anotherFile.txt");
    srcSf2.setFinalPath("afterManipulation.ext");

    StringFile resultSf1 = srcSf1;
    srcSf1.setNeedDelete(true);
    StringFile resultSf2 = srcSf2;
    srcSf2.setNeedDelete(true);

    QList<StringFile> valFiles = {srcSf1, srcSf2};
    // = (delete result elements value)
    QList<QString> valOutResSourcePath = {"aFile.txt", "afterManipulation.ext"}; // current and origine path

    // -- Perform test --
    DeleteTreatmentNode node;

    shared_ptr<GrafileDataStringFile> inSrc
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));

    // 0 is for enabled port
    node.setInData(inSrc, 1);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outData = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));

    EXPECT_EQ(outData->getList().size(), valFiles.size());

    for (int i = 0; i < valFiles.size(); i++)
    {
        EXPECT_TRUE(outData->getValueAt(i).getNeedDelete());

        EXPECT_EQ(outData->getValueAt(i).getCurrentPath(), valOutResSourcePath[i]);
        EXPECT_EQ(outData->getValueAt(i).getOriginalPath(), valOutResSourcePath[i]);

        EXPECT_TRUE(outData->getValueAt(i).getFinalPath().isEmpty());
    }
}

TEST(DeleteNode, disabled)
{
    // Testing in, out, compute logic when node is disabled
    // If the node is disabled, in files will be the same as out files

    // -- Test init --
    QString TestFolderLocation = QFileInfo("/UNIT_TEST_GRAFILE/").absoluteFilePath(); // Where the existing files are placed (absolutepath will set the correct root)
    qDebug() << TestFolderLocation;

    bool isEnable = false;

    StringFile srcSf1("aFile.txt"); // An existing file (should be created before the test)
    StringFile srcSf2("anotherFile.txt");

    QList<StringFile> valFiles = {srcSf1, srcSf2};
    // = (delete result in temp)
    QList<StringFile> valOutRes = {srcSf1, srcSf2};

    // -- Perform test --
    DeleteTreatmentNode node;

    shared_ptr<GrafileDataStringFile> inSrc
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));

    // 0 is for enabled port
    node.setInData(inSrc, 1);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outData = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));

    EXPECT_EQ(outData->getList(), valOutRes);
}
