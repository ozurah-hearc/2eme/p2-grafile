#include "gtest/gtest.h"
#include "filter/FileNameFilterNode.h"
#include "data/GrafileDataBoolean.h"
#include "data/GrafileDataText.h"
#include "data/GrafileDataStringFile.h"

using namespace std;

TEST(FileNameNode, insensitive_filteredFiles)
{
    // Testing in, out, compute logic when "case sensitive" disabled
    // Note : the tested ext will be on the original path

    // -- Test init --
    bool isEnable = true;
    bool caseSensitive = false;

    StringFile sf1("actual.abc", "original.txt", "final.123");
    StringFile sf2("sf2.abc", "SF2.hpp", "sf2.123");
    StringFile sf3("sf3.abc", "ORIginal.CPP", "sf3.123");
    StringFile sf4("sf4.abc", "sf4.md", "sf4.123");
    StringFile sf5("original.txt", "sf5.txt", "original.txt");

    QList<StringFile> valFiles = {sf1, sf2, sf3, sf4, sf5};
    // NAME MATCH (one of this)
    QList<QString> valName = {"original", "SF4"};
    // =
    QList<StringFile> valOutResMatch = {sf1, sf3, sf4};
    QList<StringFile> valOutResNotMatch = {sf2, sf5};

    // -- Perform test --
    FileNameFilterNode node;

    shared_ptr<GrafileDataStringFile> inFiles
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));

    shared_ptr<GrafileDataText> inName
            = make_shared<GrafileDataText>(
                GrafileDataText(
                    valName
                    ));

    shared_ptr<GrafileDataBoolean> inSensitive
            = make_shared<GrafileDataBoolean>(
                GrafileDataBoolean(
                    caseSensitive
                    ));

    // 0 is for enabled port
    node.setInData(inFiles, 1);
    node.setInData(inName, 2);
    node.setInData(inSensitive, 3);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outDataFiltered = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));
    auto outDataExcluded = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(1));

    EXPECT_EQ(outDataFiltered->getList(), valOutResMatch);
    EXPECT_EQ(outDataExcluded->getList(), valOutResNotMatch);
}

TEST(FileNameNode, sensitive_filteredFiles)
{
    // Testing in, out, compute logic when "case sensitive" enabled
    // Note : the tested ext will be on the original path

    // -- Test init --
    bool isEnable = true;
    bool caseSensitive = true;

    StringFile sf1("actual.abc", "original.txt", "final.123");
    StringFile sf2("sf2.abc", "SF2.hpp", "sf2.123");
    StringFile sf3("sf3.abc", "ORIginal.CPP", "sf3.123");
    StringFile sf4("sf4.abc", "sf4.md", "sf4.123");
    StringFile sf5("original.txt", "sf5.txt", "original.txt");

    QList<StringFile> valFiles = {sf1, sf2, sf3, sf4, sf5};
    // NAME MATCH (one of this)
    QList<QString> valName = {"original", "SF4"};
    // =
    QList<StringFile> valOutResMatch = {sf1};
    QList<StringFile> valOutResNotMatch = {sf2, sf3, sf4, sf5};

    // -- Perform test --
    FileNameFilterNode node;

    shared_ptr<GrafileDataStringFile> inFiles
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));

    shared_ptr<GrafileDataText> inName
            = make_shared<GrafileDataText>(
                GrafileDataText(
                    valName
                    ));

    shared_ptr<GrafileDataBoolean> inSensitive
            = make_shared<GrafileDataBoolean>(
                GrafileDataBoolean(
                    caseSensitive
                    ));

    // 0 is for enabled port
    node.setInData(inFiles, 1);
    node.setInData(inName, 2);
    node.setInData(inSensitive, 3);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outDataFiltered = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));
    auto outDataExcluded = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(1));

    EXPECT_EQ(outDataFiltered->getList(), valOutResMatch);
    EXPECT_EQ(outDataExcluded->getList(), valOutResNotMatch);
}

TEST(FileNameNode, defaultInsensitive_filteredFiles)
{
    // Testing in, out, compute logic when node have is "case sensitive" port not connected
    // If the sensitivity isn't connected, by default it's false

    // Note : the tested ext will be on the original path

    // -- Test init --
    bool isEnable = true;

    StringFile sf1("actual.abc", "original.txt", "final.123");
    StringFile sf2("sf2.abc", "SF2.hpp", "sf2.123");
    StringFile sf3("sf3.abc", "ORIginal.CPP", "sf3.123");
    StringFile sf4("sf4.abc", "sf4.md", "sf4.123");
    StringFile sf5("original.txt", "sf5.txt", "original.txt");

    QList<StringFile> valFiles = {sf1, sf2, sf3, sf4, sf5};
    // NAME MATCH (one of this)
    QList<QString> valName = {"original", "SF4"};
    // =
    QList<StringFile> valOutResMatch = {sf1, sf3, sf4};
    QList<StringFile> valOutResNotMatch = {sf2, sf5};

    // -- Perform test --
    FileNameFilterNode node;

    shared_ptr<GrafileDataStringFile> inFiles
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));

    shared_ptr<GrafileDataText> inName
            = make_shared<GrafileDataText>(
                GrafileDataText(
                    valName
                    ));

    // 0 is for enabled port
    node.setInData(inFiles, 1);
    node.setInData(inName, 2);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outDataFiltered = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));
    auto outDataExcluded = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(1));

    EXPECT_EQ(outDataFiltered->getList(), valOutResMatch);
    EXPECT_EQ(outDataExcluded->getList(), valOutResNotMatch);
}

TEST(FileNameNode, disabled)
{
    // Testing in, out, compute logic when node is disabled
    // If the node is disabled, in files will be the same as out files (for both out ports)

    // Note : the tested ext will be on the original path

    // -- Test init --
    bool isEnable = false;

    StringFile sf1("actual.abc", "original.txt", "final.123");
    StringFile sf2("sf2.abc", "SF2.hpp", "sf2.123");
    StringFile sf3("sf3.abc", "ORIginal.CPP", "sf3.123");
    StringFile sf4("sf4.abc", "sf4.md", "sf4.123");
    StringFile sf5("original.txt", "sf5.txt", "original.txt");

    QList<StringFile> valFiles = {sf1, sf2, sf3, sf4, sf5};
    // NAME MATCH (one of this)
    QList<QString> valName = {"original", "SF4"};
    // = (match disabled)
    QList<StringFile> valOutResMatch = {sf1, sf2, sf3, sf4, sf5};
    QList<StringFile> valOutResNotMatch = {sf1, sf2, sf3, sf4, sf5};

    // -- Perform test --
    FileNameFilterNode node;

    shared_ptr<GrafileDataStringFile> inFiles
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));

    shared_ptr<GrafileDataText> inName
            = make_shared<GrafileDataText>(
                GrafileDataText(
                    valName
                    ));

    // 0 is for enabled port
    node.setInData(inFiles, 1);
    node.setInData(inName, 2);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outDataFiltered = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));
    auto outDataExcluded = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(1));

    EXPECT_EQ(outDataFiltered->getList(), valOutResMatch);
    EXPECT_EQ(outDataExcluded->getList(), valOutResNotMatch);
}
