#include "gtest/gtest.h"
#include "filter/NotBoolFilterNode.h"
#include "data/GrafileDataBoolean.h"

using namespace std;

TEST(NotBoolNode, 1)
{
    // -- Test loop init --
    QList<bool> loopValIn = {false, true};
    // =
    QList<bool> loopValOut = {true, false};

    // -- Perform test --
    NotBoolFilterNode node;

    for (int i = 0; i < loopValIn.size(); i++)
    {
        shared_ptr<GrafileDataBoolean> in
                = make_shared<GrafileDataBoolean>(
                    GrafileDataBoolean(
                        {
                            loopValIn[i]
                        }));

        node.setInData(in, 0);

        node.setEnabled(true);
        node.compute();

        auto outData = std::dynamic_pointer_cast<GrafileDataBoolean>(node.outData(0));

        // We test 1 value for A, 1 for B, the output size must be 1
        EXPECT_EQ(outData->getList().size(), 1);

        bool outRes = outData->getValue();

        EXPECT_EQ(outRes, loopValOut[i]);
    }
}

TEST(NotBoolNode, n)
{
    QList<bool> valInA = {false, false, true, true};
    // =
    QList<bool> valOutRes = {true, true, false, false};

    // -- Perform test --
    NotBoolFilterNode node;

    shared_ptr<GrafileDataBoolean> inA
            = make_shared<GrafileDataBoolean>(
                GrafileDataBoolean(
                    valInA
                    ));

    node.setInData(inA, 0);

    node.setEnabled(true);
    node.compute();

    auto outData = std::dynamic_pointer_cast<GrafileDataBoolean>(node.outData(0));

    EXPECT_EQ(outData->getList().size(), valInA.size());

    EXPECT_EQ(outData->getList(), valOutRes);
}
