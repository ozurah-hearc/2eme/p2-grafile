#include "gtest/gtest.h"
#include "filter/FileExtFilterNode.h"
#include "data/GrafileDataBoolean.h"
#include "data/GrafileDataText.h"
#include "data/GrafileDataStringFile.h"

using namespace std;

TEST(FileExtNode, filteredFiles)
{
    // Testing in, out, compute logic
    // Note : the tested ext will be on the original path

    // -- Test init --
    bool isEnable = true;

    StringFile sf1("actual.abc", "original.txt", "final.123");
    StringFile sf2("sf2.abc", "original.hpp", "sf2.123");
    StringFile sf3("sf3.abc", "original.CPP", "sf3.123");
    StringFile sf4("sf4.abc", "original.md", "sf4.123");

    QList<StringFile> valFiles = {sf1, sf2, sf3, sf4};
    // EXT MATCH (one of this)
    QList<QString> valExt = {"txt", "cpp", "abc"};
    // =
    QList<StringFile> valOutResMatch = {sf1, sf3};
    QList<StringFile> valOutResNotMatch = {sf2, sf4};

    // -- Perform test --
    FileExtFilterNode node;

    shared_ptr<GrafileDataStringFile> inFiles
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));

    shared_ptr<GrafileDataText> inExt
            = make_shared<GrafileDataText>(
                GrafileDataText(
                    valExt
                    ));

    // 0 is for enabled port
    node.setInData(inFiles, 1);
    node.setInData(inExt, 2);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outDataFiltered = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));
    auto outDataExcluded = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(1));

    EXPECT_EQ(outDataFiltered->getList(), valOutResMatch);
    EXPECT_EQ(outDataExcluded->getList(), valOutResNotMatch);
}

TEST(FileExtNode, disabled)
{
    // Testing in, out, compute logic when node is disabled
    // If the node is disabled, in files will be the same as out files (for both out ports)
    // Note : the tested ext will be on the original path

    // -- Test init --
    bool isEnable = false;

    StringFile sf1("actual.abc", "original.txt", "final.123");
    StringFile sf2("sf2.abc", "original.hpp", "sf2.123");
    StringFile sf3("sf3.abc", "original.CPP", "sf3.123");
    StringFile sf4("sf4.abc", "original.md", "sf4.123");

    QList<StringFile> valFiles = {sf1, sf2, sf3, sf4};
    // EXT MATCH
    QList<QString> valExt = {"txt", "cpp", "abc"};
    // =
    QList<StringFile> valOutResMatch = {sf1, sf2, sf3, sf4};
    QList<StringFile> valOutResNotMatch = {sf1, sf2, sf3, sf4};

    // -- Perform test --
    FileExtFilterNode node;

    shared_ptr<GrafileDataStringFile> inFiles
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));

    shared_ptr<GrafileDataText> inExt
            = make_shared<GrafileDataText>(
                GrafileDataText(
                    valExt
                    ));

    // 0 is for enabled port
    node.setInData(inFiles, 1);
    node.setInData(inExt, 2);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outDataFiltered = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));
    auto outDataExcluded = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(1));

    EXPECT_EQ(outDataFiltered->getList(), valOutResMatch);
    EXPECT_EQ(outDataExcluded->getList(), valOutResNotMatch);
}
