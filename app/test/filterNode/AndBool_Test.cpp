#include "gtest/gtest.h"
#include "filter/AndBoolFilterNode.h"
#include "data/GrafileDataBoolean.h"

using namespace std;

TEST(AndBoolNode, 1_1)
{
    // -- Test loop init --
    // The in/out will get only 1 data per loop time
    // This 3 list must have the same length !
    QList<bool> loopValInA = {false, false, true, true};
    // AND
    QList<bool> loopValInB = {false, true, false, true};
    // =
    QList<bool> loopValOutRes = {false, false, false, true};

    // -- Perform test --
    AndBoolFilterNode node;

    for (int i = 0; i < loopValInA.size(); i++)
    {
        shared_ptr<GrafileDataBoolean> inA
                = make_shared<GrafileDataBoolean>(
                    GrafileDataBoolean(
                        {
                            loopValInA[i]
                        }));

        shared_ptr<GrafileDataBoolean> inB
                = make_shared<GrafileDataBoolean>(
                    GrafileDataBoolean(
                        {
                            loopValInB[i],
                        }));

        node.setInData(inA, 0);
        node.setInData(inB, 1);

        node.setEnabled(true);
        node.compute();

        auto outData = std::dynamic_pointer_cast<GrafileDataBoolean>(node.outData(0));

        // We test 1 value for A, 1 for B, the output size must be 1
        EXPECT_EQ(outData->getList().size(), 1);

        bool outRes = outData->getValue();

        EXPECT_EQ(outRes, loopValOutRes[i]);
    }
}

TEST(AndBoolNode, n_1)
{
    // -- Test loop init --
    QList<bool> valInA = {false, false, true, true}; // For each loop, A will get this value
    // AND
    QList<bool> loopValInB = {false, true}; // loop turn 1 B = false; turn 2 = true
    // =
    QList<QList<bool>> loopValOutRes = {{false, false, false, false}, {false, false, true, true}}; // {{turn 1}, {turn B}

    // -- Perform test --
    AndBoolFilterNode node;

    shared_ptr<GrafileDataBoolean> inA
            = make_shared<GrafileDataBoolean>(
                GrafileDataBoolean(
                    valInA
                    ));

    for (int i = 0; i < loopValInB.size(); i++)
    {
        shared_ptr<GrafileDataBoolean> inB
                = make_shared<GrafileDataBoolean>(
                    GrafileDataBoolean(
                        {
                            loopValInB[i]
                        }));

        node.setInData(inA, 0);
        node.setInData(inB, 1);

        node.setEnabled(true);
        node.compute();

        auto outData = std::dynamic_pointer_cast<GrafileDataBoolean>(node.outData(0));

        EXPECT_EQ(outData->getList().size(), valInA.size());

        EXPECT_EQ(outData->getList(), loopValOutRes[i]);
    }
}

TEST(AndBoolNode, n_n)
{
    // -- Test loop init --
    QList<bool> valInA = {false, true, false, true};
    // AND
    QList<bool> valInB = {false, false, true, true};
    // =
    QList<bool> valOutRes = {false, false, false, true};

    // -- Perform test --
    AndBoolFilterNode node;

    shared_ptr<GrafileDataBoolean> inA
            = make_shared<GrafileDataBoolean>(
                GrafileDataBoolean(
                    valInA
                    ));

    shared_ptr<GrafileDataBoolean> inB
            = make_shared<GrafileDataBoolean>(
                GrafileDataBoolean(
                    valInB
                    ));


    node.setInData(inA, 0);
    node.setInData(inB, 1);

    node.setEnabled(true);
    node.compute();

    auto outData = std::dynamic_pointer_cast<GrafileDataBoolean>(node.outData(0));

    EXPECT_EQ(outData->getList(), valOutRes);

}

TEST(AndBoolNode, n_0)
{
    // -- Test loop init --
    QList<bool> valInA = {false, true, false, true};
    // AND
    QList<bool> valInB = {};
    // =
    QList<bool> valOutRes = {false, true, false, true};

    // -- Perform test --
    AndBoolFilterNode node;

    shared_ptr<GrafileDataBoolean> inA
            = make_shared<GrafileDataBoolean>(
                GrafileDataBoolean(
                    valInA
                    ));

    shared_ptr<GrafileDataBoolean> inB
            = make_shared<GrafileDataBoolean>(
                GrafileDataBoolean(
                    valInB
                    ));


    node.setInData(inA, 0);
    node.setInData(inB, 1);

    node.setEnabled(true);
    node.compute();

    auto outData = std::dynamic_pointer_cast<GrafileDataBoolean>(node.outData(0));

    EXPECT_EQ(outData->getList(), valOutRes);
}

TEST(AndBoolNode, 0_n)
{
    // -- Test loop init --
    QList<bool> valInA = {};
    // AND
    QList<bool> valInB = {false, false, true, true};
    // =
    QList<bool> valOutRes = {false, false, true, true};

    // -- Perform test --
    AndBoolFilterNode node;

    shared_ptr<GrafileDataBoolean> inA
            = make_shared<GrafileDataBoolean>(
                GrafileDataBoolean(
                    valInA
                    ));

    shared_ptr<GrafileDataBoolean> inB
            = make_shared<GrafileDataBoolean>(
                GrafileDataBoolean(
                    valInB
                    ));


    node.setInData(inA, 0);
    node.setInData(inB, 1);

    node.setEnabled(true);
    node.compute();

    auto outData = std::dynamic_pointer_cast<GrafileDataBoolean>(node.outData(0));

    EXPECT_EQ(outData->getList(), valOutRes);
}
