#include "gtest/gtest.h"
#include "filter/FileSizeFilterNode.h"
#include "data/GrafileDataComparator.h"
#include "data/GrafileDataDecimal.h"
#include "data/GrafileDataStringFile.h"

#include <QFileInfo>

using namespace std;

// /!\ To perform this test, the "UNIT_TEST_GRAFILE" folder available in the git must be copied to the root of the OS
//    (like C:/UNIT_TEST_GRAFILE on window; /UNIT_TEST_GRAFILE on linux)
// Else the test will failed

TEST(FileSizeNode, enabled)
{
    // Testing in, out, compute logic
    // note: the equality (and "<=", ">=") can't be tested with size (we have the default unit as "MB", to convert it to correct "B" will not be a good idea.

    // -- Test init --
    QString TestFolderLocation = QFileInfo("/UNIT_TEST_GRAFILE/").absoluteFilePath(); // Where the existing files are placed (absolutepath will set the correct root)
    qDebug() << TestFolderLocation;

    bool isEnable = true;

    StringFile sf1(TestFolderLocation+"img40kB.png");
    StringFile sf2(TestFolderLocation+"img160kB.jpg");
    StringFile sf3(TestFolderLocation+"img4MB.bmp");

    QList<StringFile> valFiles = {sf1, sf2, sf3};

    // This list (loopVal) must have the same length !
    QList<CompareOperatorType> loopValOperator = {
        CompareOperatorType::NOT_EQUALS,            // test loop #0
        CompareOperatorType::LOWER_THAN,            // test loop #1
        CompareOperatorType::LOWER_EQUALS_THAN,     // test loop #2
        CompareOperatorType::GREATHER_THAN,         // test loop #3
        CompareOperatorType::GREATHER_EQUALS_THAN,  // test loop #4
    };

    QList<double> loopValSize = {
        8,                  // test loop #0
        3,                  // test loop #1
        0.1,                // test loop #2
        3  ,                // test loop #3
        0.1,                // test loop #4
    }; // the indicated value are in MB

    // =
    QList<QList<StringFile>> loopValOutResMatch = {
        {sf1, sf2, sf3},    // test loop #0
        {sf1, sf2},         // test loop #1
        {sf1},              // test loop #2
        {sf3},              // test loop #3
        {sf2, sf3},         // test loop #4
    };
    QList<QList<StringFile>> loopValOutResNotMatch = {
        {},                 // test loop #0
        {sf3},              // test loop #1
        {sf2, sf3},         // test loop #2
        {sf1, sf2},         // test loop #3
        {sf1},              // test loop #4
    };

    // -- Perform test --
    FileSizeFilterNode node;

    for (int i = 0; i < loopValOperator.size(); i++)
    {
        shared_ptr<GrafileDataStringFile> inFiles
                = make_shared<GrafileDataStringFile>(
                    GrafileDataStringFile(
                        valFiles
                        ));

        shared_ptr<GrafileDataComparator> inOperator
                = make_shared<GrafileDataComparator>(
                    GrafileDataComparator(
                        loopValOperator[i]
                        ));

        shared_ptr<GrafileDataDecimal> inSize
                = make_shared<GrafileDataDecimal>(
                    GrafileDataDecimal(
                        loopValSize[i]
                        ));

        // 0 is for enabled port
        node.setInData(inFiles, 1);
        node.setInData(inOperator, 2);
        node.setInData(inSize, 3);

        // enabled must be placed here, else the setInData will change the value with the "enable in port"
        // (when calling setInData, it update the value according of each ports, not only the setted one)
        node.setEnabled(isEnable);

        node.compute();

        auto outDataFiltered = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));
        auto outDataExcluded = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(1));

        EXPECT_EQ(outDataFiltered->getList(), loopValOutResMatch[i]);
        EXPECT_EQ(outDataExcluded->getList(), loopValOutResNotMatch[i]);
    }
}

TEST(FileSizeNode, disabled)
{
    // Testing in, out, compute logic when node is disabled
    // If the node is disabled, in files will be the same as out files (for both out ports)

    // -- Test init --
    QString TestFolderLocation = QFileInfo("/UNIT_TEST_GRAFILE/").absoluteFilePath(); // Where the existing files are placed (absolutepath will set the correct root)
    qDebug() << TestFolderLocation;

    bool isEnable = false;

    StringFile sf1(TestFolderLocation+"img40kB.png");
    StringFile sf2(TestFolderLocation+"img160kB.jpg");
    StringFile sf3(TestFolderLocation+"img4MB.bmp");

    QList<StringFile> valFiles = {sf1, sf2, sf3};
    CompareOperatorType valOperator = CompareOperatorType::LOWER_THAN;
    double valSize = 3;
    // =
    QList<StringFile> valOutResMatch = { sf1, sf2, sf3 };
    QList<StringFile> valOutResNotMatch = { sf1, sf2, sf3 };

    // -- Perform test --
    FileSizeFilterNode node;

    shared_ptr<GrafileDataStringFile> inFiles
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));

    shared_ptr<GrafileDataComparator> inOperator
            = make_shared<GrafileDataComparator>(
                GrafileDataComparator(
                    valOperator
                    ));

    shared_ptr<GrafileDataDecimal> inSize
            = make_shared<GrafileDataDecimal>(
                GrafileDataDecimal(
                    valSize
                    ));

    // 0 is for enabled port
    node.setInData(inFiles, 1);
    node.setInData(inOperator, 2);
    node.setInData(inSize, 3);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outDataFiltered = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));
    auto outDataExcluded = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(1));

    EXPECT_EQ(outDataFiltered->getList(), valOutResMatch);
    EXPECT_EQ(outDataExcluded->getList(), valOutResNotMatch);
}
