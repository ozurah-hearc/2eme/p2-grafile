#include "gtest/gtest.h"
#include "filter/FilePathFilterNode.h"
#include "data/GrafileDataBoolean.h"
#include "data/GrafileDataText.h"
#include "data/GrafileDataStringFile.h"

using namespace std;

TEST(FilePathNode, filteredFiles)
{
    // Testing in, out, compute logic

    // Note : the tested ext will be on the original path
    // Note2 : When executed the test on the CI (unix), it doesn't handled the \\ for paths, it interprete the path as "/" without the next elements.
    //         ie : "C:\\aFolder" becomes just "/".
    //         So we just changing the \\ to / the \\ tests (putting this base value inside /* */)

    // -- Test init --
    bool isEnable = true;

    // path differtents to check if we use the original path as reference for the filter
    StringFile sf1("C:/aFolder/actual.abc", "C:/aFolder/original.txt", "C:/somePlace/final.123");

    /*StringFile sf2("C:\\aFolder\\sf2.abc", "C:\\aFolder\\original.hpp", "C:\\somePlace\\sf2.123");*/
    StringFile sf2("C:/aFolder/sf2.abc", "C:/aFolder/original.hpp", "C:/somePlace/sf2.123");

    StringFile sf3("C:/aFolder/sf3.abc", "C:/somePlace/original.cpp", "C:/aFolder/sf3.123");

    // some folder variants
    StringFile sf4("/sf4.abc", "/original.md", "/sf4.123"); // will took the root path
    StringFile sf5("C:/AFOldER/sf5.abc", "C:/AFOldER/original.md", "C:/AFOldER/sf5.123"); //will be ok, the check is case insensitive
    StringFile sfFolder1("C:/aFolder/anotherFolder/", "C:/aFolder/", "C:/somePlace/");

    /*StringFile sfFolder2("C:\\aFolder\\anotherFolder/", "C:\\aFolder\\anotherFolder/", "C:\\aFolder\\somePlace/");*/
    StringFile sfFolder2("C:/aFolder/anotherFolder/", "C:/aFolder/anotherFolder/", "C:/aFolder/somePlace/");

    QList<StringFile> valFiles = {sf1, sf2, sf3, sf4, sf5, sfFolder1, sfFolder2};
    // PATH MATCH (one of this)
    QList<QString> valPaths = {"C:/aFolder/", "/"};
    // =
    QList<StringFile> valOutResMatch = {sf1, sf2, sf4, sf5, sfFolder1};
    QList<StringFile> valOutResNotMatch = {sf3, sfFolder2};

    // -- Perform test --
    FilePathFilterNode node;

    shared_ptr<GrafileDataStringFile> inFiles
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));

    shared_ptr<GrafileDataText> inPaths
            = make_shared<GrafileDataText>(
                GrafileDataText(
                    valPaths
                    ));

    // 0 is for enabled port
    node.setInData(inFiles, 1);
    node.setInData(inPaths, 2);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outDataFiltered = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));
    auto outDataExcluded = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(1));

    EXPECT_EQ(outDataFiltered->getList(), valOutResMatch);
    EXPECT_EQ(outDataExcluded->getList(), valOutResNotMatch);
}

TEST(FilePathNode, disabled)
{
    // Testing in, out, compute logic when node is disabled
    // If the node is disabled, in files will be the same as out files (for both out ports)

    // Note : the tested ext will be on the original path

    // -- Test init --
    bool isEnable = false;

    // path differtents to check if we use the original path as reference for the filter
    StringFile sf1("C:/aFolder/actual.abc", "C:/aFolder/original.txt", "C:/somePlace/final.123");
    StringFile sf2("C:\\aFolder\\sf2.abc", "C:\\aFolder\\original.hpp", "C:\\somePlace\\sf2.123");
    StringFile sf3("C:/aFolder/sf3.abc", "C:/somePlace/original.cpp", "C:/aFolder/sf3.123");

    // some folder variants
    StringFile sf4("/sf4.abc", "/original.md", "/sf4.123"); // will took the root path
    StringFile sf5("C:/AFOldER/sf5.abc", "C:/AFOldER/original.md", "C:/AFOldER/sf5.123"); //will be ok, the check is case insensitive
    StringFile sfFolder1("C:/aFolder/anotherFolder/", "C:/aFolder/", "C:/somePlace/");
    StringFile sfFolder2("C:\\aFolder\\anotherFolder/", "C:\\aFolder\\anotherFolder/", "C:\\aFolder\\somePlace/");

    QList<StringFile> valFiles = {sf1, sf2, sf3, sf4, sf5, sfFolder1, sfFolder2};
    // PATH MATCH
    QList<QString> valPaths = {"C:/aFolder/", "/"};
    // =
    QList<StringFile> valOutResMatch = {sf1, sf2, sf3, sf4, sf5, sfFolder1, sfFolder2};
    QList<StringFile> valOutResNotMatch = {sf1, sf2, sf3, sf4, sf5, sfFolder1, sfFolder2};

    // -- Perform test --
    FilePathFilterNode node;

    shared_ptr<GrafileDataStringFile> inFiles
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));

    shared_ptr<GrafileDataText> inPaths
            = make_shared<GrafileDataText>(
                GrafileDataText(
                    valPaths
                    ));

    // 0 is for enabled port
    node.setInData(inFiles, 1);
    node.setInData(inPaths, 2);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outDataFiltered = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));
    auto outDataExcluded = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(1));

    EXPECT_EQ(outDataFiltered->getList(), valOutResMatch);
    EXPECT_EQ(outDataExcluded->getList(), valOutResNotMatch);
}
