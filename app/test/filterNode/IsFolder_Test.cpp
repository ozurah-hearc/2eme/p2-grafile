#include "gtest/gtest.h"
#include "filter/IsFolderFilterNode.h"
#include "data/GrafileDataBoolean.h"
#include "data/GrafileDataStringFile.h"

using namespace std;

TEST(IsFolderNode, enabled)
{
    // Testing in, out, compute logic

    // Note : the tested ext will be on the original path

    // -- Test init --
    bool isEnable = true;

    // path differtents to check if we use the original path as reference for the filter
    StringFile sf1("C:/aFolder/actual.abc", "C:/aFolder/original.txt", "C:/somePlace/final.123");
    StringFile sf2("C:\\aFolder\\sf2.abc", "C:\\aFolder\\original.hpp", "C:\\somePlace\\sf2.123");
    StringFile sf3("C:/aFolder/sf3.abc", "C:/somePlace/original.cpp", "C:/aFolder/sf3.123");

    // some folder variants
    StringFile sf4("/sf4.abc", "/original.md", "/sf4.123"); // will took the root path
    StringFile sf5("C:/AFOldER/sf5.abc", "C:/AFOldER/original.md", "C:/AFOldER/sf5.123"); //will be ok, the check is case insensitive
    StringFile sfFolder1("C:/aFolder/anotherFolder/", "C:/aFolder/", "C:/somePlace/");
    StringFile sfFolder2("C:\\aFolder\\anotherFolder/", "C:\\aFolder\\anotherFolder/", "C:\\aFolder\\somePlace/");

    QList<StringFile> valFiles = {sf1, sf2, sf3, sf4, sf5, sfFolder1, sfFolder2};
    // =
    QList<StringFile> valOutResFolders = {sfFolder1, sfFolder2};
    QList<StringFile> valOutResFiles = {sf1, sf2, sf3, sf4, sf5};

    // -- Perform test --
    IsFolderFilterNode node;

    shared_ptr<GrafileDataStringFile> inFiles
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));


    // 0 is for enabled port
    node.setInData(inFiles, 1);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outDataFiltered = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));
    auto outDataExcluded = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(1));

    EXPECT_EQ(outDataFiltered->getList(), valOutResFolders);
    EXPECT_EQ(outDataExcluded->getList(), valOutResFiles);
}

TEST(IsFolderNode, disabled)
{
    // Testing in, out, compute logic when node is disabled
    // If the node is disabled, in files will be the same as out files (for both out ports)

    // Note : the tested ext will be on the original path

    // -- Test init --
    bool isEnable = false;

    // path differtents to check if we use the original path as reference for the filter
    StringFile sf1("C:/aFolder/actual.abc", "C:/aFolder/original.txt", "C:/somePlace/final.123");
    StringFile sf2("C:\\aFolder\\sf2.abc", "C:\\aFolder\\original.hpp", "C:\\somePlace\\sf2.123");
    StringFile sf3("C:/aFolder/sf3.abc", "C:/somePlace/original.cpp", "C:/aFolder/sf3.123");

    // some folder variants
    StringFile sf4("/sf4.abc", "/original.md", "/sf4.123"); // will took the root path
    StringFile sf5("C:/AFOldER/sf5.abc", "C:/AFOldER/original.md", "C:/AFOldER/sf5.123"); //will be ok, the check is case insensitive
    StringFile sfFolder1("C:/aFolder/anotherFolder/", "C:/aFolder/", "C:/somePlace/");
    StringFile sfFolder2("C:\\aFolder\\anotherFolder/", "C:\\aFolder\\anotherFolder/", "C:\\aFolder\\somePlace/");

    QList<StringFile> valFiles = {sf1, sf2, sf3, sf4, sf5, sfFolder1, sfFolder2};
    // =
    QList<StringFile> valOutResFolders = {sf1, sf2, sf3, sf4, sf5, sfFolder1, sfFolder2};
    QList<StringFile> valOutResFiles = {sf1, sf2, sf3, sf4, sf5, sfFolder1, sfFolder2};

    // -- Perform test --
    IsFolderFilterNode node;

    shared_ptr<GrafileDataStringFile> inFiles
            = make_shared<GrafileDataStringFile>(
                GrafileDataStringFile(
                    valFiles
                    ));


    // 0 is for enabled port
    node.setInData(inFiles, 1);

    // enabled must be placed here, else the setInData will change the value with the "enable in port"
    // (when calling setInData, it update the value according of each ports, not only the setted one)
    node.setEnabled(isEnable);

    node.compute();

    auto outDataFiltered = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(0));
    auto outDataExcluded = std::dynamic_pointer_cast<GrafileDataStringFile>(node.outData(1));

    EXPECT_EQ(outDataFiltered->getList(), valOutResFolders);
    EXPECT_EQ(outDataExcluded->getList(), valOutResFiles);
}
