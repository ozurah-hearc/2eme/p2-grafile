/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/treatment/DeleteTreatmentNode.cpp"
 * @brief       Source file for "app/include/treatment/DeleteTreatmentNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "treatment/DeleteTreatmentNode.h"

DeleteTreatmentNode::DeleteTreatmentNode() : TreatmentNodeModel("Delete")
{
    addInputPort("StringFile", "files");

    addOutputPort("StringFile", "files");
}

void DeleteTreatmentNode::compute()
{
    output.clear();
    output = src; // Copy the source (is the same data, but with the flag modified)

    if (isEnabled())
    {
        qDebug() << "Delete node computed";

        for(int i = 0; i < output.getList().size(); i++)
        {
            StringFile * sf = output.getPtrValueAt(i);

            // A file with a finalPath means it was handled by another node
            if (!sf->getFinalPath().isEmpty())
            {
                // we want to delete the handled file -> finalPath is the source (original path) for the deletion
                sf->setOriginalPath(sf->getFinalPath());
                sf->setCurrentPath(sf->getFinalPath()); // Just to be sure we set them with same path

                // There is no dest for this file
                sf->setFinalPath("");
            }

            // Add the flag "deleted"
            sf->setNeedDelete(true);
            // A deleted file should not be moved, so we remove this flag
            sf->setNeedMove(false);
        }
    }
    else
    {
        qDebug() << "Copy node skiped";
        output = src;
    }

    // Rise event for perform next nodes
    Q_EMIT dataUpdated(0);
}

std::shared_ptr<NodeData> DeleteTreatmentNode::outData(PortIndex outPortIndex)
{
    return std::make_shared<GrafileDataStringFile>(output);
}

void DeleteTreatmentNode::convertInputsToVars()
{
    bool hasError = false;

    // ENABLE
    setEnableInput(0);

    // SRC
    {
        auto node = std::dynamic_pointer_cast<GrafileDataStringFile>(_input[1]);
        if (node)
        {
            src = node->getList();
        }
        else
        {
            src.clear();
            hasError = true;
        }
    }

    changeValidationState(hasError);
}
