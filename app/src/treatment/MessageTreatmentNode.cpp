/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/treatment/MessageTreatmentNode.cpp"
 * @brief       Source file for "app/include/treatment/MessageTreatmentNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "treatment/MessageTreatmentNode.h"

#include <QMessageBox>
#include <QAbstractButton>
#include <QPushButton>

MessageTreatmentNode::MessageTreatmentNode() : TreatmentNodeModel("Message")
{
    addInputPort("StringFile", "For execution order");
    addInputPort("Text", "Message");
    addInputPort("Text", "Text button 1");
    addInputPort("Text", "Text button 2");

    addOutputPort("StringFile", "For execution order");
    addOutputPort("Boolean", "Clicked button 1");
    addOutputPort("Boolean", "Clicked button 2");
}

void MessageTreatmentNode::compute()
{
    onBtn1 = false;
    onBtn2 = false;

    outForConnectionOrder = inForConnectionOrder;

    if (isEnabled())
    {
        qDebug() << "Message node computed";

        QMessageBox msgBox(QMessageBox::Warning,
                           tr("Custom message"),
                           message,
                           QMessageBox::NoButton,
                           qobject_cast<QWidget *> (parent())
                           );

        QAbstractButton* pButton1 = msgBox.addButton(txtBtn1, QMessageBox::YesRole);
        msgBox.addButton(txtBtn2, QMessageBox::NoRole);

        msgBox.exec();

        bool clickedBtn1 = msgBox.clickedButton() == pButton1;

        onBtn1 = clickedBtn1;
        onBtn2 = !clickedBtn1;
    }
    else
    {
        qDebug() << "Message node skiped";
        onBtn1 = true;
        onBtn2 = true;
    }

    // Rise event for perform next nodes
    Q_EMIT dataUpdated(0);
    Q_EMIT dataUpdated(1);
}

std::shared_ptr<NodeData> MessageTreatmentNode::outData(PortIndex outPortIndex)
{
    if (outPortIndex == 0)
    {
        return std::make_shared<GrafileDataStringFile>(outForConnectionOrder);
    }
    if (outPortIndex == 1)
    {
        return std::make_shared<GrafileDataBoolean>(onBtn1);
    }
    else
    {
        return std::make_shared<GrafileDataBoolean>(onBtn2);
    }
}

void MessageTreatmentNode::convertInputsToVars()
{
    bool hasError = false;

    // ENABLE
    setEnableInput(0);

    // FOR CONNECTION ORDER
    {
        auto node = std::dynamic_pointer_cast<GrafileDataStringFile>(_input[1]);
        if (node)
        {
            inForConnectionOrder = node->getList();
        }
        else
        {
            inForConnectionOrder.clear();
            hasError = true;
        }
    }

    // MESSAGE
    {
        auto node = std::dynamic_pointer_cast<GrafileDataText>(_input[2]);
        if (node && node->hasValues())
        {
            message = node->getValue();
        }
        else
        {
            message = DEFAULT_MESSAGE;
        }
    }

    // TEXT BUTTON 1
    {
        auto node = std::dynamic_pointer_cast<GrafileDataText>(_input[3]);
        if (node && node->hasValues())
        {
            txtBtn1 = node->getValue();
        }
        else
        {
            txtBtn1 = DEFAULT_TXT_BTN1;
        }
    }

    // TEXT BUTTON 2
    {
        auto node = std::dynamic_pointer_cast<GrafileDataText>(_input[4]);
        if (node && node->hasValues())
        {
            txtBtn2 = node->getValue();
        }
        else
        {
            txtBtn2 = DEFAULT_TXT_BTN2;
        }
    }

    changeValidationState(hasError);
}
