/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/treatment/MoveTreatmentNode.cpp"
 * @brief       Source file for "app/include/treatment/MoveTreatmentNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "treatment/MoveTreatmentNode.h"
#include "includes_grafileData.h"
#include "temporarywork.h"
#include "data/StringFile.h"
#include <QFileInfo>
#include "Utility.h"

MoveTreatmentNode::MoveTreatmentNode() : TreatmentNodeModel("Move")
{
    addInputPort("StringFile", "files");
    addInputPort("StringFile", "destination");
    addOutputPort("StringFile", "files");
}

void MoveTreatmentNode::compute()
{
    output.clear();

    if (isEnabled())
    {
        qDebug() << "Move node computed";
        foreach(StringFile sf, dest.getList())
        {
            QString destPath = sf.getOriginalPath();
            if (Utility::isDir(destPath))
            {
                GrafileDataStringFile copied = moveLogic(src, destPath).getList();
                output.getList().append(copied.getList());
            }
            else
            {
                qDebug() << "This dest path isn't a folder, move skiped for this : " << destPath;
            }
        }
    }
    else
    {
        qDebug() << "Move node skiped";
        output = src;
    }

    // Rise event for perform next nodes
    Q_EMIT dataUpdated(0);
}

std::shared_ptr<NodeData> MoveTreatmentNode::outData(PortIndex outPortIndex)
{
    return std::make_shared<GrafileDataStringFile>(output);
}

void MoveTreatmentNode::convertInputsToVars()
{
    bool hasError = false;

    // ENABLE
    setEnableInput(0);

    // SRC
    {
        auto node = std::dynamic_pointer_cast<GrafileDataStringFile>(_input[1]);
        if (node)
        {
            src = node->getList();
        }
        else
        {
            src.clear();
            hasError = true;
        }
    }

    // DEST
    {
        auto node = std::dynamic_pointer_cast<GrafileDataStringFile>(_input[2]);
        if (node)
        {
            dest = node->getList();
        }
        else
        {
            dest.clear();
            hasError = true;
        }
    }

    changeValidationState(hasError);
}

GrafileDataStringFile MoveTreatmentNode::moveLogic(GrafileDataStringFile src, QString dest)
{
    GrafileDataStringFile result;

    foreach (StringFile file, src.getList())
    {
        StringFile tempFile(file);

        QString srcFile = file.getCurrentPath();
        QString tempPath = TemporaryWork::getTempFile();

        if (tempPath.isEmpty())
        {
            qDebug() << "ERROR : Can't get temp path - src " << srcFile;

            if (TemporaryWork::displayMessageNoTempFile(file.getOriginalPath(), getParent()))
            {
                result.getList().append(file);
            }
            continue;
        }

        // Move logic (it will copy to temp if not already in the temp folder)
        if (file.getNeedDelete())
        {
            qDebug() << "this file is marked as it will be deleted, skipped : " << srcFile;

            // Just retransmit the the previous one
            result.getList().append(file);
        }
        else if (Utility::isDir(srcFile))
        {
            qDebug() << "For now, you can't move a folder, skipped : " << srcFile;
        }
        else if (TemporaryWork::isFileInTemp(srcFile))
        {
            // File already in the temp folder, we didn't copied a new time, just move his final path
            tempFile.setFinalPath(dest + QFileInfo(file.getOriginalPath()).fileName());
            tempFile.setNeedMove(true);

            result.getList().append(tempFile);
        }
        else if (Utility::copyFile(srcFile, tempPath))
        {
            tempFile.setCurrentPath(tempPath);
            tempFile.setFinalPath(dest + QFileInfo(file.getOriginalPath()).fileName());
            tempFile.setNeedMove(true);

            result.getList().append(tempFile);
        }
        else
        {
            // Can happen if the source doesn't exists
            qDebug() << "ERROR : Not copied - src " << srcFile << " to temp " << tempPath << " // is the source exists ?";
        }
    }

    return result;
}
