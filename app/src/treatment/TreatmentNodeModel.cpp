/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/treatment/TreatmentNodeModel.cpp"
 * @brief       Source file for "app/include/treatment/TreatmentNodeModel.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "treatment/TreatmentNodeModel.h"

#include <nodes/ConnectionStyle>

using namespace QtNodes;

TreatmentNodeModel::TreatmentNodeModel() : TreatmentNodeModel("")
{
}

TreatmentNodeModel::TreatmentNodeModel(QString name, bool canEnable) : GrafileNodeModel(name)
{
    modelValidationState = NodeValidationState::Error;

  if (canEnable)
  {
    addInputPort("Boolean", "Is Enable");
  }

  NodeStyle style(R"(
      {
        "NodeStyle": {
          "NormalBoundaryColor": [255, 255, 255],
          "SelectedBoundaryColor": "#FFC040",
          "GradientColor0": "#806040",
          "GradientColor1": "#503C28",
          "GradientColor2": "#403020",
          "GradientColor3": "#3A2B1D",
          "ShadowColor": [20, 20, 20],
          "FontColor" : "white",
          "FontColorFaded" : "gray",
          "ConnectionPointColor": [169, 169, 169],
          "FilledConnectionPointColor": "cyan",
          "ErrorColor": "red",
          "WarningColor": [128, 128, 0],

          "PenWidth": 1.0,
          "HoveredPenWidth": 1.5,

          "ConnectionPointDiameter": 8.0,

          "Opacity": 0.8
        }
      }
    )");

  setNodeStyle(style);
}

// TODO: implement methods
