/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/treatment/StartNode.cpp"
 * @brief       Source file for "app/include/treatment/StartNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "treatment/StartNode.h"
#include "data/GrafileDataStringFile.h"

StartNode::StartNode() : TreatmentNodeModel("Start", false)
{
    modelValidationState = NodeValidationState::Valid;

    addOutputPort("StringFile", "Source");
}

void StartNode::compute()
{
    qDebug() << "Starting !";
    Q_EMIT dataUpdated(0);
}

void StartNode::setData(QStringList files)
{
    QList<StringFile> sfList;
    for(QString file : files)
    {
        sfList.append(file);
    }
    this->output = GrafileDataStringFile(sfList);
}

std::shared_ptr<NodeData> StartNode::outData(PortIndex outPortIndex)
{
    return std::make_shared<GrafileDataStringFile>(this->output);
}
