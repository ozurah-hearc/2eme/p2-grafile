/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/treatment/EndNode.cpp"
 * @brief       Source file for "app/include/treatment/EndNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "treatment/EndNode.h"
#include "includes_grafileData.h"
#include "temporarywork.h"
#include "Utility.h"

#include <QFileInfo>
#include <QMessageBox>
#include <QAbstractButton>
#include <QPushButton>

EndNode::EndNode() : TreatmentNodeModel("End", false)
{
    addInputPort("StringFile", "apply");
}

void EndNode::compute()
{
    // Close the compute execution for other nodes
    setComputeExec(false);

    // Guard clauses
    //  - No need to compute, src empty
    if (src.getList().size() == 0)
    {
        QMessageBox::information(getParent(), tr("Nothing to apply"),
                                 tr("There is no change to apply"),
                                 QMessageBox::Ok);

        TemporaryWork::cleanTempAppFolder();
        return;
    }

    // - Apply confirmation
    QMessageBox::StandardButton ret = QMessageBox::warning(getParent(), tr("Before apply changement"),
                                   tr("Confirm you want to apply the waiting changes ?\n"
                                      "(If a problem occurs during this process, files can be lost)"),
                                   QMessageBox::Yes | QMessageBox::Cancel);
    if (ret != QMessageBox::Yes)
    {
        QMessageBox::information(getParent(), tr("Canceled"),
                                 tr("You canceld the apply of the modification"),
                                 QMessageBox::Ok);

        TemporaryWork::cleanTempAppFolder();
        return;
    }

    // All guard close passed (they will return; if not passed)

    // End node logic : apply each modification

    bool fullSuccess = true; // Is apply works for all files ?

    auto copiedSrc = src; // We work in a copy because we don't want to modify the source with the pointer used for the logic

    // - Copy and move
    for (int i = 0; i < copiedSrc.getList().size(); i++)
    {
        StringFile* sf = copiedSrc.getPtrValueAt(i);
        fullSuccess &= applyCopyLogic(sf);
    }

    // - Delete
    if (fullSuccess)
    {
        for (int i = 0; i < copiedSrc.getList().size(); i++)
        {
            StringFile* sf = copiedSrc.getPtrValueAt(i);
            fullSuccess &= applyDeleteLogic(sf);
        }
    }

    // After apply logic
    if (fullSuccess)
    {
        TemporaryWork::cleanTempAppFolder();
    }

    // End of the graph
    qDebug() << "Ending !";
}

void EndNode::convertInputsToVars()
{
    bool hasError = false;

    // SRC
    {
        auto node = std::dynamic_pointer_cast<GrafileDataStringFile>(_input[0]);
        if (node)
        {
            src = node->getList();
        }
        else
        {
            hasError = true;
        }
    }

    changeValidationState(hasError);
}

bool EndNode::applyCopyLogic(StringFile *file)
{
    // Guard clause
    if (file->getFinalPath().isEmpty())
    {
        qDebug() << "No Final path (like never path through treatment nodes), skiped : "<< file->getOriginalPath();
        return true;
    }

    bool success = true;
    bool overwrite = true;
    QString applyErrorMessage = "";

    // File exists
    if (QFileInfo::exists(file->getFinalPath()))
    {
        overwrite = actionIfFileAlreadyExists(*file);
        if (!overwrite)
        {
            file->setNeedMove(false); // We cancel the move intension (for the deletionLogic)
        }
        else
        {
            // Remove existing destination (for overwrite)
            success = Utility::deleteFile(file->getFinalPath());
        }
    }

    // Dest Folder creation
    if (overwrite && success)
    {
        success = Utility::createDir(QFileInfo(file->getFinalPath()).absolutePath());
    }

    // Copy
    if (overwrite && success && !applyCopy(*file))
    {
        success = false;
        applyErrorMessage = "when copy";
    }

    // Move
    if (overwrite && success && !applyMove(*file))
    {
        success = false;
        applyErrorMessage = "when move";
    }

    // Finalize
    if (!success)
    {
        displayErrorMessageApplyLogic(*file, applyErrorMessage);
    }

    return success;
}

bool EndNode::applyDeleteLogic(StringFile *file)
{
    bool success = applyDelete(*file);

    if (!success)
    {
        QString applyErrorMessage = "when delete";
        displayErrorMessageApplyLogic(*file, applyErrorMessage);
    }

    return success;
}

bool EndNode::applyCopy(StringFile file)
{
    bool success = true;

    if (!file.getNeedDelete() && !file.getNeedMove())
    {
        success = Utility::copyFile(file.getCurrentPath(), file.getFinalPath());
    }

    return success;
}

bool EndNode::applyMove(StringFile file)
{
    bool success = true;

    if (!file.getNeedDelete() && file.getNeedMove())
    {
        success = Utility::copyFile(file.getCurrentPath(), file.getFinalPath());
    }

    return success;
}

bool EndNode::applyDelete(StringFile file)
{
    bool success = true;

    if (file.getNeedMove() || file.getNeedDelete())
    {
        if (Utility::isDir(file.getOriginalPath()))
        {
            QMessageBox::information(getParent(), tr("skipped"),
                                     tr("Actually, the deletion of folders didn't work.\n"
                                        "Skipped : ") + file.getOriginalPath(),
                                     QMessageBox::Ok);
        }
        else
        {
            success = Utility::deleteFile(file.getOriginalPath());
        }
    }

    return success;
}

void EndNode::displayErrorMessageApplyLogic(StringFile file, QString when)
{
    QString msgDest = "";
    if (!file.getFinalPath().isEmpty())
    {
        msgDest = "Destination was : " + file.getFinalPath() + "\n";
    }

    QString msg = "An error occured " + when + ".\n"
                  + msgDest +
                  "\nMake sure this file is still there (origin) : " + file.getOriginalPath() + ".\n"
                  "If not, copy and rename this one (it's a copy of your file) : " + file.getCurrentPath() + ".\n"
                  "\nBe aware, if you rexecute the graph, this copy could be deleted !";


    qInfo() << "/!\\ [IMPORTANT MESSAGE] /!\\";
    qInfo().noquote() << tr(qPrintable(msg));
    qInfo() << "[END OF THE MESSAGE]";

    QMessageBox::critical(getParent(), tr("error"),
                             tr(qPrintable(msg)),
                             QMessageBox::Ok);
}

bool EndNode::actionIfFileAlreadyExists(StringFile file)
{
    QMessageBox msgBox(QMessageBox::Warning,
                       tr("File already exists"),
                       "The file already exists in the dest. path.\n"
                       "Source : " + file.getOriginalPath() + ".\n"
                       "Destination : " + file.getFinalPath() + ".",
                       QMessageBox::NoButton,
                       getParent()
                       );

    QAbstractButton* pButtonOverwitte = msgBox.addButton(tr("Overwrite it"), QMessageBox::YesRole);
    msgBox.addButton(tr("Skip"), QMessageBox::NoRole);

    msgBox.exec();

    return msgBox.clickedButton() == pButtonOverwitte;
}
