/**
 * @attention   /!\ Unimplemented node /!\
 * 
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/treatment/CompressionTreatmentNode.cpp"
 * @brief       Source file for "app/include/treatment/CompressionTreatmentNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "treatment/CompressionTreatmentNode.h"

CompressionTreatmentNode::CompressionTreatmentNode() : TreatmentNodeModel("Compression")
{
}

// TODO: implement methods
