/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/treatment/CopyTreatmentNode.cpp"
 * @brief       Source file for "app/include/treatment/CopyTreatmentNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "treatment/CopyTreatmentNode.h"
#include "includes_grafileData.h"
#include "temporarywork.h"
#include "data/StringFile.h"
#include <QFileInfo>
#include "Utility.h"

CopyTreatmentNode::CopyTreatmentNode() : TreatmentNodeModel("Copy")
{
    addInputPort("StringFile", "files");
    addInputPort("StringFile", "destination");
    addOutputPort("StringFile", "files");
}

void CopyTreatmentNode::compute()
{
    output.clear();
    QVector<StringFile>& vecOutput = output.getList();

    if (isEnabled())
    {
        qDebug() << "Copy node computed";
        foreach(StringFile sf, dest.getList())
        {
            QString destPath = sf.getOriginalPath();
            if (Utility::isDir(destPath))
            {
                GrafileDataStringFile copied = copyLogic(src, destPath).getList();
                vecOutput.append(copied.getList());
            }
            else
            {
                qDebug() << "This dest path isn't a folder, copy skiped for this : " << destPath;
            }
        }
    }
    else
    {
        qDebug() << "Copy node skiped";
        output = src;
    }

    // Rise event for perform next nodes
    Q_EMIT dataUpdated(0);
}

std::shared_ptr<NodeData> CopyTreatmentNode::outData(PortIndex outPortIndex)
{
    return std::make_shared<GrafileDataStringFile>(output);
}

void CopyTreatmentNode::convertInputsToVars()
{
    bool hasError = false;

    // ENABLE
    setEnableInput(0);

    // SRC
    {
        auto node = std::dynamic_pointer_cast<GrafileDataStringFile>(_input[1]);
        if (node)
        {
            src = node->getList();
        }
        else
        {
            src.clear();
            hasError = true;
        }
    }

    // DEST
    {
        auto node = std::dynamic_pointer_cast<GrafileDataStringFile>(_input[2]);
        if (node)
        {
            dest = node->getList();
        }
        else
        {
            dest.clear();
            hasError = true;
        }
    }

    changeValidationState(hasError);
}

GrafileDataStringFile CopyTreatmentNode::copyLogic(GrafileDataStringFile src, QString dest)
{
    GrafileDataStringFile result;

    foreach (StringFile file, src.getList())
    {

        StringFile tempFile(file);
        
        QString srcFile = file.getCurrentPath();
        QString tempPath = TemporaryWork::getTempFile();

        if (tempPath.isEmpty())
        {
            qDebug() << "ERROR : Can't get temp path - src " << srcFile;

            if (TemporaryWork::displayMessageNoTempFile(file.getOriginalPath(), getParent()))
            {
                result.getList().append(file);
            }
            continue;
        }

        // Copy logic (it will copy to temp)
        if (file.getNeedDelete())
        {
            qDebug() << "this file is marked as it will be deleted, skipped : " << srcFile;

            // Just retransmit the the previous one
            result.getList().append(file);
        }
        else if (Utility::isDir(srcFile))
        {
            qDebug() << "For now, you can't copy a folder, skipped : " << srcFile;
        }
        else if (Utility::copyFile(srcFile, tempPath))
        {
            if (file.getNeedMove())
            {
                // Keep the previous one, and also the "copied"
                result.getList().append(file);
            }

            tempFile.setCurrentPath(tempPath);
            tempFile.setFinalPath(dest + QFileInfo(file.getOriginalPath()).fileName());

            // we did a copy, so we reset the flags
            tempFile.setNeedMove(false);
            tempFile.setNeedDelete(false);

            result.getList().append(tempFile);
        }
        else
        {
            // Can happen if the source doesn't exists
            qDebug() << "ERROR : Not copied - src " << srcFile << " to temp " << tempPath << " // is the source exists ?";
        }
    }

    return result;
}
