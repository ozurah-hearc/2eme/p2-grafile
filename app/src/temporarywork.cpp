/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/temporarywork.cpp"
 * @brief       Source file for "app/include/temporarywork.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "temporarywork.h"
#include <QFile>
#include <QDir>
#include <iostream>
#include <QTemporaryFile>

#include <QMessageBox>
#include <QAbstractButton>
#include <QPushButton>

#include "Utility.h"

const QString TemporaryWork::FOLDER_APP_NAME = "Grafile";
const QString TemporaryWork::TEMP_FILENAME_TEMPLATE = "XXXXXX";
const QString TemporaryWork::TEMP_FOLDER = QDir::tempPath() + "/"+FOLDER_APP_NAME+"/";

QString TemporaryWork::getTempFile(){
    createTempAppFolder();

    QString tempFile;
    QTemporaryFile temp; // https://cpp.hotexamples.com/fr/examples/-/QTemporaryFile/-/cpp-qtemporaryfile-class-examples.html
    temp.setFileTemplate(TEMP_FOLDER + TEMP_FILENAME_TEMPLATE);

    if (temp.open()) {
        tempFile = temp.fileName();

        temp.close(); // We got the temporary file name, we can close it to recreate with the correct content
    }

    return tempFile;
}

void TemporaryWork::cleanTempAppFolder(){
    QDir dir(TEMP_FOLDER);
    dir.removeRecursively();
}

void TemporaryWork::createTempAppFolder(){
    Utility::createDir(TEMP_FOLDER);
}

bool TemporaryWork::isFileInTemp(QString file)
{
    // Without the absolute path on the TEMP_FOLDER, they didn't match because we add a / to indicate that is a folder
    return QDir::match(QFileInfo(TEMP_FOLDER).absolutePath(), QFileInfo(file).absolutePath());
}

bool TemporaryWork::displayMessageNoTempFile(QString file, QWidget* parent)
{
    QMessageBox msgBox(QMessageBox::Warning,
                       QObject::tr("Can't copy in temp folder"),
                       "Error while copy file in temporary folder\n"
                       "Source : " + file + ".\n"
                       "Keep this unmodified file for next nodes ?",
                       QMessageBox::NoButton,
                       parent
                       );

    QAbstractButton* pButtonOverwitte = msgBox.addButton(QObject::tr("Keep"), QMessageBox::YesRole);
    msgBox.addButton(QObject::tr("Skip"), QMessageBox::NoRole);

    msgBox.exec();

    return msgBox.clickedButton() == pButtonOverwitte;
}

