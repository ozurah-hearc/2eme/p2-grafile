/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/ui/OutputColumnWidget.cpp"
 * @brief       Source file for "app/include/ui/OutputColumnWidget.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "ui/OutputColumnWidget.h"

#include <QTreeView>
#include <QPushButton>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QMessageBox>

OutputColumnWidget::OutputColumnWidget(QWidget *parent)
    : QWidget{parent}
{
    this->fileSystemModel = new QFileSystemModel(this);
    this->fileSystemModel->setReadOnly(true);

    QFileDialog* selectOutputDialog = new QFileDialog(this, "Select output folder");
    selectOutputDialog->setFileMode(QFileDialog::Directory);
    selectOutputDialog->setViewMode(QFileDialog::Detail);

    this->outputTree = new QTreeView(this);
    this->outputTree->setModel(this->fileSystemModel);
    for(int i = 1; i < this->fileSystemModel->columnCount(); i++){
        this->outputTree->hideColumn(i);
    }

    QPushButton* selectOutputButton = new QPushButton("Select output", this);


    QVBoxLayout* outputColumnLayout = new QVBoxLayout(this);
    outputColumnLayout->addWidget(outputTree);
    outputColumnLayout->addWidget(selectOutputButton);
    outputColumnLayout->addWidget(executeButton);

    connect(selectOutputButton, &QPushButton::clicked, selectOutputDialog, &QDialog::open);
    connect(selectOutputDialog, &QFileDialog::fileSelected, this, &OutputColumnWidget::selectedFolder);
}

void OutputColumnWidget::selectedFolder(QString folder)
{
    // empty check
    if(!QDir(folder).isEmpty())
    {
        QMessageBox::warning(this, "Selected folder is not empty", "Please select an empty folder to output.");
        return;
    }

    this->outputTree->setRootIndex(this->fileSystemModel->setRootPath(QDir::cleanPath(folder)));
    emit this->folderChanged(folder);
}

void OutputColumnWidget::readyState(bool enabled)
{
    this->executeButton->setEnabled(enabled);
}

