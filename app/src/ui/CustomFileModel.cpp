/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/ui/CustomFileModel.cpp"
 * @brief       Source file for "app/include/ui/CustomFileModel.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "ui/CustomFileModel.h"

#include <QApplication>
#include <QStyle>
#include <QDir>
#include <QStack>
#include <QtConcurrent>
#include <QtConcurrentRun>
#include <QRunnable>
#include <QFutureWatcher>
#include <QStatusBar>

CustomFileModel::CustomFileModel(QObject *parent)
    : QStandardItemModel{parent}
{
    this->populateFiles = new QList<QFileInfo>();
    this->rootItem = this->invisibleRootItem();
    this->dirIcon = QApplication::style()->standardIcon(QStyle::SP_DirIcon);
    this->fileIcon = QApplication::style()->standardIcon(QStyle::SP_FileIcon);
}

QFutureWatcher<void>* CustomFileModel::addDirectory(QString path)
{
    if(path.isEmpty())
    {
        return nullptr;
    }

    CustomFileEntry root;
    root.parent = this->rootItem;
    root.fileInfo = QFileInfo(path);

    QFutureWatcher<void>* watcher = new QFutureWatcher<void>(this->parent());
    QFuture<void> future = QtConcurrent::run(&CustomFileModel::populateDirectory, this, root);
    watcher->setFuture(future);
    return watcher;
}

void CustomFileModel::populateDirectory(QPromise<void> &promise, CustomFileEntry root)
{
    promise.setProgressRange(0, 1);

    QStack<CustomFileEntry> itemStack;

    CustomFileEntry rootItem;
    rootItem.fileInfo = root.fileInfo;
    rootItem.parent = nullptr;
    itemStack.push(rootItem);

    while(!itemStack.empty()) {
        promise.setProgressRange(0, itemStack.count());
        CustomFileEntry current = itemStack.pop();
        promise.setProgressValue(itemStack.count());
        if(current.fileInfo.isDir()) {
            // if it's a directory, push children on stack and add it to it's parent
            QStandardItem* newItem = new QStandardItem(dirIcon, current.fileInfo.fileName());
            newItem->setAccessibleDescription(current.fileInfo.filePath());
            newItem->setEditable(false);
            QFileInfoList childrenFileInfo = QDir(current.fileInfo.filePath()).entryInfoList(
                QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot,
                QDir::Name | QDir::Reversed | QDir::DirsLast
            );
            QFileInfo currentChild;
            foreach (currentChild, childrenFileInfo) {
                CustomFileEntry item;
                item.fileInfo = currentChild;
                item.parent = newItem;
                itemStack.push(item);
            }
            (current.parent == nullptr ? root.parent : current.parent)->appendRow(newItem);
        } else {
            // if it's a file, simply add it
            populateFiles->append(current.fileInfo);
            QStandardItem* newItem = new QStandardItem(fileIcon, current.fileInfo.fileName());
            newItem->setAccessibleDescription(current.fileInfo.filePath());
            newItem->setEditable(false);
            current.parent->appendRow(newItem);
        }
    }
}

QList<QFileInfo>* CustomFileModel::getPopulateFiles()
{
    return populateFiles;
}
