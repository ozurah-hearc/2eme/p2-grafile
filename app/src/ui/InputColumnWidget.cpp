/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/ui/InputColumnWidget.cpp"
 * @brief       Source file for "app/include/ui/InputColumnWidget.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "ui/InputColumnWidget.h"

#include <QTreeView>
#include <QPushButton>
#include <QVBoxLayout>
#include <QFileDialog>
#include <QHeaderView>
#include <QFutureWatcher>

InputColumnWidget::InputColumnWidget(QWidget* parent)
    : QWidget{parent}
{
    this->setupUI();
}

void InputColumnWidget::setupUI()
{
    this->fileSystemModel = new CustomFileModel(this);

    this->selectInputDialog = new QFileDialog(this, "Add directory to input");
    selectInputDialog->setFileMode(QFileDialog::Directory);
    selectInputDialog->setViewMode(QFileDialog::Detail);

    this->inputTree = new QTreeView(this);
    this->inputTree->setModel(this->fileSystemModel);
    this->inputTree->header()->hide();
    for (int i = 1; i < this->fileSystemModel->columnCount(); i++)
    {
        this->inputTree->hideColumn(i);
    }

    QPushButton* selectInputButton = new QPushButton("Add directory", this);

    QVBoxLayout *inputColumnLayout = new QVBoxLayout(this);
    inputColumnLayout->addWidget(inputTree);
    inputColumnLayout->addWidget(selectInputButton);

    connect(selectInputButton, &QPushButton::clicked, selectInputDialog, &QDialog::open);
    connect(selectInputDialog, &QFileDialog::fileSelected, this, &InputColumnWidget::selectedDirectory);

    emit dataLoaded(false);
}

void InputColumnWidget::updateReadyness()
{
    if(this->listDirectoriesWatcher == nullptr) {
        return;
    }

    bool ready = this->listDirectoriesWatcher->isFinished();

    emit dataLoaded(ready);
}


void InputColumnWidget::selectedDirectory(QString directory)
{
    emit dataLoaded(false);
    this->listDirectoriesWatcher = this->fileSystemModel->addDirectory(QDir::cleanPath(directory));
    connect(this->listDirectoriesWatcher, &QFutureWatcherBase::finished, this, &InputColumnWidget::updateReadyness);

    emit this->directoryChanged(directory);
    connect(selectInputDialog, &QFileDialog::fileSelected, this, &InputColumnWidget::selectedFolder);
}

QList<QFileInfo> *InputColumnWidget::getOpenedFiles()
{
    return this->fileSystemModel->getPopulateFiles();
}
