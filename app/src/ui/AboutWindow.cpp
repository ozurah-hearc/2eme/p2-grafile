#include "ui/AboutWindow.h"

#include <QLabel>
#include <QPixmap>
#include <QVBoxLayout>
#include <QIcon>

AboutWindow::AboutWindow(QWidget *parent, Qt::WindowType type)
    : QWidget{parent, type}
{
    this->setupUI();
}

void AboutWindow::setupUI()
{
    this->setFixedSize(400, 400);
    this->setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    this->setWindowTitle("Grafile - About");
    this->setWindowIcon(QIcon(":/img/resources/images/logo.png"));

    QVBoxLayout* layout = new QVBoxLayout();

    // image
    QLabel* logo = new QLabel(this);
    QPixmap pixmap(":/img/resources/images/he-arc.png");
    pixmap = pixmap.scaledToWidth(340, Qt::SmoothTransformation);
    logo->setPixmap(pixmap);
    logo->setAlignment(Qt::AlignCenter);

    // Name
    QLabel* nameLabel = new QLabel(this);
    nameLabel->setText("Grafile v1.0.0");
    nameLabel->setAlignment(Qt::AlignCenter);
    QFont nameFont = nameLabel->font();
    nameFont.setPointSize(32);
    nameLabel->setFont(nameFont);

    // Authors
    QLabel* authorsLabel = new QLabel(this);
    authorsLabel->setText("Developed in 2022 by Allemann Jonas, Chappuis Sébastien and Stouder Xavier.");
    authorsLabel->setWordWrap(true);
    authorsLabel->setAlignment(Qt::AlignCenter);
    QFont authorsFont = authorsLabel->font();
    authorsFont.setPointSize(14);
    authorsLabel->setFont(authorsFont);

    // Credits
    QLabel* creditsLabel = new QLabel(this);
    creditsLabel->setOpenExternalLinks(true);
    creditsLabel->setText("Thanks to <a href=\"https://github.com/Daguerreo\">Daguerreo</a> for the library <a href=\"https://github.com/Daguerreo/NodeEditor\">NodeEditor</a> forked on <a href=\"https://github.com/paceholder/nodeeditor\">paceholder's lib</a> under <a href=\"https://choosealicense.com/licenses/bsd-3-clause/\">BSD 3-Clause revised licence</a>.");
    creditsLabel->setWordWrap(true);
    creditsLabel->setAlignment(Qt::AlignCenter);
    creditsLabel->setTextFormat(Qt::RichText);
    creditsLabel->setTextInteractionFlags(Qt::TextBrowserInteraction);
    QFont creditsFont = creditsLabel->font();
    creditsFont.setPointSize(14);
    creditsLabel->setFont(creditsFont);


    layout->addWidget(logo);
    layout->addWidget(nameLabel);
    layout->addWidget(authorsLabel);
    layout->addWidget(creditsLabel);

    this->setLayout(layout);
}
