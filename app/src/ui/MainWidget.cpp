/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/ui/MainWidget.cpp"
 * @brief       Source file for "app/include/ui/MainWidget.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "ui/MainWidget.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTreeView>
#include <QSizePolicy>
#include <QPushButton>
#include <QFileInfo>

#include "ui/GrafileWidget.h"
#include "ui/InputColumnWidget.h"
#include "ui/OutputColumnWidget.h"

MainWidget::MainWidget(MainWindow *parent)
    : QWidget{parent}
{
    isInputColumnWidgetDataLoaded = false;
    this->setupUI();
}

void MainWidget::setupUI()
{
    // Input column
    this->inputColumnWidget = new InputColumnWidget(this);

    this->executeButton = new QPushButton("Execute", this);
    this->cancelExecuteButton = new QPushButton("Cancel execution", this);

    QVBoxLayout* leftColumnLayout = new QVBoxLayout();
    leftColumnLayout->addWidget(this->inputColumnWidget);
    leftColumnLayout->addWidget(this->executeButton);
    leftColumnLayout->addWidget(this->cancelExecuteButton);

    // Global layout
    this->_grafileWidget = new GrafileWidget(this);

    connect(this->executeButton, &QPushButton::clicked, this, &MainWidget::executeGraph);
    connect(this->cancelExecuteButton, &QPushButton::clicked, this, &MainWidget::cancelGraph);
    connect(this->inputColumnWidget, &InputColumnWidget::dataLoaded, this, &MainWidget::onInputColumnWidgetDataLoaded);
    connect(this->grafileWidget(), &GrafileWidget::eventEnabledComputeChange, this, &MainWidget::onGraphExecutionChanged);

    QHBoxLayout* globalLayout = new QHBoxLayout(this);
    globalLayout->addLayout(leftColumnLayout, 1);
    globalLayout->addWidget(this->_grafileWidget, 4);

    this->setLayout(globalLayout);

    toggleExecutionButton();
}

GrafileWidget* MainWidget::grafileWidget()
{
    return this->_grafileWidget;
}

void MainWidget::executeGraph()
{
    QList<QFileInfo> *files = this->inputColumnWidget->getOpenedFiles();

    QStringList graphEntryData;

    for(QFileInfo file : *files)
    {
        graphEntryData.append(file.filePath());
    }

    this->grafileWidget()->executeGraph(graphEntryData);

    // Toggle button by the event onGraphExecutionChanged
}

void MainWidget::cancelGraph()
{
    this->grafileWidget()->cancelExecuteGraph();

    // Toggle button by the event onGraphExecutionChanged
}

void MainWidget::toggleExecutionButton()
{
    // Which button is visible (start or cancel)
    bool isStarted = this->_grafileWidget->isGraphStarted();

    this->executeButton->setVisible(!isStarted);
    this->cancelExecuteButton->setVisible(isStarted);

    // Is graph executable ?
    bool executable = this->isInputColumnWidgetDataLoaded && !isStarted;

    this->executeButton->setEnabled(executable);
}

void MainWidget::onInputColumnWidgetDataLoaded(bool state)
{
    this->isInputColumnWidgetDataLoaded = state;
    toggleExecutionButton();
}

void MainWidget::onGraphExecutionChanged(bool state)
{
    this->grafileWidget()->setEnabled(!state);
    toggleExecutionButton();
}
