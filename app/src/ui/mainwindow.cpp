/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/ui/mainwindow.cpp"
 * @brief       Source file for "app/include/ui/mainwindow.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include <QWidget>
#include <QMenuBar>
#include "ui/MainWindow.h"
#include "ui/MainWidget.h"
#include "ui/AboutWindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow{parent}
{
    this->setupUI();
    this->setupMenubar();
}

void MainWindow::setupUI()
{
    MainWidget* mainWidget = new MainWidget(this);
    this->setCentralWidget(mainWidget);
    this->resize(1200, 600);
    this->setWindowIcon(QIcon(":/img/resources/images/logo.png"));

    this->aboutWindow = new AboutWindow(this);
}

void MainWindow::setupMenubar()
{
    this->saveGraphAction = new QAction("Save", this);
    this->loadGraphAction = new QAction("Load", this);
    this->aboutAction = new QAction("About", this);

    this->editMenu = this->menuBar()->addMenu("Edit");
    this->editMenu->addAction(this->saveGraphAction);
    this->editMenu->addAction(this->loadGraphAction);

    this->helpMenu = this->menuBar()->addMenu("Help");
    this->helpMenu->addAction(this->aboutAction);

    GrafileWidget* grafileWidget = qobject_cast<MainWidget*>(this->centralWidget())->grafileWidget();

    connect(this->saveGraphAction, &QAction::triggered, grafileWidget, &GrafileWidget::saveGraph);
    connect(this->loadGraphAction, &QAction::triggered, grafileWidget, &GrafileWidget::loadGraph);
    connect(this->aboutAction, &QAction::triggered, this, &MainWindow::showAbout);
}

void MainWindow::showAbout()
{
    this->aboutWindow->show();
}
