/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 *
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/ui/GrafileWidget.cpp"
 * @brief       Source file for "app/include/ui/GrafileWidget.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include <stdexcept>

#include "ui/GrafileWidget.h"

#include <nodes/FlowScene>
#include <nodes/FlowView>
#include <nodes/ConnectionStyle>
#include <nodes/StyleCollection>
#include <nodes/TypeConverter>
#include <QGraphicsView>
#include <QGridLayout>
#include <QMessageBox>

#include <QDebug>

#include "includes_grafileNodes.h"
#include "includes_grafileData.h"
#include "includes_grafileConverters.h"

#include "nodes/Node"

using namespace QtNodes;

GrafileWidget::GrafileWidget(QWidget *parent)
    : QWidget(parent)
{
    this->start = nullptr;

    scene = new FlowScene(registerDataModels(), this);
    FlowView *view = new FlowView(scene);
    QGridLayout *layout = new QGridLayout(this);

    ConnectionStyle style(R"(
      {
        "ConnectionStyle": {
          "__ConstructionColor": "red",
          "__NormalColor": "blue",
          "__SelectedColor": "green",
          "__SelectedHaloColor": "orange",
          "__HoveredColor": "purple",

          "__LineWidth": 8.0,
          "__ConstructionLineWidth": 4.0,
          "__PointDiameter": 20.0,

          "UseDataDefinedColors": true
        }
      }
    )");

    style.associateTypeIdToColor("boolean", QColor(0, 96, 224));
    style.associateTypeIdToColor("integer", QColor(128, 224, 0));
    style.associateTypeIdToColor("decimal", QColor(0, 224, 128));
    style.associateTypeIdToColor("text", QColor(255, 96, 0));
    style.associateTypeIdToColor("stringfile", QColor(255, 192, 0));
    style.associateTypeIdToColor("datetime", QColor(128, 0, 255));
    style.associateTypeIdToColor("comparator", QColor(255, 128, 192));

    StyleCollection::setConnectionStyle(style);

    layout->addWidget(view);
    this->setLayout(layout);
}

std::shared_ptr<DataModelRegistry> GrafileWidget::registerDataModels()
{
    auto ret = std::make_shared<DataModelRegistry>();

    ret->registerModel<TestInNode>("Debug");

    ret->registerModel<StartNode>("Unique");
    ret->registerModel<EndNode>("Unique");

    //ret->registerModel<FileContainsFilterNode>("Filter");
    ret->registerModel<FileExtFilterNode>("Filter");
    ret->registerModel<FileNameFilterNode>("Filter");
    ret->registerModel<FilePathFilterNode>("Filter");
    ret->registerModel<FileSizeFilterNode>("Filter");
    ret->registerModel<IsFolderFilterNode>("Filter");
    ret->registerModel<AndBoolFilterNode>("Filter");
    ret->registerModel<NotBoolFilterNode>("Filter");
    ret->registerModel<OrBoolFilterNode>("Filter");

    //ret->registerModel<ApplyTreatmentNode>("Treatment");
    //ret->registerModel<CompressionTreatmentNode>("Treatment");
    ret->registerModel<CopyTreatmentNode>("Treatment");
    //ret->registerModel<DateTimeAnalysisTreatmentNode>("Treatment");
    ret->registerModel<DeleteTreatmentNode>("Treatment");
    ret->registerModel<MessageTreatmentNode>("Treatment");
    ret->registerModel<MoveTreatmentNode>("Treatment");
    //ret->registerModel<NameAnalysisTreatmentNode>("Treatment");
    //ret->registerModel<PreviewTreatmentNode>("Treatment");

    ret->registerModel<BooleanSourceNode>("Input");
    ret->registerModel<CompareOperationSourceNode>("Input");
    ret->registerModel<DateTimeSourceNode>("Input");
    ret->registerModel<DecimalSourceNode>("Input");
    ret->registerModel<IntegerSourceNode>("Input");
    ret->registerModel<StringFileSourceNode>("Input");
    ret->registerModel<TextSourceNode>("Input");

    ret->registerTypeConverter(std::make_pair(GrafileDataDecimal(0.0).type(),
                                              GrafileDataInteger(0).type()),
                               TypeConverter{DecimalToIntegerConverter()});

    ret->registerTypeConverter(std::make_pair(GrafileDataInteger(0.0).type(),
                                              GrafileDataDecimal(0).type()),
                               TypeConverter{IntegerToDecimalConverter()});

    return ret;
}

bool GrafileWidget::isGraphStarted()
{
    bool started = false;
    if (start != nullptr)
    {
        started = start->isComputeExec();
    }

    return started;
}

void GrafileWidget::executeGraph(QStringList sourceFiles)
{
    // Validating the graphe
    bool validation = verifyGraphValidity();
    if (!validation)
    {
        qDebug() << "validation failed";
        QMessageBox::information(this, tr("Validation failed"),
                                 tr("The graph can't be started.\n"
                                "Possible cause :\n"
                                " - No or more than 1 start node & end nodes\n"
                                " - A node have an error"),
                                 QMessageBox::Ok);

        return; // TODO : an custom exception or bool return would be a better idea ?
    }

    // Disconnect previous events
    QObject::disconnect(connectionEventStart);
    QObject::disconnect(connectionEventEnd);

    // Search the start node
    start = nullptr;
    end = nullptr;

    auto nodes = scene->allNodes();
    foreach (auto node, nodes)
    {
        auto datamodel = node->nodeDataModel();

        if (start == nullptr)
        {
            if ((start = qobject_cast<StartNode *>(datamodel)) != nullptr)
            {
                connectionEventStart = connect(start, &GrafileNodeModel::eventEnabledComputeChange, this, &GrafileWidget::eventEnabledComputeChange);
            }
        }

        if (end == nullptr)
        {
            if ((end = qobject_cast<EndNode *>(datamodel)) != nullptr)
            {
                connectionEventEnd = connect(end, &GrafileNodeModel::eventEnabledComputeChange, this, &GrafileWidget::eventEnabledComputeChange);
            }
        }

        if (start != nullptr && end != nullptr)
        {
            break;
        }
    }

    if (start == nullptr || end == nullptr)
    {
        // Should never appear, because we verify the validity before
        //throw std::runtime_error("Even the validity, there is no start (or end) node detected, please contact the developper about it, and how you did it");

        QMessageBox::critical(this, tr("Validation failed"),
                                 tr("Even the validity, there is no start (or end) node detected, "
                                    "please contact the developper about it, and how you did it"),
                                 QMessageBox::Ok);

        // We close the compute to avoid issue
        GrafileNodeModel().setComputeExec(false); // compute exec is shared for all, just close it from the main model
        Q_EMIT this->eventEnabledComputeChange(false);
        return;
    }

    // Set the data
    start->setData(sourceFiles); // We want a copy to be sure not to interfere with the source content

    // Start the calculation of the graphe
    start->setComputeExec(true);
    if (start->isComputeExec()) // Is the modification works well ? (and wasn't canceled between the time (for evt. concurrency)
    {
        start->compute();
    }
}

void GrafileWidget::cancelExecuteGraph()
{
    GrafileNodeModel().setComputeExec(false); // Canceled by the superclass to avoid null pointer on start and end
    Q_EMIT eventEnabledComputeChange(false); // Emit the signal because we don't have a connected signal for the previous instruction
}

bool GrafileWidget::verifyGraphValidity()
{
    bool hasStart = false;
    bool hasEnd = false;
    bool allNodesValid = true;
    bool hasError = false;


    auto nodes = scene->allNodes();
    foreach (auto node, nodes)
    {
        auto datamodel = node->nodeDataModel();

        if (datamodel->inherits(StartNode::staticMetaObject.className()))
        {
            if (hasStart)
            {
                hasError = true; // The graph contains multiple start
            }
            hasStart = true;
        }
        else if (datamodel->inherits(EndNode::staticMetaObject.className()))
        {
            if (hasEnd)
            {
                hasError = true; // The graph contains multiple end
            }
            hasEnd = true;
        }
        else if (datamodel->inherits(GrafileNodeModel::staticMetaObject.className()))
        {
            // No need to do something with this node
        }
        else
        {
            qDebug() << "Is a node not created for the app";
            // No handle, for eventually future integration
        }

        if (datamodel->validationState() != NodeValidationState::Valid)
        {
            allNodesValid = false;
        }
    }

    if (!hasStart || !hasEnd || !allNodesValid)
    {
        hasError = true;
    }

    qDebug() << "error : " << hasError << " // start : " << hasStart << " / end : " << hasEnd << " / all nodes valid : " << allNodesValid;
    return !hasError;
}

void GrafileWidget::loadGraph()
{
    cancelExecuteGraph();
    scene->load();
}

void GrafileWidget::saveGraph()
{
    scene->save();
}
