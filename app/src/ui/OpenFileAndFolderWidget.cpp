/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/ui/OpenFileAndFolderWidget.cpp"
 * @brief       Source file for "app/include/ui/OpenFileAndFolderWidget.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "ui/OpenFileAndFolderWidget.h"

#include <QFileDialog>
#include <QVBoxLayout>
#include <Utility.h>


OpenFileAndFolderWidget::OpenFileAndFolderWidget(QWidget *parent)
    : QWidget(parent)
{
    // Instead of having 2 button (1 for files, 1 for folders) this solution is possible :
    // https://www.qtcentre.org/threads/43841-QFileDialog-to-select-files-AND-folders
    // But the UI isn't modern (and maybe a better solution exists, this one is dated 2010)

    openedFiles = new QStringList();
    openedFolders = new QStringList();

    // Setup file dialogs
    openLocation = QDir::homePath();

    dialogFiles = new QFileDialog(this);
    dialogFiles->setDirectory(openLocation);
    dialogFiles->setFileMode(QFileDialog::ExistingFiles);

    dialogFolders = new QFileDialog(this);
    dialogFolders->setDirectory(openLocation);
    dialogFolders->setFileMode(QFileDialog::Directory);


    // Create widget content

    this->btnOpenFiles = new QPushButton(this);
    this->btnOpenFiles->setText("Select files");

    this->btnOpenFolders = new QPushButton(this);
    this->btnOpenFolders->setText("Select folders");

    this->btnCleanFiles = new QPushButton(this);
    this->btnCleanFiles->setText("Remove opened files");

    this->btnCleanFolders = new QPushButton(this);
    this->btnCleanFolders->setText("Remove opened folders");

    this->lblOpenedElements = new QLabel(this);
    this->lblOpenedElements->setText(NOTHING_OPENED);
    this->lblOpenedElements->setStyleSheet("QLabel {color: #CECECE}");


    // Create widget layout
    QVBoxLayout * layout = new QVBoxLayout(this);

    layout->addWidget(btnOpenFiles);
    layout->addWidget(btnOpenFolders);
    layout->addSpacing(20);
    layout->addWidget(btnCleanFiles);
    layout->addWidget(btnCleanFolders);
    layout->addSpacing(20);
    layout->addWidget(lblOpenedElements);

    this->setAttribute(Qt::WA_OpaquePaintEvent); // Transparent background for the widget
    this->setLayout(layout);

    // Connect actions of the widget
    connect(this->btnOpenFiles , &QPushButton::pressed, this, [&]()
    {
        this->openedFiles->append(dialogFiles->getOpenFileNames());
        this->openedFiles->removeDuplicates();
        this->openedFiles->removeAll(QString(""));

        updateOpenLocation(*this->openedFiles);

        Q_EMIT openedElementsChange(getOpenedElements());
    });

    connect(this->btnOpenFolders , &QPushButton::pressed, this, [&]()
    {
        this->openedFolders->append(dialogFolders->getExistingDirectory() + "/");
        this->openedFolders->removeDuplicates();
        this->openedFolders->removeAll(QString(""));

        updateOpenLocation(*this->openedFolders);

        Q_EMIT openedElementsChange(getOpenedElements());
    });

    connect(this->btnCleanFiles , &QPushButton::pressed, this, [&]()
    {
        this->openedFiles->clear();

        Q_EMIT openedElementsChange(getOpenedElements());
    });

    connect(this->btnCleanFolders , &QPushButton::pressed, this, [&]()
    {
        this->openedFolders->clear();

        Q_EMIT openedElementsChange(getOpenedElements());
    });

    connect(this, &OpenFileAndFolderWidget::openedElementsChange, this, [&](QStringList openedElements)
    {
        updateLabel();
    });

}

QStringList OpenFileAndFolderWidget::getOpenedElements()
{
    QStringList merged = *openedFiles;
    merged.append(*openedFolders);
    return merged;
}

void OpenFileAndFolderWidget::setOpenedElements(QStringList elements)
{
    openedFiles->clear();
    openedFolders->clear();

    foreach(auto element, elements)
    {
        if (Utility::isDir(element))
        {
           openedFolders->append(element);
        }
        else
        {
            openedFiles->append(element);
        }
    }

    //updateLabel();
    Q_EMIT openedElementsChange(getOpenedElements());
}

void OpenFileAndFolderWidget::updateLabel()
{
    auto elements = getOpenedElements();

    if (elements.size() == 0)
    {
        lblOpenedElements->setText(NOTHING_OPENED);
        lblOpenedElements->setToolTip("");
        return;
    }

    QString txt = "";
    QString txtFullPath = "";
    foreach(auto element, elements)
    {
        QFileInfo qfi(element);

        QString val = qfi.baseName();

        if (Utility::isDir(element))
        {
            val = qfi.dir().absolutePath().split("/").last() + "/";
        }

        txt += val +"\n";
        txtFullPath += element + "\n";
    }

    lblOpenedElements->setToolTip(txtFullPath);
    lblOpenedElements->setText(txt);

}

void OpenFileAndFolderWidget::updateOpenLocation(QStringList openedSrc)
{
    if (openedSrc.size() > 0)
    {
        openLocation = QFileInfo(openedSrc[0]).path();

        dialogFiles->setDirectory(openLocation);
        dialogFolders->setDirectory(openLocation);
    }
}
