/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/filter/FileNameFilterNode.cpp"
 * @brief       Source file for "app/include/filter/FileNameFilterNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "filter/FileNameFilterNode.h"

#include <QFileInfo>
#include "data/GrafileDataStringFile.h"
#include "data/GrafileDataText.h"


FileNameFilterNode::FileNameFilterNode() : FilterNodeModel("File name", true)
{
    addInputPort("StringFile", "File");
    addInputPort("Text", "File name");
    addInputPort("Boolean", "Case sensitive");

    addOutputPort("StringFile", "Filtered");
    addOutputPort("StringFile", "Excluded");
}

void FileNameFilterNode::compute()
{
    QVector<StringFile>& vecFiles = inFiles.getList();
    QVector<QString>& vecNames = inNames.getList();

    outFiltered.clear();
    QVector<StringFile>& vecOut = outFiltered.getList();
    outExcluded.clear();
    QVector<StringFile>& vecOut2 = outExcluded.getList();

    if (!isEnabled())
    {
        outFiltered = vecFiles;
        outExcluded = vecFiles;
    }
    else
    {
        Qt::CaseSensitivity sensitivity = Qt::CaseSensitive;
        if (!inCaseSensitive)
        {
            sensitivity = Qt::CaseInsensitive;
        }

        for (int i = 0; i < vecFiles.size(); i++)
        {
            QString fileName = QFileInfo(vecFiles[i].getOriginalPath()).completeBaseName();

            bool match = false;
            for (int j = 0; j < vecNames.size() && !match; j++)
            {
                QString compareName = vecNames[j];
                match = QString::compare(fileName, compareName, sensitivity) == 0;
            }

            if (match)
            {
                vecOut.push_back(vecFiles[i]);
            }
            else
            {
                vecOut2.push_back(vecFiles[i]);
            }
        }
    }

    // Rise event for perform next nodes
    Q_EMIT dataUpdated(0);
    Q_EMIT dataUpdated(1);
}

std::shared_ptr<NodeData> FileNameFilterNode::outData(PortIndex outPortIndex)
{
    if (outPortIndex == 0){
        return std::make_shared<GrafileDataStringFile>(outFiltered);
    }
    else
    {
        return std::make_shared<GrafileDataStringFile>(outExcluded);
    }
}

void FileNameFilterNode::convertInputsToVars()
{
    bool hasError = false;

    // Enable
    setEnableInput(0);

    // Input files
    {
        auto node = std::dynamic_pointer_cast<GrafileDataStringFile>(_input[1]);
        if (node)
        {
            inFiles = node->getList();
        }
        else
        {
            inFiles.clear();
            hasError = true;
        }
    }

    // Input ext text
    {
        auto node = std::dynamic_pointer_cast<GrafileDataText>(_input[2]);
        if (node)
        {
            inNames = node->getList();
        }
        else
        {
            inNames.clear();
            hasError = true;
        }
    }

    // Case sensitve
    {
        auto node = std::dynamic_pointer_cast<GrafileDataBoolean>(_input[3]);
        if (node && node->hasValues())
        {
            inCaseSensitive = node->getValue();
        }
        else
        {
            inCaseSensitive = false;
        }
    }

    changeValidationState(hasError);
}
