/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/filter/FileExtFilterNode.cpp"
 * @brief       Source file for "app/include/filter/FileExtFilterNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "filter/FileExtFilterNode.h"

#include <QFileInfo>
#include "data/GrafileDataStringFile.h"
#include "data/GrafileDataText.h"


FileExtFilterNode::FileExtFilterNode() : FilterNodeModel("File extension", true)
{
    addInputPort("StringFile", "Files");
    addInputPort("Text", "Extension");

    addOutputPort("StringFile", "Filtered");
    addOutputPort("StringFile", "Excluded");
}

void FileExtFilterNode::compute()
{
    QVector<StringFile>& vecFiles = inFiles.getList();
    QVector<QString>& vecExts = inExt.getList();

    outFiltered.clear();
    QVector<StringFile>& vecOut = outFiltered.getList();
    outExcluded.clear();
    QVector<StringFile>& vecOut2 = outExcluded.getList();

    if (!isEnabled())
    {
        outFiltered = vecFiles;
        outExcluded = vecFiles;
    }
    else
    {
        for (int i = 0; i < vecFiles.size(); i++)
        {
            QString ext = QFileInfo(vecFiles[i].getOriginalPath()).suffix();

            bool match = false;
            for (int j = 0; j < vecExts.size() && !match; j++)
            {
                match = QString::compare(ext, vecExts[j], Qt::CaseInsensitive) == 0;
            }

            if (match)
            {
                vecOut.push_back(vecFiles[i]);
            }
            else
            {
                vecOut2.push_back(vecFiles[i]);
            }
        }
    }

    // Rise event for perform next nodes
    Q_EMIT dataUpdated(0);
    Q_EMIT dataUpdated(1);

}

std::shared_ptr<NodeData> FileExtFilterNode::outData(PortIndex outPortIndex)
{
    if (outPortIndex == 0){
        return std::make_shared<GrafileDataStringFile>(outFiltered);
    }
    else
    {
        return std::make_shared<GrafileDataStringFile>(outExcluded);
    }
}

void FileExtFilterNode::convertInputsToVars()
{
    bool hasError = false;

    // Enable
    setEnableInput(0);

    // Input files
    {
        auto node = std::dynamic_pointer_cast<GrafileDataStringFile>(_input[1]);
        if (node)
        {
            inFiles = node->getList();
        }
        else
        {
            inExt.clear();
            hasError = true;
        }
    }

    // Input ext text
    {
        auto node = std::dynamic_pointer_cast<GrafileDataText>(_input[2]);
        if (node)
        {
            inExt = node->getList();
        }
        else
        {
            inExt.clear();
            hasError = true;
        }
    }

    changeValidationState(hasError);
}
