/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/filter/FilterNodeModel.cpp"
 * @brief       Source file for "app/include/filter/FilterNodeModel.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "filter/FilterNodeModel.h"

#include <nodes/ConnectionStyle>

using namespace QtNodes;

FilterNodeModel::FilterNodeModel() : FilterNodeModel("")
{
}

FilterNodeModel::FilterNodeModel(QString name, bool canEnable) : GrafileNodeModel(name)
{
    modelValidationState = NodeValidationState::Error;

    if (canEnable)
    {
        addInputPort("Boolean", "Is Enable");
    }

    NodeStyle style(R"(
      {
        "NodeStyle": {
          "NormalBoundaryColor": [255, 255, 255],
          "SelectedBoundaryColor": "#40C0FF",
          "GradientColor0": "#406080",
          "GradientColor1": "#283C50",
          "GradientColor2": "#203040",
          "GradientColor3": "#1D2B3A",
          "ShadowColor": [20, 20, 20],
          "FontColor" : "white",
          "FontColorFaded" : "gray",
          "ConnectionPointColor": [169, 169, 169],
          "FilledConnectionPointColor": "cyan",
          "ErrorColor": "red",
          "WarningColor": [128, 128, 0],

          "PenWidth": 1.0,
          "HoveredPenWidth": 1.5,

          "ConnectionPointDiameter": 8.0,

          "Opacity": 0.8
        }
      }
    )");

    setNodeStyle(style);
}

bool FilterNodeModel::compareValues(int a, CompareOperatorType comparator, int b)
{
    switch (comparator)
    {
        case CompareOperatorType::EQUALS:
            return a == b;

        case CompareOperatorType::GREATHER_THAN:
            return a > b;

        case CompareOperatorType::LOWER_THAN:
            return a < b;

        case CompareOperatorType::LOWER_EQUALS_THAN:
            return a <= b;

        case CompareOperatorType::GREATHER_EQUALS_THAN:
            return a >= b;

        case CompareOperatorType::NOT_EQUALS:
            return a != b;
    }
    return false;
}
