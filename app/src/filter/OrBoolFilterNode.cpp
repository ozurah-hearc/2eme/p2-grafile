/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/filter/OrBoolFilterNode.cpp"
 * @brief       Source file for "app/include/filter/OrBoolFilterNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "filter/OrBoolFilterNode.h"

#include "data/GrafileDataBoolean.h"

OrBoolFilterNode::OrBoolFilterNode() : FilterNodeModel("OR", false)
{
    forceCompute = true;
    addInputPort("Boolean", "A");
    addInputPort("Boolean", "B");

    addOutputPort("Boolean", "Result");
}

void OrBoolFilterNode::compute()
{
    QVector<bool>& vecA = inA.getList();
    QVector<bool>& vecB = inB.getList();

    output.clear();
    QVector<bool>& vecOut = output.getList();

    if (vecA.size() == 0)
    {
        vecOut = vecB;
    }
    else if (vecB.size() == 0)
    {
        vecOut = vecA;
    }
    else if (vecA.size() == vecB.size())
    {
        for (int i = 0; i < vecA.size(); i++)
        {
            vecOut.push_back(vecA[i] | vecB[i]);
        }
    }
    else if (vecA.size() == 1)
    {
        for (bool valueB : vecB)
        {
            vecOut.push_back(vecA[0] | valueB);
        }
    }
    else if (vecB.size() == 1)
    {
        for (bool valueA : vecA)
        {
            vecOut.push_back(valueA | vecB[0]);
        }
    }

    // Rise event for perform next nodes
    Q_EMIT dataUpdated(0);
}

std::shared_ptr<NodeData> OrBoolFilterNode::outData(PortIndex outPortIndex)
{
    return std::make_shared<GrafileDataBoolean>(output);
}

void OrBoolFilterNode::convertInputsToVars()
{
    bool hasError = false;
    bool hasNodeA = false;
    bool hasNodeB = false;

    // Input A
    {
        auto node = std::dynamic_pointer_cast<GrafileDataBoolean>(_input[0]);
        if (node)
        {
            hasNodeA = true;
            inA = node->getList();
        }
        else
        {
            inA.clear();
        }
    }

    // Input B
    {
        auto node = std::dynamic_pointer_cast<GrafileDataBoolean>(_input[1]);
        if (node)
        {
            hasNodeB = true;
            inB = node->getList();
        }
        else
        {
            inB.clear();
        }
    }

    // Validation
    if (!hasNodeA && !hasNodeB)
    {
        hasError = true;
    }

    changeValidationState(hasError);
}
