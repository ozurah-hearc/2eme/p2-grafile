/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/filter/IsFolderFilterNode.cpp"
 * @brief       Source file for "app/include/filter/IsFolderFilterNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "filter/IsFolderFilterNode.h"

#include <QFileInfo>
#include "data/GrafileDataStringFile.h"
#include "data/GrafileDataText.h"
#include "Utility.h"


IsFolderFilterNode::IsFolderFilterNode() : FilterNodeModel("Is folder", true)
{
    addInputPort("StringFile", "Files");

    addOutputPort("StringFile", "Folders");
    addOutputPort("StringFile", "Files");
}

void IsFolderFilterNode::compute()
{
    QVector<StringFile>& vecFiles = inFiles.getList();

    outFolders.clear();
    QVector<StringFile>& vecOut = outFolders.getList();
    outFiles.clear();
    QVector<StringFile>& vecOut2 = outFiles.getList();

    if (!isEnabled())
    {
        outFolders = vecFiles;
        outFiles = vecFiles;
    }
    else{
        for (StringFile& value : vecFiles)
        {
            bool isFolder = Utility::isDir(value.getOriginalPath());

            if (isFolder)
            {
                vecOut.push_back(value);
            }
            else
            {
                vecOut2.push_back(value);
            }
        }
    }

    // Rise event for perform next nodes
    Q_EMIT dataUpdated(0);
    Q_EMIT dataUpdated(1);
}

std::shared_ptr<NodeData> IsFolderFilterNode::outData(PortIndex outPortIndex)
{
    if (outPortIndex == 0){
        return std::make_shared<GrafileDataStringFile>(outFolders);
    }
    else
    {
        return std::make_shared<GrafileDataStringFile>(outFiles);
    }
}

void IsFolderFilterNode::convertInputsToVars()
{
    bool hasError = false;

    // Enable
    setEnableInput(0);

    // Input files
    {
        auto node = std::dynamic_pointer_cast<GrafileDataStringFile>(_input[1]);
        if (node)
        {
            inFiles = node->getList();
        }
        else
        {
            inFiles.clear();
            hasError = true;
        }
    }

    changeValidationState(hasError);
}
