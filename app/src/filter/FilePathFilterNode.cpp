/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/filter/FilePathFilterNode.cpp"
 * @brief       Source file for "app/include/filter/FilePathFilterNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "filter/FilePathFilterNode.h"

#include <QFileInfo>
#include <QDir>
#include "data/GrafileDataStringFile.h"
#include "data/GrafileDataText.h"

FilePathFilterNode::FilePathFilterNode() : FilterNodeModel("File path", true)
{
    addInputPort("StringFile", "File");
    addInputPort("Text", "Path name");

    addOutputPort("StringFile", "Filtered");
    addOutputPort("StringFile", "Excluded");
}

void FilePathFilterNode::compute()
{
    QVector<StringFile>& vecFiles = inFiles.getList();
    QVector<QString>& vecPaths = inPaths.getList();

    outFiltered.clear();
    QVector<StringFile>& vecOut = outFiltered.getList();
    outExcluded.clear();
    QVector<StringFile>& vecOut2 = outExcluded.getList();

    if (!isEnabled())
    {
        outFiltered = vecFiles;
        outExcluded = vecFiles;
    }
    else
    {
        for (int i = 0; i < vecFiles.size(); i++)
        {
            QString filePath = QFileInfo(vecFiles[i].getOriginalPath()).absolutePath();

            bool match = false;
            for (int j = 0; j < vecPaths.size() && !match; j++)
            {
                QString path = QFileInfo(vecPaths[j]).absolutePath();
                match = QString::compare(path, filePath, Qt::CaseInsensitive) == 0;
            }

            if (match)
            {
                vecOut.push_back(vecFiles[i]);
            }
            else
            {
                vecOut2.push_back(vecFiles[i]);
            }
        }
    }

    // Rise event for perform next nodes
    Q_EMIT dataUpdated(0);
    Q_EMIT dataUpdated(1);
}

std::shared_ptr<NodeData> FilePathFilterNode::outData(PortIndex outPortIndex)
{
    if (outPortIndex == 0){
        return std::make_shared<GrafileDataStringFile>(outFiltered);
    }
    else
    {
        return std::make_shared<GrafileDataStringFile>(outExcluded);
    }
}

void FilePathFilterNode::convertInputsToVars()
{
    bool hasError = false;

    // Enable
    setEnableInput(0);

    // Input files
    {
        auto node = std::dynamic_pointer_cast<GrafileDataStringFile>(_input[1]);
        if (node)
        {
            inFiles = node->getList();
        }
        else
        {
            inFiles.clear();
            hasError = true;
        }
    }

    // Input ext text
    {
        auto node = std::dynamic_pointer_cast<GrafileDataText>(_input[2]);
        if (node)
        {
            inPaths = node->getList();
        }
        else
        {
            inPaths.clear();
            hasError = true;
        }
    }

    changeValidationState(hasError);
}
