/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/filter/FileSizeFilterNode.cpp"
 * @brief       Source file for "app/include/filter/FileSizeFilterNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "filter/FileSizeFilterNode.h"

#include <QFileInfo>
#include "data/GrafileDataStringFile.h"
#include "data/GrafileDataComparator.h"
#include "data/GrafileDataInteger.h"


FileSizeFilterNode::FileSizeFilterNode() : FilterNodeModel("File size", true)
{
    addInputPort("StringFile", "File");
    addInputPort("Comparator", "Comparator");
    addInputPort("Decimal", "Size");

    addOutputPort("StringFile", "Filtered");
    addOutputPort("StringFile", "Excluded");
}

void FileSizeFilterNode::compute()
{
    QVector<StringFile>& vecFiles = inFiles.getList();
    QVector<CompareOperatorType>& vecComps = inComparators.getList();
    QVector<double>& vecSizes = inSizes.getList();

    outFiltered.clear();
    QVector<StringFile>& vecOut = outFiltered.getList();
    outExcluded.clear();
    QVector<StringFile>& vecOut2 = outExcluded.getList();

    if (!isEnabled())
    {
        outFiltered = vecFiles;
        outExcluded = vecFiles;
    }
    else if (vecFiles.size() == vecComps.size() && vecFiles.size() == vecSizes.size())
    {
        for (int i = 0; i < vecFiles.size(); i++)
        {
            int actualSize = QFileInfo(vecFiles[i].getOriginalPath()).size();
            int cmpSize = vecSizes[i] * 1.0 * DEFAULT_UNIT;

            if (compareValues(actualSize, vecComps[i], cmpSize))
            {
                vecOut.push_back(vecFiles[i]);
            }
            else
            {
                vecOut2.push_back(vecFiles[i]);
            }
        }
    }
    else if (vecComps.size() == 1 && vecSizes.size() == 1)
    {
        for (StringFile& value : vecFiles)
        {
            int actualSize = QFileInfo(value.getOriginalPath()).size();
            double cmpSize = vecSizes[0] * 1.0 * DEFAULT_UNIT;

            if (compareValues(actualSize, vecComps[0], cmpSize))
            {
                vecOut.push_back(value);
            }
            else
            {
                vecOut2.push_back(value);
            }
        }
    }

    // Rise event for perform next nodes
    Q_EMIT dataUpdated(0);
    Q_EMIT dataUpdated(1);

}

std::shared_ptr<NodeData> FileSizeFilterNode::outData(PortIndex outPortIndex)
{
    if (outPortIndex == 0){
        return std::make_shared<GrafileDataStringFile>(outFiltered);
    }
    else
    {
        return std::make_shared<GrafileDataStringFile>(outExcluded);
    }
}

void FileSizeFilterNode::convertInputsToVars()
{
    bool hasError = false;

    // Enable
    setEnableInput(0);

    // Input files
    {
        auto node = std::dynamic_pointer_cast<GrafileDataStringFile>(_input[1]);
        if (node)
        {
            inFiles = node->getList();
        }
        else
        {
            inFiles.clear();
            hasError = true;
        }
    }

    // Input Comparator
    {
        auto node = std::dynamic_pointer_cast<GrafileDataComparator>(_input[2]);
        if (node)
        {
            inComparators = node->getList();
        }
        else
        {
            inComparators.clear();
            hasError = true;
        }
    }

    // Input Size
    {
        auto node = std::dynamic_pointer_cast<GrafileDataDecimal>(_input[3]);
        if (node)
        {
            inSizes = node->getList();
        }
        else
        {
            inSizes.clear();
            hasError = true;
        }
    }

    changeValidationState(hasError);
}
