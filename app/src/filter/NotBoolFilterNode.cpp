/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/filter/NotBoolFilterNode.cpp"
 * @brief       Source file for "app/include/filter/NotBoolFilterNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "filter/NotBoolFilterNode.h"

#include "data/GrafileDataBoolean.h"

NotBoolFilterNode::NotBoolFilterNode() : FilterNodeModel("NOT", false)
{
    forceCompute = true;
    addInputPort("Boolean", "Value");
    addOutputPort("Boolean", "Result");
}

void NotBoolFilterNode::compute()
{

    QVector<bool>& vecIn = input.getList();

    output.clear();
    QVector<bool>& vecOut = output.getList();

    for (bool value : vecIn)
    {
        vecOut.push_back(!value);
    }

    // Rise event for perform next nodes
    Q_EMIT dataUpdated(0);
}

std::shared_ptr<NodeData> NotBoolFilterNode::outData(PortIndex outPortIndex)
{
    return std::make_shared<GrafileDataBoolean>(output);
}

void NotBoolFilterNode::convertInputsToVars()
{
    bool hasError = false;

    // Input
    {
        auto node = std::dynamic_pointer_cast<GrafileDataBoolean>(_input[0]);
        if (node)
        {
            input = node->getList();
        }
        else
        {
            input.clear();
            hasError = true;
        }
    }

    changeValidationState(hasError);
}
