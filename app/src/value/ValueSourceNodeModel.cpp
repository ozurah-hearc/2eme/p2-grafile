/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/value/ValueSourceNodeModel.cpp"
 * @brief       Source file for "app/include/value/ValueSourceNodeModel.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "value/ValueSourceNodeModel.h"

#include <nodes/ConnectionStyle>

using namespace QtNodes;

ValueSourceNodeModel::ValueSourceNodeModel() : ValueSourceNodeModel("")
{
}

ValueSourceNodeModel::ValueSourceNodeModel(QString name) : GrafileNodeModel(name)
{
    NodeStyle style(R"(
      {
        "NodeStyle": {
          "NormalBoundaryColor": [255, 255, 255],
          "SelectedBoundaryColor": "#40FFC0",
          "GradientColor0": "#408060",
          "GradientColor1": "#28503C",
          "GradientColor2": "#204030",
          "GradientColor3": "#1D3A2B",
          "ShadowColor": [20, 20, 20],
          "FontColor" : "white",
          "FontColorFaded" : "gray",
          "ConnectionPointColor": [169, 169, 169],
          "FilledConnectionPointColor": "cyan",
          "ErrorColor": "red",
          "WarningColor": [128, 128, 0],

          "PenWidth": 1.0,
          "HoveredPenWidth": 1.5,

          "ConnectionPointDiameter": 8.0,

          "Opacity": 0.8
        }
      }
    )");

    setNodeStyle(style);
}

// TODO: implement methods
