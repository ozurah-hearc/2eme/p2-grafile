/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/value/StringFileSourceNode.cpp"
 * @brief       Source file for "app/include/value/StringFileSourceNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "value/StringFileSourceNode.h"
#include "data/GrafileDataStringFile.h"
#include <QJsonArray>


StringFileSourceNode::StringFileSourceNode() : ValueSourceNodeModel("String file")
{
    addOutputPort("StringFile", "Value");
}

QWidget *StringFileSourceNode::embeddedWidget()
{
    if (this->_inputWidget) return this->_inputWidget;

    this->_inputWidget = new OpenFileAndFolderWidget(getParent());
    this->_inputWidget->setOpenedElements(valuesToListString());
    this->_inputWidget->setFixedWidth(200);

    // Connect actions of the widget
    connect(this->_inputWidget , &OpenFileAndFolderWidget::openedElementsChange, this, [&](QStringList openedElements)
    {
        bool ok = true;

        _values.clear();
        foreach(auto element, openedElements)
        {
            _values.append(StringFile(element));
        }

        // never setted ok to false, because we accept empty list
        if (ok)
        {
            Q_EMIT dataUpdated(0);
        }else
        {
            Q_EMIT dataInvalidated(0);
        }
    });

    return this->_inputWidget;
}

std::shared_ptr<NodeData> StringFileSourceNode::outData(QtNodes::PortIndex)
{
    return std::make_shared<GrafileDataStringFile>(this->_values);
}

QJsonObject StringFileSourceNode::save() const
{
    QJsonObject modelJson = NodeDataModel::save();

    modelJson["values"] = StringFile::toJson(this->_values);

    return modelJson;
}

void StringFileSourceNode::restore(QJsonObject const &p)
{
    QJsonValue v = p["values"];

    if (!v.isUndefined())
    {
        QJsonArray vals = v.toArray();
        this->_values = StringFile::fromJson(vals);

        if (this->_inputWidget)
        {
            this->_inputWidget->setOpenedElements(valuesToListString());
        }
    }
}

QStringList StringFileSourceNode::valuesToListString()
{
    QList<QString> origins;
    foreach(auto value, _values)
    {
        origins.append(value.getOriginalPath());
    }
    return origins;
}
