/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/value/BooleanSourceNode.cpp"
 * @brief       Source file for "app/include/value/BooleanSourceNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "value/BooleanSourceNode.h"

BooleanSourceNode::BooleanSourceNode() : ValueSourceNodeModel("Boolean")
{
    addOutputPort("Boolean", "Value");
}

QWidget *BooleanSourceNode::embeddedWidget()
{
    if (this->_inputWidget)
        return this->_inputWidget;

    this->_inputWidget = new QPushButton(getParent());
    this->_inputWidget->setText(valToString());

    connect(this->_inputWidget, &QPushButton::clicked, this, [&]()
            {
        // Swap the boolean
        this->_value = !this->_value;

        this->_inputWidget->setText(valToString());
        Q_EMIT dataUpdated(0); });

    return this->_inputWidget;
}

std::shared_ptr<NodeData> BooleanSourceNode::outData(QtNodes::PortIndex)
{
    return std::make_shared<GrafileDataBoolean>(this->_value);
}

QJsonObject BooleanSourceNode::save() const
{
    QJsonObject modelJson = NodeDataModel::save();

    modelJson["value"] = QString::number(this->_value);

    return modelJson;
}

void BooleanSourceNode::restore(QJsonObject const &p)
{
    QJsonValue v = p["value"];

    if (!v.isUndefined())
    {
        QString strVal = v.toString();

        bool ok;
        int d = strVal.toInt(&ok);
        if (ok)
        {
            this->_value = d;
            if (this->_inputWidget)
                this->_inputWidget->setText(valToString());
        }
    }
}

QString BooleanSourceNode::valToString()
{
    return this->_value ? "True" : "False";
}
