/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/value/TextSourceNode.cpp"
 * @brief       Source file for "app/include/value/TextSourceNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "value/TextSourceNode.h"

TextSourceNode::TextSourceNode() : ValueSourceNodeModel("Text")
{
    addOutputPort("Text", "Value");
}

QWidget *TextSourceNode::embeddedWidget()
{
    if (this->_inputWidget) return this->_inputWidget;

    this->_inputWidget = new QTextEdit(getParent());
    this->_inputWidget->setText(this->_value);
    this->_inputWidget->setFixedSize(160, 80);

    connect(this->_inputWidget , &QTextEdit::textChanged, this, [&]()
    {
        this->_value = this->_inputWidget->toPlainText();

        Q_EMIT dataUpdated(0);
    });

    return this->_inputWidget;
}

std::shared_ptr<NodeData> TextSourceNode::outData(QtNodes::PortIndex)
{
    return std::make_shared<GrafileDataText>(this->_value);
}

QJsonObject TextSourceNode::save() const
{
    QJsonObject modelJson = NodeDataModel::save();

    modelJson["value"] = this->_value;

    return modelJson;
}

void TextSourceNode::restore(QJsonObject const &p)
{
    QJsonValue v = p["value"];

    if (!v.isUndefined())
    {
        QString strVal = v.toString();

        this->_value = strVal;
        if (this->_inputWidget)
            this->_inputWidget->setText(this->_value);
    }
}
