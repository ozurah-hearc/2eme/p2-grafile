/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/value/CompareOperationSourceNode.cpp"
 * @brief       Source file for "app/include/value/CompareOperationSourceNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "value/CompareOperationSourceNode.h"

CompareOperationSourceNode::CompareOperationSourceNode() : ValueSourceNodeModel("Comparator")
{
    addOutputPort("Comparator", "Value");
}

QWidget *CompareOperationSourceNode::embeddedWidget()
{
    if (this->_inputWidget)
        return this->_inputWidget;

    this->_inputWidget = new QComboBox(getParent());

    this->_inputWidget->addItem(CompareOperatorToString[CompareOperatorType::EQUALS]);
    this->_inputWidget->addItem(CompareOperatorToString[CompareOperatorType::LOWER_THAN]);
    this->_inputWidget->addItem(CompareOperatorToString[CompareOperatorType::LOWER_EQUALS_THAN]);
    this->_inputWidget->addItem(CompareOperatorToString[CompareOperatorType::GREATHER_THAN]);
    this->_inputWidget->addItem(CompareOperatorToString[CompareOperatorType::GREATHER_EQUALS_THAN]);
    this->_inputWidget->addItem(CompareOperatorToString[CompareOperatorType::NOT_EQUALS]);

    this->_inputWidget->setCurrentText(CompareOperatorToString[this->_value]);

    connect(this->_inputWidget, &QComboBox::currentIndexChanged, this, [&]()
            {
        this->_value = StringToCompareOperator[this->_inputWidget->currentText()];
        Q_EMIT dataUpdated(0); });

    return this->_inputWidget;
}

std::shared_ptr<NodeData> CompareOperationSourceNode::outData(QtNodes::PortIndex)
{
    return std::make_shared<GrafileDataComparator>(this->_value);
}

QJsonObject CompareOperationSourceNode::save() const
{
    QJsonObject modelJson = NodeDataModel::save();

    modelJson["value"] = CompareOperatorToString[this->_value];

    return modelJson;
}

void CompareOperationSourceNode::restore(QJsonObject const &p)
{
    QJsonValue v = p["value"];

    if (!v.isUndefined())
    {
        QString strVal = v.toString();

        this->_value = StringToCompareOperator[strVal];
        if (this->_inputWidget)
            this->_inputWidget->setCurrentText(CompareOperatorToString[this->_value]);
    }
}
