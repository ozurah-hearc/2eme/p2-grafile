/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/value/DateTimeSourceNode.cpp"
 * @brief       Source file for "app/include/value/DateTimeSourceNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "value/DateTimeSourceNode.h"

DateTimeSourceNode::DateTimeSourceNode() : ValueSourceNodeModel("Date time")
{
    addOutputPort("DateTime", "Value");
}

QWidget *DateTimeSourceNode::embeddedWidget()
{
    if (this->_inputWidget) return this->_inputWidget;

    this->_inputWidget = new QDateTimeEdit(getParent());
    this->_inputWidget->setDateTime(this->_value);

    connect(this->_inputWidget , &QDateTimeEdit::dateTimeChanged, this, [&]()
    {
        this->_value = this->_inputWidget->dateTime();

        Q_EMIT dataUpdated(0);
    });

    return this->_inputWidget;
}

std::shared_ptr<NodeData> DateTimeSourceNode::outData(QtNodes::PortIndex)
{
    return std::make_shared<GrafileDataDateTime>(this->_value);
}

QJsonObject DateTimeSourceNode::save() const
{
    QJsonObject modelJson = NodeDataModel::save();

    modelJson["value"] = this->_value.toString(DATETIME_FORMAT_SAVED);

    return modelJson;
}

void DateTimeSourceNode::restore(QJsonObject const &p)
{
    QJsonValue v = p["value"];

    if (!v.isUndefined())
    {
        QString strVal = v.toString();

        this->_value = QDateTime::fromString(strVal, DATETIME_FORMAT_SAVED);
        if (this->_inputWidget)
            this->_inputWidget->setDateTime(this->_value);
    }
}
