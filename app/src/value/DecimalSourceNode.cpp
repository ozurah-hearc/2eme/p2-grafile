/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/value/DecimalSourceNode.cpp"
 * @brief       Source file for "app/include/value/DecimalSourceNode.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "value/DecimalSourceNode.h"

DecimalSourceNode::DecimalSourceNode() : ValueSourceNodeModel("Decimal")
{
    addOutputPort("Decimal", "Value");
    modelValidationError = "The input isn't a valid decimal (be sure to use \".\" and not \",\").";
}

QWidget *DecimalSourceNode::embeddedWidget()
{
    if (this->_inputWidget) return this->_inputWidget;

    this->_inputWidget = new QLineEdit(getParent());
    this->_inputWidget->setText(QString::number(this->_value));
    this->_inputWidget->setFixedWidth(80);

    connect(this->_inputWidget , &QLineEdit::textChanged, this, [&]()
    {
        bool ok = false;

        double result = this->_inputWidget->text().toDouble(&ok);

        this->_value = ok ? result : 0.0;

        if (ok)
        {
            changeValidationState(false);
            Q_EMIT dataUpdated(0);
        }
        else
        {
            changeValidationState(true);
            Q_EMIT dataInvalidated(0);
        }
    });

    return this->_inputWidget;
}

std::shared_ptr<NodeData> DecimalSourceNode::outData(QtNodes::PortIndex)
{
    return std::make_shared<GrafileDataDecimal>(this->_value);
}

QJsonObject DecimalSourceNode::save() const
{
    QJsonObject modelJson = NodeDataModel::save();

    modelJson["value"] = QString::number(this->_value);

    return modelJson;
}

void DecimalSourceNode::restore(QJsonObject const &p)
{
    QJsonValue v = p["value"];

    if (!v.isUndefined())
    {
        QString strVal = v.toString();

        bool ok;
        double d = strVal.toDouble(&ok);
        if (ok)
        {
            this->_value = d;
            if (this->_inputWidget)
                this->_inputWidget->setText(strVal);
        }
    }
}
