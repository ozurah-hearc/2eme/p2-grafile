/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/data/GrafileDataText.cpp"
 * @brief       Source file for "app/include/data/GrafileDataText.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "data/GrafileDataText.h"

using QtNodes::NodeDataType;

GrafileDataText::GrafileDataText(QVector<QString> texts)
    : GrafileData(texts, NodeDataType{"text", "Text"})
{
}

GrafileDataText::GrafileDataText(QString text)
    : GrafileDataText(QVector<QString>(1, text))
{
}

GrafileDataText::GrafileDataText()
    : GrafileDataText(QVector<QString>())
{
}
