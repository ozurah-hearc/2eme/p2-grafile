/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/data/GrafileDataDecimal.cpp"
 * @brief       Source file for "app/include/data/GrafileDataDecimal.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "data/GrafileDataDecimal.h"

using QtNodes::NodeDataType;

GrafileDataDecimal::GrafileDataDecimal(QVector<double> values)
    : GrafileData(values, NodeDataType{"decimal", "Decimal"})
{
}

GrafileDataDecimal::GrafileDataDecimal(double value)
    : GrafileDataDecimal(QVector<double>(1, value))
{
}

GrafileDataDecimal::GrafileDataDecimal()
    : GrafileDataDecimal(QVector<double>())
{
}
