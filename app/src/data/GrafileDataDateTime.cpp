/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/data/GrafileDataDateTime.cpp"
 * @brief       Source file for "app/include/data/GrafileDataDateTime.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "data/GrafileDataDateTime.h"

using QtNodes::NodeDataType;

GrafileDataDateTime::GrafileDataDateTime(QVector<QDateTime> dates)
    : GrafileData(dates, NodeDataType{"datetime", "DateTime"})
{
}

GrafileDataDateTime::GrafileDataDateTime(QDateTime date)
    : GrafileDataDateTime(QVector<QDateTime>(1, date))
{
}

GrafileDataDateTime::GrafileDataDateTime()
    : GrafileDataDateTime(QVector<QDateTime>())
{
}
