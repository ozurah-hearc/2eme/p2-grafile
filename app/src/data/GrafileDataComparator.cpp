/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/data/GrafileDataComparator.cpp"
 * @brief       Source file for "app/include/data/GrafileDataComparator.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "data/GrafileDataComparator.h"

using QtNodes::NodeDataType;

GrafileDataComparator::GrafileDataComparator(QVector<CompareOperatorType> values)
    : GrafileData(values, NodeDataType{"comparator", "Comparator"})
{
}

GrafileDataComparator::GrafileDataComparator(CompareOperatorType value)
    : GrafileDataComparator(QVector<CompareOperatorType>(1, value))
{
}

GrafileDataComparator::GrafileDataComparator()
    : GrafileDataComparator(QVector<CompareOperatorType>())
{
}
