/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/data/GrafileDataInteger.cpp"
 * @brief       Source file for "app/include/data/GrafileDataInteger.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "data/GrafileDataInteger.h"

using QtNodes::NodeDataType;

GrafileDataInteger::GrafileDataInteger(QVector<int> values)
    : GrafileData(values, NodeDataType{"integer", "Integer"})
{
}

GrafileDataInteger::GrafileDataInteger(int value)
    : GrafileDataInteger(QVector<int>(1, value))
{
}

GrafileDataInteger::GrafileDataInteger()
    : GrafileDataInteger(QVector<int>())
{
}
