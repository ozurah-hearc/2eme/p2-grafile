/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/data/GrafileDataBoolean.cpp"
 * @brief       Source file for "app/include/data/GrafileDataBoolean.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "data/GrafileDataBoolean.h"

using QtNodes::NodeDataType;

GrafileDataBoolean::GrafileDataBoolean(QVector<bool> values)
    : GrafileData(values, NodeDataType{"boolean", "Boolean"})
{
}

GrafileDataBoolean::GrafileDataBoolean(bool value)
    : GrafileDataBoolean(QVector<bool>(1, value))
{
}

GrafileDataBoolean::GrafileDataBoolean()
    : GrafileDataBoolean(QVector<bool>())
{
}
