/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/data/GrafileDataStringFile.cpp"
 * @brief       Source file for "app/include/data/GrafileDataStringFile.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "data/GrafileDataStringFile.h"
#include "data/StringFile.h"

using QtNodes::NodeDataType;


GrafileDataStringFile::GrafileDataStringFile(QVector<StringFile> values)
    : GrafileData<StringFile>(values, NodeDataType{"stringfile", "StringFile"})
{
}

GrafileDataStringFile::GrafileDataStringFile(StringFile value)
    : GrafileDataStringFile(QVector<StringFile>(1, value))
{
}

GrafileDataStringFile::GrafileDataStringFile()
    : GrafileDataStringFile(QVector<StringFile>())
{
}

StringFile * GrafileDataStringFile::getPtrValueAt(int index) {
    return &this->_list[index];
}
