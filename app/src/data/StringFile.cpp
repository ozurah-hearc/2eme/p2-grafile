/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/data/StringFile.cpp"
 * @brief       Source file for "app/include/data/StringFile.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "data/StringFile.h"
#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>

StringFile::StringFile(QString path)
    : StringFile(path, path)
{

}

StringFile::StringFile(QString path, QString originalPath)
    : StringFile(path, originalPath, "")
{

}

StringFile::StringFile(QString path, QString originalPath, QString finalPath)
    : _currentPath(path), _originalPath(originalPath), _finalPath(finalPath)
{

}

void StringFile::setCurrentPath(QString path){
    this->_currentPath = path;
}
QString StringFile::getCurrentPath(){
    return this->_currentPath;
}
void StringFile::setOriginalPath(QString path){
    this->_originalPath = path;
}
QString StringFile::getOriginalPath(){
    return this->_originalPath;
}
void StringFile::setFinalPath(QString path){
    this->_finalPath = path;
}
QString StringFile::getFinalPath(){
    return this->_finalPath;
}

void StringFile::setNeedDelete(bool state)
{
    needDelete = state;
}

bool StringFile::getNeedDelete()
{
    return needDelete;
}

void StringFile::setNeedMove(bool state)
{
    needMove = state;
}

bool StringFile::getNeedMove()
{
  return needMove;
}


QJsonObject StringFile::toJson() const
{
    return {
        {"originalPath", this->_originalPath},
        {"currentPath", this->_currentPath},
        {"finalPath", this->_finalPath},
        {"needDelete", this->needDelete},
        {"needMove", this->needMove},
    };
}

QJsonArray StringFile::toJson(const QList<StringFile> & list) {
   QJsonArray array;
   for (auto & element : list)
   {
      array.append(element.toJson());
   }
   return array;
}

void StringFile::fromJson(QJsonObject const& p)
{
    QJsonValue v1 = p["originalPath"];
    QJsonValue v2 = p["currentPath"];
    QJsonValue v3 = p["finalPath"];
    QJsonValue v4 = p["needDelete"];
    QJsonValue v5 = p["needMove"];

    if (!v1.isUndefined())
    {
        this->_originalPath = v1.toString();
        if (v2.isUndefined()) // No current path -> same as original
        {
            this->_currentPath = v1.toString();
        }
    }

    if (!v2.isUndefined())
    {
        this->_currentPath = v2.toString();
        if (v1.isUndefined()) // No original path -> same as current
        {
            this->_originalPath = v2.toString();
        }
    }

    if (!v3.isUndefined())
    {
        this->_finalPath = v3.toString();
    }

    if (!v4.isUndefined())
    {
        this->needDelete = v4.toBool();
    }

    if (!v5.isUndefined())
    {
        this->needMove = v5.toBool();
    }
}

QList<StringFile> StringFile::fromJson(QJsonArray const& list)
{
    QList<StringFile> array;
    for (auto & element : list)
    {
       StringFile sf;
       sf.fromJson(element.toObject());
       array.append(sf);
    }
    return array;
}

bool operator==(const StringFile& l, const StringFile& r)
{
    return l._originalPath ==  r._originalPath
            && l._currentPath ==  r._currentPath
            && l._finalPath ==  r._finalPath
            && l.needDelete ==  r.needDelete
            && l.needMove ==  r.needMove
            ;
}

QDebug operator<<(QDebug dbg, const StringFile &stringFile)
{
    QDebugStateSaver saver(dbg);
    dbg.nospace() << "StringFile(paths: current=" << stringFile._currentPath
                  << ", original=" << stringFile._originalPath
                  << ", final=" << stringFile._finalPath
                  << " / flags: move=" << stringFile.needMove
                  << ", delete=" << stringFile.needDelete
                  << ")";
    return dbg;
}

QDataStream &operator<<(QDataStream &out, const StringFile &item)
{
    out << item._originalPath
        << item._currentPath
        << item._finalPath
        << item.needDelete
        << item.needMove;

    return out;
}

QDataStream &operator>>(QDataStream &in, StringFile &item)
{
    item = StringFile();
    in >> item._originalPath
        >> item._currentPath
        >> item._finalPath
        >> item.needDelete
        >> item.needMove;

    return in;
}
