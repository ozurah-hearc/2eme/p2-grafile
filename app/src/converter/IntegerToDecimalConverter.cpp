/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/converter/IntegerToDecimalConverter.cpp"
 * @brief       Source file for "app/include/converter/IntegerToDecimalConverter.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "converter/IntegerToDecimalConverter.h"

SharedNodeData
IntegerToDecimalConverter::
operator()(SharedNodeData data)
{
  auto dataInteger = std::dynamic_pointer_cast<GrafileDataInteger>(data);

  if (dataInteger)
  {
    _decimal = std::make_shared<GrafileDataDecimal>(dataInteger->getValueAt(0));
  }
  else
  {
    _decimal = std::make_shared<GrafileDataDecimal>(0.0);
  }

  return _decimal;
}
