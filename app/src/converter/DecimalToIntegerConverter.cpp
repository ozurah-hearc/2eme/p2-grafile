/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/converter/DecimalToIntegerConverter.cpp"
 * @brief       Source file for "app/include/converter/DecimalToIntegerConverter.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "converter/DecimalToIntegerConverter.h"

SharedNodeData
DecimalToIntegerConverter::
operator()(SharedNodeData data)
{
  auto dataDecimal = std::dynamic_pointer_cast<GrafileDataDecimal>(data);

  if (dataDecimal)
  {
    _integer = std::make_shared<GrafileDataInteger>(dataDecimal->getValueAt(0));
  }
  else
  {
    _integer = std::make_shared<GrafileDataInteger>(0);
  }

  return _integer;
}
