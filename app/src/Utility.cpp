/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/Utility.cpp"
 * @brief       Source file for "app/include/Utility.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "Utility.h"

#include <QString>
#include <QFile>
#include <QDir>
#include <QFileInfo>

bool Utility::isDir(QString path)
{
    return QFileInfo(path).isDir() || // isDir work only on existing dir
            path.endsWith("/.") || //     After some test, a directory finish with a "/" or "\" or "." (like /aFolder/..)
            path.endsWith("/..") ||
            path.endsWith("\\.") ||
            path.endsWith("\\..") ||
            path.endsWith("/") ||
            path.endsWith("\\");
}

bool Utility::copyFile(QString src, QString to)
{
    bool result = Utility::createDir(QFileInfo(to).absolutePath());
    if (result)
    {
        result = QFile::copy(src, to);
    }
    return result;
}

bool Utility::deleteFile(QString file)
{
    bool result = true;
    if (QFile::exists(file))
    {
        result = QFile::moveToTrash(file);
    }
    return result;
}

bool Utility::createDir(QString path)
{
    bool result = true;
    if (!QDir(path).exists())
    {
        result = QDir().mkdir(path);
    }
    return result;
}
