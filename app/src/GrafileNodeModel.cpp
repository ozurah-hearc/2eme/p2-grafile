/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/GrafileNodeModel.cpp"
 * @brief       Source file for "app/include/GrafileNodeModel.h"
 *
 * @copyright   Copyright (c) 2022
 */

#include "GrafileNodeModel.h"
#include "GrafileData.h"
#include "data/GrafileDataBoolean.h"

bool GrafileNodeModel::_isComputeExec = false;

GrafileNodeModel::GrafileNodeModel() : GrafileNodeModel("Unnamed node")
{
}

GrafileNodeModel::GrafileNodeModel(QString name) : NodeDataModel(), _name(name)
{

}

QString GrafileNodeModel::caption() const
{
    return this->_name;
}

QString GrafileNodeModel::name() const
{
    return this->_name;
}

unsigned int GrafileNodeModel::nPorts(PortType portType) const
{
    switch (portType)
    {
    case PortType::In:
        return _inputTypes.size();
    case PortType::Out:
        return _outputTypes.size();
    default:
        return 0;
    }
}

NodeDataType GrafileNodeModel::dataType(QtNodes::PortType portType, QtNodes::PortIndex portIndex) const
{
    switch (portType)
    {
    case PortType::In:
        return this->_inputTypes[portIndex];
    case PortType::Out:
        return this->_outputTypes[portIndex];
    default:
        return {"unknown", "Unknown"};
    }
}

QString GrafileNodeModel::portCaption(QtNodes::PortType portType, QtNodes::PortIndex portIndex) const
{
    switch (portType)
    {
    case PortType::In:
        return this->_inputNames[portIndex];
    case PortType::Out:
        return this->_outputNames[portIndex];
    default:
        return "Unnamed port";
    }
}

void GrafileNodeModel::addInputPort(QString typeName, QString name)
{
    this->_inputTypes.append({typeName.toLower(), typeName});
    this->_inputNames.append(name);

    // We know the minimal number of input of this nodes, so we resize it by append a new one
    // The value will be setted in "convertInputsToVars"
    _input.append(nullptr);
}

void GrafileNodeModel::addOutputPort(QString typeName, QString name)
{
    this->_outputTypes.append({typeName.toLower(), typeName});
    this->_outputNames.append(name);
}

void GrafileNodeModel::
    setInData(std::shared_ptr<NodeData> data, PortIndex portIndex)
{
    _input[portIndex] = data;
    
    convertInputsToVars();

    if (_isComputeExec || forceCompute)
    {
        compute();
    }
}

void GrafileNodeModel::
    setInData(std::vector<std::shared_ptr<QtNodes::NodeData>> data, QtNodes::PortIndex inPortIndex)
{
    if (data.size() == 0)
    {
        _input[inPortIndex] = nullptr;
    }else{
        for (auto &node : data)
        {
            // For now, we don't support the multiple connection at 1 input node, so we took only the first row
            // This is why the "break"
            _input[inPortIndex] = node;
            break;
        }
    }

    convertInputsToVars();

    if (_isComputeExec || forceCompute)
    {
        compute();
    }
}

void GrafileNodeModel::setEnableInput(QtNodes::PortIndex inPortIndex)
{
    // ENABLE
    {
        auto node = std::dynamic_pointer_cast<GrafileDataBoolean>(_input[inPortIndex]);
        if (node && node->hasValues())
        {
            _isEnabled = node->getValue();
        }
        else
        {
            _isEnabled = true;
        }
    }
}

NodeValidationState GrafileNodeModel::validationState() const
{
  return modelValidationState;
}


QString GrafileNodeModel::validationMessage() const
{
  return modelValidationError;
}

void GrafileNodeModel::changeValidationState(bool hasError)
{
    if (hasError)
    {
        modelValidationState = NodeValidationState::Error;
    }else
    {
        modelValidationState = NodeValidationState::Valid;
    }
}

QWidget* GrafileNodeModel::getParent()
{
    return qobject_cast<QWidget *> (parent());
}
