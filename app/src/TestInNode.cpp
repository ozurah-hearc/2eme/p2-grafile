/**
 * @section     Domain Haute Ecole Arc (HE-Arc) Engineering (Neuchâtel)
 * @details     IT department
 * @details     Module P2 SP
 * @details     Project Grafile
 * 
 * @authors     Allemann Jonas; Chappuis Sébastien; Stouder Xavier (ISC2il-a;b)
 *
 * @version     1.0
 * @date        February - June 2022
 *
 * @file        "app/src/TestInNode.cpp"
 * @brief       Source file for "app/include/TestInNode.h"
 * @remark      This node is used to debug "output" of other nodes
 *
 * @copyright   Copyright (c) 2022
 */

#include "TestInNode.h"

#include "includes_grafileData.h"

TestInNode::TestInNode() : GrafileNodeModel("Test In")
{
    forceCompute = true;
    addInputPort("Boolean", "Bool");
    addInputPort("Comparator", "Comparator");
    addInputPort("DateTime", "Date Time");
    addInputPort("Decimal", "Decimal");
    addInputPort("Integer", "Int");
    addInputPort("StringFile", "String File");
    addInputPort("Text", "Text");
}

void TestInNode::compute()
{
    if (inBoolConnected)
    {
        qDebug() << "--- Input boolean ----";
        qDebug() << inBool.getList();
    }

    if (inCompConnected)
    {
        qDebug() << "--- Input Comparator ----";
        qDebug() << inComp.getList();
    }

    if (inDateTimeConnected)
    {
        qDebug() << "--- Input Date time ----";
        qDebug() << inDateTime.getList();
    }

    if (inDecimalConnected)
    {
        qDebug() << "--- Input Decimal ----";
        qDebug() << inDecimal.getList();
    }

    if (inIntConnected)
    {
        qDebug() << "--- Input Int ----";
        qDebug() << inInt.getList();
    }

    if (inStringFileConnected)
    {
        qDebug() << "--- Input String file ----";
        qDebug() << inStringFile.getList();
    }

    if (inTextConnected)
    {
        qDebug() << "--- Input Text ----";
        qDebug() << inText.getList();
    }
}

void TestInNode::convertInputsToVars()
{
    inBool.clear();
    inComp.clear();
    inDateTime.clear();
    inDecimal.clear();
    inInt.clear();
    inStringFile.clear();
    inText.clear();

    inBoolConnected = false;
    inCompConnected = false;
    inDateTimeConnected = false;
    inDecimalConnected = false;
    inIntConnected = false;
    inStringFileConnected = false;
    inTextConnected = false;

    for (int i = 0; i < _input.size(); i++)
    {
        {
            auto node = std::dynamic_pointer_cast<GrafileDataBoolean>(_input[i]);
            if (node)
            {
                inBool = node->getList();
                inBoolConnected = true;
                continue;
            }
        }
        {
            auto node = std::dynamic_pointer_cast<GrafileDataComparator>(_input[i]);
            if (node)
            {
                inComp = node->getList();
                inCompConnected = true;
                continue;
            }
        }
        {
            auto node = std::dynamic_pointer_cast<GrafileDataDateTime>(_input[i]);
            if (node)
            {
                inDateTime = node->getList();
                inDateTimeConnected = true;
                continue;
            }
        }
        {
            auto node = std::dynamic_pointer_cast<GrafileDataDecimal>(_input[i]);
            if (node)
            {
                inDecimal = node->getList();
                inDecimalConnected = true;
                continue;
            }
        }
        {
            auto node = std::dynamic_pointer_cast<GrafileDataInteger>(_input[i]);
            if (node)
            {
                inInt = node->getList();
                inIntConnected = true;
                continue;
            }
        }
        {
            auto node = std::dynamic_pointer_cast<GrafileDataStringFile>(_input[i]);
            if (node)
            {
                inStringFile = node->getList();
                inStringFileConnected = true;
                continue;
            }
        }
        {
            auto node = std::dynamic_pointer_cast<GrafileDataText>(_input[i]);
            if (node)
            {
                inText = node->getList();
                inTextConnected = true;
                continue;
            }
        }
    }
}
